/* eslint-disable @typescript-eslint/no-var-requires */
const npmPackage = require('./package.json')
const webpack = require('webpack');
const path = require('path');
const nodeExternals = require('webpack-node-externals');
const {TsConfigPathsPlugin, CheckerPlugin} = require('awesome-typescript-loader');
module.exports = {
  entry: ['webpack/hot/poll?100', './src/main.ts'],
  watch: true,
  target: 'node',
  externals: [
    nodeExternals({
      whitelist: ['webpack/hot/poll?100'],
    }),
  ],
  module: {
    rules: [
      {
        test: /.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  mode: 'development',
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
    plugins: [new TsConfigPathsPlugin(path.join(__dirname, 'tsonfig.json'))],
    alias: npmPackage._moduleAliases || {},
  },
  plugins: [new webpack.HotModuleReplacementPlugin(), new CheckerPlugin()],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'server.js',
  },
};
