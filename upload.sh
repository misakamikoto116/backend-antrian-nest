# yarn && yarn build
rsync -e=ssh -avz --exclude-from 'exclude.rsync' .  root@178.128.61.85:/home/server/www/backend_antrian
rsync -e=ssh -avz --exclude-from 'exclude.rsync' .  vps1@36.91.27.228:/home/vps1/www/backend-antrian-nest
ssh root@178.128.61.85 "cd /home/server/www/backend_antrian &&  ~/.nvm/versions/node/v12.13.0/bin/npm install  && ~/.nvm/versions/node/v12.13.0/bin/node -r module-alias/register ./node_modules/typeorm/cli.js schema:sync"
ssh vps1@36.91.27.228 "cd /home/vps1/www/backend-antrian-nest &&  ~/.nvm/versions/node/v12.13.0/bin/npm install  && ~/.nvm/versions/node/v12.13.0/bin/node -r module-alias/register ./node_modules/typeorm/cli.js schema:sync"
ssh root@178.128.61.85 "systemctl restart antrian"
ssh root@36.91.27.228 "systemctl restart antrian"