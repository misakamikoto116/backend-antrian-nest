module.exports = {
  apps: [
    {
      name: 'API',
      script: 'dist/main.js',
      node_args: '-r module-alias/register -r source-map-support/register ',
      interpreter: '~/.nvm/versions/node/v12.13.0/bin/node',
      instances: 1,
      autorestart: true,
      watch: false,
      max_memory_restart: '1G',
    },
  ],
};
