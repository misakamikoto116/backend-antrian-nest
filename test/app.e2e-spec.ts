import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';

describe('AppController (e2e)', () => {
  let app;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });
  describe('MobileAntrian', () =>{
    it('GET /api/v1/mobile/antrian', () =>{
      return new Promise( (resolve,reject) => {
        request(app.getHttpServer())
          .get('/api/v1/mobile/antrian')
          // .sett()
          .expect(200)
          .then(response =>{
            expect(response).toHaveProperty('metaData');
            expect(response.metaData).toHaveProperty('message');
            resolve(response);
          }).catch(reject)

      }) 
    });
  });
  afterAll(() =>{
    app.close()
  })
});
