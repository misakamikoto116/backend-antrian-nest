import { ConflictException } from '@nestjs/common';
import Axios, { AxiosInstance } from 'axios';
import { format } from 'url';

export default class Nexmo {
  private credential: { api_key: string; api_secret: string };
  axiosService: AxiosInstance;
  constructor(private apiKey: string, private apiSecreatKey: string) {
    this.axiosService = Axios.create({
      baseURL: 'https://api.nexmo.com',
    });
    this.credential = {
      api_key: this.apiKey,
      api_secret: this.apiSecreatKey,
    };
  }

  async request(otp: RequestOptions): Promise<Success> {
    return this.axiosService
      .get('verify/json', {
        params: {
          ...otp,
          ...this.credential,
        },
      })
      .then(res => {
        if (res.data.status === '0') {
          return res.data as Success;
        }
        const errorMessage = res.data.error_text;
        throw new ConflictException(errorMessage);
      });
  }

  async cancel(requestId: string): Promise<Success> {
    return this.axiosService
      .get('verify/control/json', {
        params: {
          cmd: 'cancel',
          request_id: requestId,
          ...this.credential,
        },
      })
      .then(res => {
        if (res.data.status === '0') {
          return res.data as Success;
        }
        const errorMessage = res.data.error_text;
        throw new ConflictException(errorMessage);
      });
  }

  async verify(opt: CheckOption): Promise<Success> {
    return this.axiosService
      .get('verify/check/json', {
        params: {
          ...opt,
          ...this.credential,
        },
      })
      .then(res => {
        if (res.data.status === '0') {
          return res.data as Success;
        }
        const errorMessage = res.data.error_text;
        throw new ConflictException(errorMessage);
      });
  }
}

interface RequestOptions {
  number: string;
  country?: string;
  brand?: string;
  sender_id?: string;
  code_length?: number;
  pin_expiry?: number;
  next_event_wait?: number;
}

interface CheckOption {
  request_id: string;
  code: string;
}
export interface Success {
  request_id: string;
  status: string;
}
export interface Error {
  request_id: string;
  status: string;
  error_text: string;
}
