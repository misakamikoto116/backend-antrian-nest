import { Loket } from '@entities/loket/loket.entity';

import {
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  ManyToOne,
} from 'typeorm';
import { Instansi } from '@entities/instansi/instansi.entity';

@Entity()
export class Layar {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  kode: string;

  @Column()
  status: boolean;

  @OneToMany(() => Loket, loket => loket.layar)
  loket: Loket[];

  @ManyToOne(() => Instansi, loket => loket.layar)
  instansi: Instansi;
}
