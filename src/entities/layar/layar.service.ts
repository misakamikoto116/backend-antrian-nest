import { Injectable, Inject, Scope } from '@nestjs/common';
import { Layar } from './layar.entity';
import { LayarDto } from '@modules/web/web-layar/dtos/layar.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { REQUEST } from '@nestjs/core';
import { CrudService } from '@system/modules/crud.service';
import { Repository } from 'typeorm';
import { Pagination, paginate } from 'nestjs-typeorm-paginate';
import { LayarModule } from './layar.module';

@Injectable({
  scope: Scope.REQUEST,
})
export class LayarService extends CrudService<Layar, LayarDto> {
  constructor(
    @InjectRepository(Layar) private readonly repository: Repository<Layar>,
    @Inject(REQUEST) req: any,
  ) {
    super(Layar, repository, req);
  }
  list(
    relations: string[],
    page?: number,
    limit?: number,
  ): Promise<Pagination<Layar>> {
    const option = {
      page,
      limit,
    };
    return paginate(this.repository, option, {
      relations,
    });
  }
}
