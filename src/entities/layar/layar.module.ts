import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Layar } from './layar.entity';
import { LayarService } from './layar.service';

@Module({
    imports: [TypeOrmModule.forFeature([Layar])],
    providers: [LayarService],
    exports: [LayarService],
})
export class LayarModule {}
