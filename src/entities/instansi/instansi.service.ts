import { Injectable, Inject } from '@nestjs/common';
import { Instansi } from './instansi.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BadRequestException } from '@system/execptions/BadRequestException';
import { basePath } from '@system/helpers/basePath';
import { join } from 'path';
import {
  paginate,
  IPaginationOptions,
  Pagination,
} from 'nestjs-typeorm-paginate';
import { Layanan } from '@entities/layanan/layanan.entity';
import { Printer } from '@entities/printer/printer.entity';
import { Petugas } from '@entities/petugas/petugas.entity';
import { Layar } from '@entities/layar/layar.entity';
import { Anjungan } from '@entities/anjungan/anjungan.entity';

@Injectable()
export class InstansiService {
  constructor(
    @InjectRepository(Instansi)
    private readonly modelRepository: Repository<Instansi>,
    @InjectRepository(Layanan)
    private readonly instansiRepository: Repository<Layanan>,
    @InjectRepository(Printer)
    private readonly printerRepository: Repository<Printer>,
    @InjectRepository(Petugas)
    private readonly petugasRepository: Repository<Petugas>,
    @InjectRepository(Layar)
    private readonly layarRepository: Repository<Layar>,
    @InjectRepository(Anjungan)
    private readonly anjunganRepository: Repository<Anjungan>,
  ) {}

  async store(instansi: Instansi): Promise<Instansi> {
    return await this.modelRepository.save(instansi);
  }
  async getAll(propOpt: IPaginationOptions): Promise<Pagination<Instansi>> {
    const props = Object.assign(
      {
        limit: 10,
        page: 1,
      },
      propOpt,
    ) as IPaginationOptions;
    return await paginate(this.modelRepository, props, {
      relations: ['provinsi', 'kabupaten', 'kecamatan', 'kelurahan'],
    });
  }
  async getSingleItem(id: number): Promise<Instansi> {
    return await this.modelRepository.findOne(id, {
      relations: ['provinsi', 'kabupaten', 'kecamatan', 'kelurahan'],
    });
  }
  async udpate(id: number, instansi: Instansi): Promise<Instansi> {
    const inDatabse = await this.modelRepository.preload({
      id,
    });
    const updated = this.modelRepository.merge(inDatabse, instansi);
    return await this.modelRepository.save(updated);
  }
  async delete(id: number): Promise<void> {
    if ((await this.printerRepository.find()).length > 0) {
      throw new BadRequestException('Instansi still in releated with  printer');
    }
    if ((await this.petugasRepository.find()).length > 0) {
      throw new BadRequestException('Instansi still in releated with  petugas');
    }
    if ((await this.layarRepository.find()).length > 0) {
      throw new BadRequestException('Instansi still in releated with  layar');
    }
    if ((await this.anjunganRepository.find()).length > 0) {
      throw new BadRequestException(
        'Instansi still in releated with  anjungan',
      );
    }
    await this.modelRepository.delete(id);
  }
}
