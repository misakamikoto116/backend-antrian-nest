import { Layanan } from '@entities/layanan/layanan.entity';
import { Kabupaten } from '@entities/wilayah/kabupaten/kabupaten.entity';
import { Kecamatan } from '@entities/wilayah/kecamatan/kecamatan.entity';
import { Kelurahan } from '@entities/wilayah/kelurahan/kelurahan.entity';
import { Provinsi } from '@entities/wilayah/provinsi/provinsi.entity';
import { IsNotEmpty, IsString, ValidateIf } from 'class-validator';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Printer } from '@entities/printer/printer.entity';
import { Anjungan } from '@entities/anjungan/anjungan.entity';
import { Petugas } from '@entities/petugas/petugas.entity';
import { Layar } from '@entities/layar/layar.entity';

@Entity()
export class Instansi {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nama: string;

  @Column()
  logo: string;

  @Column()
  tagline: string;

  @Column({ default: null })
  alamat: string;

  @ManyToOne(type => Provinsi, provinsi => provinsi.instansi)
  provinsi: Provinsi;

  @ManyToOne(type => Kabupaten, kabupaten => kabupaten.instansi)
  kabupaten: Kabupaten;

  @ManyToOne(type => Kecamatan, kecamatan => kecamatan.instansi)
  kecamatan: Kecamatan;

  @ManyToOne(type => Kelurahan, kelurahan => kelurahan.instansi)
  kelurahan: Kelurahan;

  @OneToMany(type => Layanan, layanan => layanan.instansi)
  layanan: Layanan[];

  @OneToMany(type => Anjungan, anjugan => anjugan.instansi)
  anjungan: Anjungan[];

  @OneToMany(type => Printer, printer => printer.instansi)
  printer: Printer[];

  @OneToMany(type => Layar, layar => layar.instansi)
  layar: Layar[];

  @OneToOne(() => Petugas, petugas => petugas.instansi)
  petugas: Petugas;
}
