import { Module } from '@nestjs/common';
import { InstansiService } from './instansi.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Instansi } from './instansi.entity';
import { InstansiCrudService } from './instansi-crud.service';
import { Anjungan } from '@entities/anjungan/anjungan.entity';
import { Printer } from '@entities/printer/printer.entity';
import { Layanan } from '@entities/layanan/layanan.entity';
import { Layar } from '@entities/layar/layar.entity';
import { Petugas } from '@entities/petugas/petugas.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Instansi,
      Anjungan,
      Printer,
      Layanan,
      Layar,
      Petugas,
    ]),
  ],
  providers: [InstansiService, InstansiCrudService],
  exports: [
    InstansiService,
    InstansiCrudService,
    TypeOrmModule.forFeature([Instansi]),
  ],
})
export class InstansiModule {}
