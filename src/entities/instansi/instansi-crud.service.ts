import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Instansi } from './instansi.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
@Injectable()
export class InstansiCrudService extends TypeOrmCrudService<Instansi> {
  constructor(@InjectRepository(Instansi) repo: Repository<Instansi>) {
    super(repo);
  }
}
