import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Iklan } from './iklan.entity';
import { IklanService } from './iklan.service';

const iklan = TypeOrmModule.forFeature([Iklan]);

@Module({
  imports: [iklan],
  exports: [iklan, IklanService],
  providers: [IklanService],
})
export class IklanModule {}
