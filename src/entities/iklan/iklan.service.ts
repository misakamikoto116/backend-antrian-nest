import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Iklan } from './iklan.entity';

@Injectable()
export class IklanService {
  getIklanInMobile(limit: number): any {
    return this.repo
      .createQueryBuilder('iklan')
      .orderBy('RAND()', 'DESC')
      .limit(limit)
      .getMany()
      .then(aRes => aRes.map(({ id, ...res }) => res));
  }
  constructor(
    @InjectRepository(Iklan) private readonly repo: Repository<Iklan>,
  ) {}
}
