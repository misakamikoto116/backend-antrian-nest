import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Iklan {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;
  @Column()
  url: string;
}
