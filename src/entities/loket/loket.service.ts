import { AntrianService } from '@entities/antrian/antrian.service';
import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Loket } from './loket.entity';
import { CrudService } from '@system/modules/crud.service';
import { REQUEST } from '@nestjs/core';
import { LoketDto } from '@modules/web/web-loket/dtos/loket.dto';

@Injectable()
export class LoketService extends CrudService<Loket, LoketDto> {
  constructor(
    @InjectRepository(Loket)
    private readonly loketRepository: Repository<Loket>,
    private readonly antrianService: AntrianService,
    @Inject(REQUEST) req: any,
  ) {
    super(Loket, loketRepository, req);
  }
  getSisaAntrianLayanan(id: number, date: string): Promise<number> {
    return this.antrianService.getWaitingQueue(id, date);
  }
  async getLoketByLayanan(
    id: number,
    date: string,
  ): Promise<ResponseLoketMapped[]> {
    const lokets = await this.loketRepository
      .createQueryBuilder('loket')
      .where('loket.layananId = :id', { id })
      .andWhere('loket.aktif = :aktif', { aktif: 1 })
      .getMany();
    let antrianInLoket = [];
    if (lokets.length > 0) {
      antrianInLoket = await this.antrianService.getLastQueueInLokets(
        lokets.map(lok => lok.id),
        date,
      );
    }
    return lokets.map(itemLoket => {
      const hasLastAntrian = antrianInLoket.find(item => {
        return item.loket_id === itemLoket.id;
      });
      const antrianTerakhir =
        hasLastAntrian !== undefined ? hasLastAntrian.last_antrian : 0;
      return {
        id: itemLoket.id,
        nama: itemLoket.nama,
        kode: itemLoket.kode,
        antrianTerakhir,
      };
    });
  }
}
