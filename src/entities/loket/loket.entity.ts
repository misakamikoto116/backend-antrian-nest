import { Antrian } from '@entities/antrian/antrian.entity';
import { Layanan } from '@entities/layanan/layanan.entity';
import { Layar } from '@entities/layar/layar.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Loket {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  kode: string;

  @Column()
  nama: string;

  @Column()
  aktif: boolean;

  @ManyToOne(() => Layar, layar => layar.loket, { nullable: true })
  layar: Layar;

  @OneToMany(() => Antrian, antrian => antrian.loket)
  antrian: Antrian[];

  @ManyToOne(() => Layanan, layanan => layanan.loket, {
    nullable: true,
  })
  layanan: Layanan;
}
