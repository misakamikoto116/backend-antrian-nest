import { Antrian } from '@entities/antrian/antrian.entity';
import { AntrianModule } from '@entities/antrian/antrian.module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Loket } from './loket.entity';
import { LoketService } from './loket.service';

@Module({
  imports: [AntrianModule, TypeOrmModule.forFeature([Loket])],
  exports: [LoketService, TypeOrmModule.forFeature([Loket])],
  providers: [LoketService],
})
export class LoketModule {}
