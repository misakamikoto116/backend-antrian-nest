import { Entity, ManyToOne, OneToOne, JoinColumn, OneToMany } from 'typeorm';
import { BaseEntity } from '@system/base.entity';
import { Printer } from '@entities/printer/printer.entity';
import { Instansi } from '@entities/instansi/instansi.entity';
import { Antrian } from '@entities/antrian/antrian.entity';

@Entity()
export class Anjungan extends BaseEntity {
  @ManyToOne(() => Instansi, instansi => instansi.anjungan)
  instansi: Instansi;

  @OneToMany(() => Antrian, antrian => antrian.anjungan)
  antrian: Antrian[];

  @OneToOne(() => Printer, printer => printer.anjungan)
  @JoinColumn()
  printer: Printer;
}
