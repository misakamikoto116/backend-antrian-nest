import { Module } from '@nestjs/common';
import { Anjungan } from './anjungan.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Anjungan])],
  exports: [TypeOrmModule.forFeature([Anjungan])],
})
export class AnjunganModule {}
