import { Injectable, Inject } from '@nestjs/common';
import { CrudService } from '@system/modules/crud.service';
import { Role } from './role.entity';
import { RoleDto } from '@modules/web/web-role/dto/role.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { REQUEST } from '@nestjs/core';

@Injectable()
export class RoleService extends CrudService<Role, RoleDto> {
  constructor(
    @InjectRepository(Role)
    private readonly roleRepository: Repository<Role>,
    @Inject(REQUEST) req: any,
  ) {
    super(Role, roleRepository, req);
  }
}
