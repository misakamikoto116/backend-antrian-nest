import { BadRequestException } from '@system/execptions/BadRequestException';
import { paginate, Pagination } from 'nestjs-typeorm-paginate';
import { Like, Repository } from 'typeorm';

export abstract class WilayahService<T, U> {
  findOne(id: number, relations: string[]): Promise<T> {
    return this.wilayahRepository.findOne(id, {
      relations,
    });
  }
  private entity: T;
  constructor(
    entity: new () => T,
    private readonly wilayahRepository: Repository<T>,
    private readonly request: any,
  ) {
    this.entity = new entity();
  }
  async update(id: number, data: U): Promise<T> {
    const wilayah: T = await this.wilayahRepository.findOne(id);
    if (wilayah === undefined) {
      throw new BadRequestException(
        'wilayah dengan id berikut tidak ditemukan',
      );
    }
    this.wilayahRepository.merge(wilayah, data);
    return await this.wilayahRepository.save(wilayah);
  }
  async create(data: U): Promise<T> {
    const entity = this.entity;
    const provinsi: T = this.wilayahRepository.merge(entity, data);
    return await this.wilayahRepository.save(provinsi);
  }
  async delete(id: number): Promise<void> {
    await this.wilayahRepository.delete(id);
  }
  async list(page = 1, limit = 10, relations): Promise<Pagination<T>> {
    const option = {
      page,
      limit,
      route: 'http://' + this.request.hostname + this.request.originalUrl,
    };
    const orderBy = {};
    return await paginate(this.wilayahRepository, option, {
      relations,
      order: {
        ...orderBy,
      },
      ...this.filterBy(this.request),
    });
  }
  filterBy(req: any): {} {
    return {
      code: Like(req.code),
      name: Like(req.name),
    };
  }
}
