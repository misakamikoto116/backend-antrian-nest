import { Injectable, Inject } from '@nestjs/common';
import { WilayahService } from '../wilayah.service';
import { Kabupaten } from './kabupaten.entity';
import { KabupatenDto } from './dtos/kabupaten.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Equal } from 'typeorm';
import { REQUEST } from '@nestjs/core';

@Injectable()
export class KabupatenService extends WilayahService<Kabupaten, KabupatenDto> {
  constructor(
    @InjectRepository(Kabupaten) repository: Repository<Kabupaten>,
    @Inject(REQUEST) req: any,
  ) {
    super(Kabupaten, repository, req);
  }
  filterBy(req: any): {} {
      return {
          ...super.filterBy(req),
          provinsiId: Equal(req.provinsiId),
        };
  }
}
