import { Kecamatan } from '../kecamatan/kecamatan.entity';
import { OneToMany, ManyToOne, Entity } from 'typeorm';
import { Provinsi } from '../provinsi/provinsi.entity';
import { AbstractWilayah } from '../abstract-wilayah';
import { Instansi } from '@entities/instansi/instansi.entity';

@Entity()
export class Kabupaten extends AbstractWilayah {
  @OneToMany(() => Kecamatan, kecamatan => kecamatan.kelurahan)
  kecamatan: Kecamatan[];

  @ManyToOne(() => Provinsi, provinsi => provinsi.kabupaten)
  provinsi: Provinsi[];
  @OneToMany(() => Instansi, instansi => instansi.kabupaten)
  instansi: Instansi[];
}
