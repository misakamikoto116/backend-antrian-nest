import { WilayahDto } from '@entities/wilayah/dtos/wilayah.dto';
import { Expose, Type } from 'class-transformer';
import { IsDefined, ValidateNested } from 'class-validator';

import { ProvinsiDto } from './provinsi.dto';

export class KabupatenDto extends WilayahDto {
    @IsDefined()
    @Type(() => ProvinsiDto)
    @ValidateNested()
    provinsi: ProvinsiDto;
}
