import { Provinsi } from '@entities/wilayah/provinsi/provinsi.entity';
import { InDatabase } from '@system/decorators/validation/in.database.validation';
import { IsDefined, Validate } from 'class-validator';

export class ProvinsiDto extends Provinsi {
  @InDatabase(Provinsi)
  @IsDefined()
  id: number;
}
