import { Instansi } from '@entities/instansi/instansi.entity';
import { IsNotEmpty, IsString } from 'class-validator';
import { Column, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

export abstract class AbstractWilayah {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  code: string;

  @Column()
  name: string;
}
