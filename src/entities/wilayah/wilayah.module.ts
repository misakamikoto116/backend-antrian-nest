import { Module } from '@nestjs/common';
import { ProvinsiService } from './provinsi/provinsi.service';
import { KecamatanService } from './kecamatan/kecamatan.service';
import { KelurahanService } from './kelurahan/kelurahan.service';
import { KabupatenService } from './kabupaten/kabupaten.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Provinsi } from './provinsi/provinsi.entity';
import { Kecamatan } from './kecamatan/kecamatan.entity';
import { Kelurahan } from './kelurahan/kelurahan.entity';
import { Kabupaten } from './kabupaten/kabupaten.entity';

@Module({
  imports: [
        TypeOrmModule.forFeature([
      Provinsi,
      Kecamatan,
      Kelurahan,
      Kabupaten,
    ]),
  ],
  providers: [
    ProvinsiService, KecamatanService, KelurahanService, KabupatenService,
  ],
  exports: [
    ProvinsiService, KecamatanService, KelurahanService, KabupatenService,
  ],
})
export class WilayahModule {}
