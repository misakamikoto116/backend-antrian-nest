import { Injectable, Inject } from '@nestjs/common';
import { WilayahService } from '../wilayah.service';
import { Kecamatan } from './kecamatan.entity';
import { KecamatanDto } from './dtos/kecamatan.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Equal } from 'typeorm';
import { REQUEST } from '@nestjs/core';

@Injectable()
export class KecamatanService extends WilayahService<Kecamatan, KecamatanDto> {
  constructor(
    @InjectRepository(Kecamatan) repository: Repository<Kecamatan>,
    @Inject(REQUEST) req: any,
  ) {
    super(Kecamatan, repository, req);
  }
  filterBy(req: any): {} {
    return {
      ...super.filterBy(req),
      kabupatenId: Equal(req.kabupatenId),
    };
  }
}
