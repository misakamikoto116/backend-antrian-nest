import { OneToMany, Entity, ManyToOne } from 'typeorm';
import { Kabupaten } from '../kabupaten/kabupaten.entity';
import { Kelurahan } from '../kelurahan/kelurahan.entity';
import { AbstractWilayah } from '../abstract-wilayah';
import { Instansi } from '@entities/instansi/instansi.entity';

@Entity()
export class Kecamatan extends AbstractWilayah {
  @ManyToOne(() => Kabupaten, kabupaten => kabupaten.kecamatan)
  kabupaten: Kabupaten;

  @OneToMany(() => Kelurahan, kelurahan => kelurahan.kecamatan)
  kelurahan: Kelurahan[];

  @OneToMany(() => Instansi, instansi => instansi.kecamatan)
  instansi: Instansi[];
}
