import { Kabupaten } from '@entities/wilayah/kabupaten/kabupaten.entity';
import { InDatabase } from '@system/decorators/validation/in.database.validation';
import { Validate } from 'class-validator';

export class KabupatenDto extends Kabupaten {
  @InDatabase(Kabupaten, 'id')
  id: number;
}
