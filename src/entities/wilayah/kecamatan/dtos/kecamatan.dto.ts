import { WilayahDto } from '@entities/wilayah/dtos/wilayah.dto';
import { Type } from 'class-transformer';
import { IsDefined, ValidateNested } from 'class-validator';

import { KabupatenDto } from './kabupaten.dto';

export class KecamatanDto extends WilayahDto {
         @IsDefined()
         @ValidateNested()
         @Type(() => KabupatenDto)
         kabupaten: KabupatenDto;
       }
