import { Kecamatan } from '@entities/wilayah/kecamatan/kecamatan.entity';
import { InDatabase } from '@system/decorators/validation/in.database.validation';
import { IsDefined, Validate } from 'class-validator';

export class KecamatanDto extends Kecamatan {
  @InDatabase(Kecamatan)
  @IsDefined()
  id: number;
}
