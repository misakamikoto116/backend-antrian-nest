import { WilayahDto } from '@entities/wilayah/dtos/wilayah.dto';
import { Kecamatan } from '@entities/wilayah/kecamatan/kecamatan.entity';
import { InDatabase } from '@system/decorators/validation/in.database.validation';
import { Type } from 'class-transformer';
import { Validate, ValidateNested } from 'class-validator';

import { KecamatanDto } from './kecamatan.dto';

export class KelurahanDto extends WilayahDto {
  @InDatabase(Kecamatan, 'id')
  @ValidateNested()
  @Type(() => KecamatanDto)
  kecamatan: KecamatanDto;
}
