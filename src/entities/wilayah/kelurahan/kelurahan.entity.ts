import { Kecamatan } from '../kecamatan/kecamatan.entity';
import { ManyToOne, Entity, OneToMany } from 'typeorm';
import { AbstractWilayah } from '../abstract-wilayah';
import { Instansi } from '@entities/instansi/instansi.entity';
@Entity()
export class Kelurahan extends AbstractWilayah {
  @ManyToOne(() => Kecamatan, kecamatan => kecamatan.kelurahan)
  kecamatan: Kecamatan;
  @OneToMany(() => Instansi, instansi => instansi.kelurahan)
  instansi: Instansi[];
}
