import { Injectable, Inject } from '@nestjs/common';
import { WilayahService } from '../wilayah.service';
import { Kelurahan } from './kelurahan.entity';
import { KelurahanDto } from './dtos/kelurahan.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Equal } from 'typeorm';
import { REQUEST } from '@nestjs/core';

@Injectable()
export class KelurahanService extends WilayahService<Kelurahan, KelurahanDto> {
  constructor(
    @InjectRepository(Kelurahan) repository: Repository<Kelurahan>,
    @Inject(REQUEST) req: any,
  ) {
    super(Kelurahan, repository, req);
  }
  filterBy(req: any): {} {
    return {
      ...super.filterBy(req),
      kelurahanId: Equal(req.kelurahanId),
    };
  }
}
