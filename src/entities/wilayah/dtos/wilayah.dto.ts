import { IsNotEmpty, IsString } from 'class-validator';

export class WilayahDto {

    @IsString()
    @IsNotEmpty()
    code: string;

    @IsString()
    @IsNotEmpty()
    name: string;
}
