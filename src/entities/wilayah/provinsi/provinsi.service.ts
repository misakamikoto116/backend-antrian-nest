import { Injectable, Inject } from '@nestjs/common';
import { WilayahService } from '../wilayah.service';
import { Provinsi } from './provinsi.entity';
import { ProvinsiDto } from './dtos/provinsi.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { REQUEST } from '@nestjs/core';

@Injectable()
export class ProvinsiService extends WilayahService<Provinsi, ProvinsiDto> {
    constructor(
        @InjectRepository(Provinsi) repository: Repository<Provinsi>,
        @Inject(REQUEST) req: any,
    ) {
        super(Provinsi, repository, req);
    }
}
