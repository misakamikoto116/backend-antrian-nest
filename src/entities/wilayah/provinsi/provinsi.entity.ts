import { OneToMany, Entity } from 'typeorm';
import { Kabupaten } from '../kabupaten/kabupaten.entity';
import { AbstractWilayah } from '../abstract-wilayah';
import { Instansi } from '@entities/instansi/instansi.entity';

@Entity()
export class Provinsi extends AbstractWilayah {
  @OneToMany(() => Kabupaten, kabupaten => kabupaten.provinsi)
  kabupaten: Kabupaten[];

  @OneToMany(() => Instansi, instansi => instansi.provinsi)
  instansi: Instansi[];
}
