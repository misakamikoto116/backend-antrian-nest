import { Module, HttpModule } from '@nestjs/common';
import { LayananService } from './layanan.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Layanan } from './layanan.entity';
import { LoketModule } from '@entities/loket/loket.module';
import { UserModule } from '@entities/user/user.module';
import { AntrianModule } from '@entities/antrian/antrian.module';
import { RethinkDbModule } from '@modules/rethink-db/rethink-db.module';
import { LayananCrudService } from './layanan-crud.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Layanan]),
    LoketModule,
    UserModule,
    AntrianModule,
  ],
  providers: [LayananService, LayananCrudService],
  exports: [
    LayananService,
    LayananCrudService,
    TypeOrmModule.forFeature([Layanan]),
  ],
})
export class LayananModule {}
