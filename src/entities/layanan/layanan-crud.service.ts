import { Layanan } from './layanan.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';

export class LayananCrudService extends TypeOrmCrudService<Layanan> {
  constructor(@InjectRepository(Layanan) repo: Repository<Layanan>) {
    super(repo);
  }
}
