import { Antrian } from '@entities/antrian/antrian.entity';
import { Instansi } from '@entities/instansi/instansi.entity';
import { Loket } from '@entities/loket/loket.entity';
import { DateStringValidation } from '@system/decorators/validation/datestring.validation';
import { IsBoolean, IsEmpty, IsString, Validate } from 'class-validator';
import {
  Column,
  Entity,
  JoinTable,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Printer } from '@entities/printer/printer.entity';
import { Anjungan } from '@entities/anjungan/anjungan.entity';

@Entity()
export class Layanan {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nama: string;

  @Column()
  keterangan: string;

  @Column()
  registrasiOnline: boolean;

  @ManyToOne(type => Instansi, instansi => instansi.layanan)
  @JoinTable()
  instansi: Instansi;

  @OneToMany(() => Loket, loket => loket.layanan)
  loket: Loket[];

  @Column()
  estimasiWaktuPelayanan: number;

  @Column()
  jamBukaPelayanan: string;

  @Column()
  jamTutupPelayanan: string;

  @OneToMany(() => Antrian, antrian => antrian.layanan)
  antrian: Antrian;
}
