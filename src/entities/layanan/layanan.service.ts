import {
  BadRequestException,
  Inject,
  Injectable,
  NotFoundException,
  Scope,
  OnApplicationBootstrap,
  Logger,
} from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { InjectRepository } from '@nestjs/typeorm';
import { paginate, Pagination } from 'nestjs-typeorm-paginate';
import { Repository, Like } from 'typeorm';

import { Layanan } from './layanan.entity';
import { paginateDefaultOptions } from '@system/helpers/paginateDefaultOptions';
import { PaginationOption } from '@system/interfaces/PaginationOption';
import { FastifyRequest } from 'fastify';
import { IncomingMessage } from 'http';
import { LoketService } from '@entities/loket/loket.service';
import { UsersService } from '@entities/user/user.service';
import { AntrianService } from '@entities/antrian/antrian.service';
import { RequestWithUser } from '@system/interfaces/request-with-user';
import { TakeQueueDto } from '@modules/mobile/mobile-layanan/dtos/take-queue.dto';
import { RethinkDbService } from '@modules/rethink-db/rethinkdb.provider';
import { Antrian } from '@entities/antrian/antrian.entity';
import { Loket } from '@entities/loket/loket.entity';

@Injectable()
export class LayananService {
  searchService(namaLayanan: string, page: number): Promise<Pagination<any>> {
    const query = this.modelRepository
      .createQueryBuilder('layanan')
      .innerJoin('layanan.instansi', 'instansi')
      .addSelect('layanan.nama')
      .addSelect('layanan.id')
      .addSelect('layanan.instansi')
      .addSelect('instansi.nama')
      .addSelect('instansi.logo')
      .leftJoinAndSelect('instansi.provinsi', 'provinsi')
      .leftJoinAndSelect('instansi.kabupaten', 'kabupaten')
      .leftJoinAndSelect('instansi.kecamatan', 'kecamatan')
      .leftJoinAndSelect('instansi.kelurahan', 'kelurahan')
      .where('layanan.nama LIKE :nama', { nama: namaLayanan })
      .innerJoin('layanan.loket', 'loket', 'loket.aktif = :aktif', {
        aktif: true,
      })
      .andWhere('layanan.registrasiOnline = 1');
    return paginate(query, {
      page,
      limit: 15,
    }).then(res => {
      const items = res.items.map(
        // tslint:disable-next-line:variable-name
        ({
          id,
          nama,
          jamBukaPelayanan: jamBuka,
          jamTutupPelayanan: jamTutup,
          instansi: {
            // tslint:disable-next-line:variable-name
            nama: nama_instansi,
            logo,
            kelurahan,
            kecamatan,
            kabupaten,
            provinsi,
          },
        }) => {
          let wilayah;
          if (kelurahan) {
            wilayah = 'Kelurahan ' + kelurahan.name;
          }
          if (kecamatan) {
            wilayah = 'Kecamatan ' + kecamatan.name;
          }
          if (kabupaten) {
            wilayah = 'Kabupaten ' + kabupaten.name;
          }
          if (provinsi) {
            wilayah = 'Provinsi ' + provinsi.name;
          }
          return {
            id,
            nama,
            nama_instansi,
            wilayah,
            logo,
            jamBuka,
            jamTutup,
          };
        },
      );
      return { ...res, items };
    });
  }
  constructor(
    @InjectRepository(Layanan)
    private readonly modelRepository: Repository<Layanan>,
    @InjectRepository(Loket)
    private readonly loketRepo: Repository<Loket>,
    @InjectRepository(Antrian)
    private readonly antrianRepo: Repository<Antrian>,
    private readonly loketService: LoketService,
    private readonly userService: UsersService,
    private readonly antrianService: AntrianService,
  ) { }
  async getItems(
    request: FastifyRequest,
    options: PaginationOption = {},
  ): Promise<Pagination<Layanan>> {
    const defaultOptions = paginateDefaultOptions(request, options);

    return await paginate(this.modelRepository, defaultOptions, {
      relations: ['instansi'],
    });
  }
  async storeItems(request: Layanan): Promise<Layanan> {
    try {
      const model = await this.modelRepository.save(request);
      return model;
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  async getSingleItem(itemId: string): Promise<Layanan> {
    const model = await this.findOne(itemId);

    return model;
  }

  async UpdateItem(request: Layanan, itemId: string): Promise<void> {
    try {
      //   const layanan:Layanan = await this.findOne(itemId);
      await this.modelRepository.update(itemId, request);
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  async deleteItem(itemId: string): Promise<void> {
    try {
      await this.loketRepo
        .find({
          where: {
            layanan: {
              id: itemId,
            },
          },
        })
        .then(res => {
          if (res && res.length > 0) {
            throw new Error('Data have relationship with data loket');
          }
        });
      await this.antrianRepo
        .find({
          where: {
            layanan: {
              id: itemId,
            },
          },
        })
        .then(res => {
          if (res && res.length > 0) {
            throw new Error('Data have relationship with data antrian');
          }
        });
      await this.modelRepository.delete(itemId);
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  // helper
  async findOne(id: string | number): Promise<Layanan> {
    const model = await this.modelRepository.findOne(id, {
      relations: ['instansi'],
    });
    return model;
  }

  async topTen(): Promise<any[]> {
    const layanans = await this.modelRepository
      .createQueryBuilder('layanan')
      .innerJoinAndSelect('layanan.instansi', 'instansi')
      .leftJoinAndSelect('instansi.provinsi', 'provinsi')
      .leftJoinAndSelect('instansi.kabupaten', 'kabupaten')
      .leftJoinAndSelect('instansi.kecamatan', 'kecamatan')
      .leftJoinAndSelect('instansi.kelurahan', 'kelurahan')
      .innerJoin('layanan.loket', 'loket', 'loket.aktif = :aktif', {
        aktif: true,
      })
      .andWhere('layanan.registrasiOnline = 1')
      .take(10)
      .getMany();
    return layanans.map(service => {
      let wilayah;
      if (service.instansi.kelurahan) {
        wilayah = 'Kelurahan ' + service.instansi.kelurahan.name;
      }
      if (service.instansi.kecamatan) {
        wilayah = 'Kecamatan ' + service.instansi.kecamatan.name;
      }
      if (service.instansi.kabupaten) {
        wilayah = 'Kabupaten ' + service.instansi.kabupaten.name;
      }
      if (service.instansi.provinsi) {
        wilayah = 'Provinsi ' + service.instansi.provinsi.name;
      }
      return {
        nama: service.nama,
        id: service.id,
        wilayah,
        nama_instansi: service.instansi.nama,
        logo: service.instansi.logo,
        jamBuka: service.jamBukaPelayanan,
        jamTutup: service.jamTutupPelayanan,
      };
    });
  }

  async getListLoket(layananId: number, date: string): Promise<any> {
    const loket = await this.loketService.getLoketByLayanan(layananId, date);
    const sisaAntrian = await this.loketService.getSisaAntrianLayanan(
      layananId,
      date,
    );
    return {
      id: date + '-' + layananId,
      sisaAntrian,
      loket,
      date,
    };
  }

  async takeQueue(request: RequestWithUser, userInput: TakeQueueDto) {
    const user = await this.userService.lastQueueUser(request.user.id);
    if (user !== undefined) {
      throw new BadRequestException(`User ${user.nama} masih dalam antrian`);
    }

    return await this.antrianService.takeQueueOnline(
      request.user,
      userInput.layananId,
    );
  }
}
