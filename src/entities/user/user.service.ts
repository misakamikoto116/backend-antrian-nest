import { TokenFCM as TFCM } from '@entities/user/tokenFcm.entity';
import { UpdateDto } from '@modules/mobile/mobile-user/dto/update.dto';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { todayStringFormated } from '@system/helpers/nowStringFormated';
import { Repository, DeepPartial, Not } from 'typeorm';

import { User } from './user.entity';
import { isArray } from 'util';
import { Role } from '@entities/role/role.entity';

@Injectable()
export class UsersService {
  isNoKtpUsed(id: number, noKTP: string) {
    return this.URepo.findOne({
      where: {
        id: Not(id),
        noKTP,
      },
    });
  }
  findOneWithRelation(id: number, relations: string[]): any {
    return this.URepo.findOneOrFail(id, {
      relations,
    });
  }
  constructor(
    @InjectRepository(User) private readonly URepo: Repository<User>,
    @InjectRepository(TFCM) private readonly TRepo: Repository<TFCM>,
    @InjectRepository(Role) private readonly roleRepo: Repository<Role>,
  ) {}
  async findOneByNoHp(noHp: string): Promise<User | undefined> {
    const dataUser: User = await this.URepo.findOne({
      where: { noHp },
    });
    return dataUser || null;
  }
  async createUser(user: User): Promise<User> {
    try {
      user.roles = await this.roleRepo.find({ where: { title: 'User' } });
      const userResult: User = await this.URepo.save(user);
      return userResult;
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }
  findByEmail(email: string): Promise<User> {
    return this.URepo.findOne({
      where: {
        email,
      },
    });
  }
  findOne(userId: number): Promise<User> {
    return this.URepo.findOneOrFail(userId, {
      join: {
        alias: 'user',
        leftJoinAndSelect: {
          roles: 'user.roles',
          petugas: 'user.petugas',
          instansi: 'petugas.instansi',
        },
      },
    });
  }
  async;
  updateUser(updateData: DeepPartial<User>, user: User) {
    return this.URepo.save(this.URepo.merge(user, updateData));
    //  return await this.userRepository.update(id,updateData);
  }
  addFcm(user: User, fcm: string): Promise<User> {
    const token = new TFCM();
    token.token = fcm;
    token.user = { id: user.id } as User;
    this.TRepo.save(token);
    if (isArray(user.tokenFCM)) {
      user.tokenFCM.push(token);
    } else {
      user.tokenFCM = [token];
    }

    return this.URepo.save(user);
  }
  lastQueueUser(id: number): Promise<User> {
    return this.URepo.createQueryBuilder('user')
      .leftJoin('user.antrianUser', 'antrianU', 'antrianU.isServed = :isServed')
      .innerJoin(
        'antrianU.antrian',
        'antrian',
        'Date(antrian.tanggal) = Date(:now)',
      )
      .where('user.id = :id')
      .setParameters({
        id,
        isServed: false,
        now: todayStringFormated(),
      })
      .getOne();
  }
}
