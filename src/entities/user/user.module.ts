import { TokenFCM } from '@entities/user/tokenFcm.entity';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { User } from './user.entity';
import { UsersService } from './user.service';
import { Role } from '@entities/role/role.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User, TokenFCM, Role])],
  providers: [UsersService],
  exports: [UsersService, TypeOrmModule.forFeature([User, TokenFCM])],
})
export class UserModule {}
