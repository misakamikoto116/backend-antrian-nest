import { AntrianUser } from '@entities/antrian/antrian-user.entity';
import { TokenFCM } from '@entities/user/tokenFcm.entity';
import { Role } from '@entities/role/role.entity';
import {
  BeforeInsert,
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  Unique,
  OneToOne,
} from 'typeorm';
import { Petugas } from '@entities/petugas/petugas.entity';

@Entity()
@Unique(['noKTP', 'noHp'])
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: () => 'NULL', nullable: true })
  foto: string;

  @Column()
  namaDepan: string;

  @Column()
  namaBelakang: string;

  @Column()
  noKTP: string;

  @Column({ default: null })
  noHp: string;

  @Column({
    type: 'date',
  })
  tanggalLahir: string;

  @Column()
  tempatLahir: string;

  @Column({ default: null })
  email: string;

  @OneToMany(() => TokenFCM, tokenfcm => tokenfcm.user)
  tokenFCM: TokenFCM[];

  @OneToMany(() => AntrianUser, antrianUser => antrianUser.user)
  antrianUser: AntrianUser[];

  @OneToMany(() => AntrianUser, antrianUser => antrianUser.petugas)
  terlayanin: AntrianUser[];

  @ManyToMany(() => Role, role => role.users)
  @JoinTable()
  roles: Role[];

  @OneToOne(() => Petugas, petugas => petugas.user)
  petugas: Petugas;

  countActiveQueue: number;

  get nama(): string {
    return `${this.namaDepan} ${this.namaBelakang}`;
  }

  toPlain(): object {
    return Object.assign({}, this);
  }

  @BeforeInsert()
  handleBeforeInsert() {
    if (this.roles) {
      if (this.roles.length === 0) {
        this.roles.push({
          id: 3,
        } as Role);
      }
    } else {
      this.roles = [
        {
          id: 3,
        } as Role,
      ];
    }
  }
}
