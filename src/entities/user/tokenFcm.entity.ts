import { User } from '@entities/user/user.entity';
import {
  ManyToOne,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  JoinTable,
} from 'typeorm';
@Entity()
export class TokenFCM {
  @PrimaryGeneratedColumn()
  id: number;

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @ManyToOne(type => User, user => user.tokenFCM)
  @JoinTable()
  user: User;

  @Column({ name: 'token' })
  // tslint:disable-next-line: variable-name
  token: string;
}
