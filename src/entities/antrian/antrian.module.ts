import { Layanan } from '@entities/layanan/layanan.entity';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AntrianUser } from './antrian-user.entity';
import { Antrian } from './antrian.entity';
import { AntrianService } from './antrian.service';

@Module({
  imports: [TypeOrmModule.forFeature([Antrian, AntrianUser, Layanan])],
  exports: [AntrianService, TypeOrmModule.forFeature([Antrian, AntrianUser])],
  providers: [AntrianService],
})
export class AntrianModule {}
