import { Layanan } from '@entities/layanan/layanan.entity';
import { Loket } from '@entities/loket/loket.entity';
import {
  AfterUpdate,
  Column,
  Entity,
  getRepository,
  JoinTable,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
  AfterInsert,
} from 'typeorm';

import { AntrianUser } from './antrian-user.entity';
import { Anjungan } from '@entities/anjungan/anjungan.entity';

@Entity()
export class Antrian {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nomor: number;

  @Column()
  tanggal: string;

  @Column()
  estimasiPemanggilan: number;

  @Column()
  terpanggil: boolean;

  @Column({ type: 'datetime', default: null })
  waktuDipanggil: string;

  @ManyToOne(() => Loket, loket => loket.layanan)
  loket: Loket;

  @ManyToOne(() => Layanan, layanan => layanan.antrian)
  layanan: Layanan;

  @ManyToOne(() => Anjungan, anjungan => anjungan.antrian)
  anjungan: Anjungan;

  @OneToOne(() => AntrianUser, antrianUser => antrianUser.antrian)
  @JoinTable()
  antrianUser: AntrianUser;

  sisaAntrian: number = 0;

  @AfterUpdate()
  async updateAntrianUser(): Promise<void> {
    if (![undefined, null].includes(this.antrianUser)) {
      this.antrianUser.isServed = true;
      await getRepository(AntrianUser).save(this.antrianUser);
    }
  }
}
