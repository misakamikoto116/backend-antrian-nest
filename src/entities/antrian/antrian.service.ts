import { Layanan } from '@entities/layanan/layanan.entity';
import { Loket } from '@entities/loket/loket.entity';
import { UpdateFeedBackAntrianDto } from '@modules/mobile/mobile-antrian/dtos/update-feedback-antrian.dto';
import { User } from '@entities/user/user.entity';
import {
  Injectable,
  UseGuards,
  Logger,
  ConflictException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { todayStringFormated } from '@system/helpers/nowStringFormated';
import { Repository, getConnection } from 'typeorm';

import { AntrianUser } from './antrian-user.entity';
import { Antrian } from './antrian.entity';
import moment = require('moment');

interface LastAntrianLoket {
  loket_id: number;
  last_antrian: number;
}

@Injectable()
export class AntrianService {
  getLastQueueLayanan(layananId: number): Promise<number> {
    return new Promise((resolve, reject) => {
      this.antrianRepository
        .createQueryBuilder('antrian')
        .select('MAX(antrian.nomor)', 'lastQueue')
        .where('antrian.layananId = :layananId')
        .andWhere('date(antrian.tanggal) = date(:tanggal)')
        .andWhere('antrian.terpanggil = :terpanggil')
        .setParameters({
          tanggal: todayStringFormated(),
          terpanggil: true,
          layananId,
        })
        .getRawOne()
        .then(
          (res: any) => {
            resolve(res.lastQueue);
          },
          err => {
            reject(new ConflictException(err));
          },
        );
    });
  }
  createUserQueue(layanan: Layanan, user: User) {
    return new Promise<Antrian>((resolveMain, rejectMain) => {
      getConnection()
        .transaction(manager => {
          const EntityAntrian = new Antrian();
          const EntityAntrianUser = new AntrianUser();
          return new Promise<Antrian>((resolve, reject) => {
            manager
              .save(
                this.antrianRepository.merge(EntityAntrian, {
                  tanggal: todayStringFormated(),
                  layanan,
                  nomor: 0,
                  estimasiPemanggilan: 0,
                  terpanggil: false,
                }),
              )
              .then(
                antrian => {
                  manager
                    .save(
                      this.antrianUserRepository.merge(EntityAntrianUser, {
                        user,
                        isServed: false,
                        antrian,
                      }),
                    )
                    .then(
                      () => resolve(antrian),
                      err => {
                        reject(new ConflictException(err));
                      },
                    );
                },
                err => {
                  reject(new ConflictException(err));
                },
              );
          });
        })
        .then(
          antrian => {
            this.antrianRepository
              .createQueryBuilder('antrian')
              .leftJoinAndSelect('antrian.layanan', 'layanan')
              .leftJoinAndSelect('layanan.instansi', 'instansi')
              .leftJoinAndSelect('antrian.antrianUser', 'antrianUser')
              .leftJoinAndSelect('antrianUser.user', 'user')
              .where({
                id: antrian.id,
              })
              .getOne()
              .then(resolveMain, err => {
                rejectMain(new ConflictException(err));
              });
          },
          err => {
            rejectMain(new ConflictException(err));
          },
        );
    });
  }
  isUserInQueue(user: User) {
    return new Promise<boolean>((resolve, reject) => {
      this.antrianUserRepository
        .createQueryBuilder('antrianUser')
        .innerJoin(
          'antrianUser.antrian',
          'antrian',
          'date(antrian.tanggal) = date(:tanggal) AND terpanggil = :terpanggil',
        )
        .innerJoin('antrianUser.user', 'user', 'user.id = :userId')
        .where('antrianUser.isServed = :isServed')
        .setParameters({
          tanggal: todayStringFormated(),
          userId: user.id,
          terpanggil: false,
          isServed: false,
        })
        .getCount()
        .then(
          count => resolve(count > 0),
          err => {
            reject(new ConflictException(err));
          },
        );
    });
  }
  constructor(
    @InjectRepository(Antrian)
    private readonly antrianRepository: Repository<Antrian>,
    @InjectRepository(AntrianUser)
    private readonly antrianUserRepository: Repository<AntrianUser>,
    @InjectRepository(Layanan)
    private readonly layananRepository: Repository<Layanan>,
  ) {}
  async getWaitingQueue(id: number, date: string): Promise<number> {
    return await this.antrianRepository
      .createQueryBuilder('a')
      .where(
        'a.layananId = :id AND ' +
          'DATE(a.tanggal) = DATE(:tanggal) AND ' +
          'a.terpanggil = :terlayani',
      )
      .setParameters({
        tanggal: date,
        id,
        terlayani: false,
      })
      .getCount();
  }

  async getLastQueueInLokets(
    ids: number[],
    date: string,
  ): Promise<LastAntrianLoket[]> {
    return await this.antrianRepository
      .createQueryBuilder('antrian')
      .where('antrian.loketId IN (:ids) ', { ids })
      .andWhere('DATE(antrian.tanggal) = DATE(:date)', {
        date,
      })
      .andWhere('antrian.terpanggil = :terpanggil', {
        terpanggil: true,
      })
      .groupBy('antrian.layananId')
      .orderBy('antrian.terpanggil', 'DESC')
      .limit(ids.length)
      .leftJoinAndSelect('antrian.loket', 'loket')
      .getMany()
      .then(items =>
        items.map(item => ({
          loket_id: item.loket.id,
          last_antrian: item.nomor,
        })),
      );
  }
  async takeQueueOnline(user: User, layananId: number): Promise<Antrian> {
    const layanan = await this.layananRepository.findOne(layananId, {
      relations: ['instansi'],
    });
    const antrianUser = await this.antrianUserRepository.create({
      user,
      isServed: false,
    });
    await this.antrianUserRepository.save(antrianUser);
    const antrian = this.antrianRepository.create();
    const res = await this.antrianRepository.save(antrian);
    const dataFromDB = await this.antrianRepository.findOne(res.id, {
      select: ['nomor'],
    });
    res.nomor = dataFromDB.nomor;
    return res;
  }
  getAntrianNow(userId: number): Promise<Antrian> {
    return new Promise<Antrian>((resolve, reject) => {
      this.antrianUserRepository
        .createQueryBuilder('antrianUser')
        .where('userId = :userId and isServed = :isServed')
        .innerJoin(
          'antrianUser.antrian',
          'antrian',
          'antrian.terpanggil = :terpanggil',
        )
        .innerJoin('antrian.layanan', 'layanan')
        .leftJoin('antrian.loket', 'loket')
        .innerJoin('layanan.instansi', 'instansi')
        .select([
          'antrianUser.id',
          'antrianUser.antrian',
          'antrian.nomor',
          'antrian.estimasiPemanggilan',
          'antrian.layanan',
          'layanan.nama',
          'layanan.id',
          'layanan.keterangan',
          'layanan.instansi',
          'instansi.nama',
          'antrian.loket',
        ])
        .setParameters({ userId, isServed: 0, terpanggil: 0 })
        .limit(1)
        .orderBy('antrian.id', 'DESC')
        .getOne()
        .then(ret => {
          if (ret) {
            resolve(ret.antrian);
          } else {
            resolve(undefined);
          }
        })
        .catch(err => {
          reject(new ConflictException(err));
        });
    });
  }
  async getHistoryQueue(id: number): Promise<any> {
    return await this.antrianUserRepository
      .createQueryBuilder('antrianUser')
      .innerJoin(
        'antrianUser.antrian',
        'antrian',
        'antrian.terpanggil = :terpanggil',
        { terpanggil: true },
      )
      .innerJoin('antrianUser.user', 'user', 'user.id = :id', { id })
      .leftJoin('antrian.layanan', 'layanan')
      .leftJoin('antrian.loket', 'loket')
      .leftJoin('layanan.instansi', 'instansi')
      .leftJoin('antrianUser.petugas', 'petugas')
      .addSelect('antrian.nomor')
      .addSelect('antrian.tanggal')
      .addSelect('IFNULL(antrian.waktuDiPanggil, 0)', 'antrian.waktuDiPanggil')
      .addSelect('antrian.loket')
      .addSelect('loket.layanan')
      .addSelect('layanan.nama')
      .addSelect('layanan.instansi')
      .addSelect('instansi.nama')
      .addSelect('antrianUser.petugas')
      .addSelect('petugas.namaDepan')
      .addSelect('petugas.namaBelakang')
      .orderBy('antrian.tanggal', 'DESC')
      .getMany();
  }
  updateFeedback(
    userInput: UpdateFeedBackAntrianDto,
    userId: number,
  ): Promise<any> {
    const { id, ...updateData } = userInput;
    return new Promise((resolve, reject) => {
      this.antrianUserRepository
        .update(
          {
            id,
            user: {
              id: userId,
            },
          },
          updateData,
        )
        .then(async x => {
          resolve(updateData);
          // const resp = await this.antrianUserRepository
          //   .createQueryBuilder('antUser')
          //   .where('antUser.id = :id')
          //   .andWhere('antUser.isServed = :isServed')
          //   .innerJoin('antUser.user', 'user', 'user.id = :userId')
          //   .innerJoin('antUser.antrian', 'antrian')
          //   .leftJoin('antUser.petugas', 'petugas')
          //   .leftJoin('antrian.loket', 'loket')
          //   .leftJoin('antrian.layanan', 'layanan')
          //   .addSelect('antUser.FeedbackDesc')
          //   .addSelect('antUser.FeedbackRating')
          //   .addSelect('antUser.isServed')
          //   .addSelect('antUser.antrian')
          //   .addSelect('antrian.nomor')
          //   .addSelect('antrian.tanggal')
          //   .addSelect('antrian.waktuDipanggil')
          //   .addSelect('antrian.loket')
          //   .addSelect('antrian.layanan')
          //   .addSelect('layanan.nama')
          //   .addSelect('antUser.petugas')
          //   .addSelect('petugas.namaDepan')
          //   .addSelect('petugas.namaBelakang')
          //   .addSelect('loket.nama')
          //   .setParameters({
          //     id,
          //     isServed: true,
          //     userId,
          //   })
          //   .getOne()
          //   .then()
          //   .catch(reject);
        })
        .catch(err => {
          reject(new ConflictException(err));
        });
    });
  }
  async callNextAntrian(
    petugasId: number,
    layananId: number,
    loketId: number,
  ): Promise<Antrian> {
    const antrian = await this.antrianRepository
      .createQueryBuilder('antrian')
      .leftJoinAndSelect('antrian.antrianUser', 'antrianUser')
      .leftJoinAndSelect('antrianUser.petugas', 'petugas')
      .leftJoinAndSelect('antrianUser.user', 'user')
      .where('antrian.terpanggil = :terpanggil')
      .andWhere('antrian.layananId = :layananId')
      .andWhere('DATE(antrian.tanggal) = DATE(:tanggal)')
      .andWhere('ISNULL(antrian.loketId)')
      .setParameters({
        terpanggil: false,
        layananId,
        tanggal: todayStringFormated(),
      })
      .getOne();
    if (antrian !== undefined) {
      antrian.terpanggil = true;
      antrian.waktuDipanggil = moment().format('YYYY-MM-DD hh:mm:ss');
      antrian.loket = { id: loketId } as Loket;
      const antrianUser = antrian.antrianUser;
      if (![undefined, null].includes(antrianUser)) {
        antrianUser.petugas = {
          id: petugasId,
        } as User;

        await this.antrianUserRepository.save(antrianUser);
      }
      return await this.antrianRepository.save(antrian);
    }
    return antrian;
  }
}
