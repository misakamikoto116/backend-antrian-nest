import { User } from '@entities/user/user.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { Antrian } from './antrian.entity';

@Entity()
export class AntrianUser {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, user => user.antrianUser)
  user: User;

  @OneToOne(() => Antrian, antrian => antrian.antrianUser)
  @JoinColumn()
  antrian: Antrian;

  @ManyToOne(() => User, user => user.terlayanin)
  petugas: User;

  @Column({ default: null })
  FeedbackDesc: string;

  @Column({ default: null })
  FeedbackRating: number;

  @Column({ default: false })
  isServed: boolean;
}
