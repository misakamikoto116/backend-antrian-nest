import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  BeforeUpdate,
  ManyToOne,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { BaseEntity } from '@system/base.entity';
import { Anjungan } from '@entities/anjungan/anjungan.entity';
import * as uniqid from 'uniqid';
import { Instansi } from '@entities/instansi/instansi.entity';

@Entity()
export class Printer extends BaseEntity {
  @Column()
  code: string;

  @ManyToOne(() => Instansi, instansi => instansi.printer)
  instansi: Instansi;

  @OneToOne(() => Anjungan, anjungan => anjungan.printer)
  anjungan: Anjungan;

  handleBeforeInsert() {
    super.handleBeforeInsert();
    this.code = uniqid('thor');
  }
}
