import { Module } from '@nestjs/common';
import { Printer } from './printer.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Printer])],
  exports: [TypeOrmModule.forFeature([Printer])],
})
export class PrinterModule {}
