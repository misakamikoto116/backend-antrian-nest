import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Petugas } from './petugas.entity';
@Module({
  imports: [TypeOrmModule.forFeature([Petugas])],
  exports: [TypeOrmModule.forFeature([Petugas])],
})
export class PetugasModule {}
