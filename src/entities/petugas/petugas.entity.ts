import {
  Entity,
  OneToOne,
  JoinColumn,
  PrimaryGeneratedColumn,
  ManyToOne,
} from 'typeorm';
import { Instansi } from '@entities/instansi/instansi.entity';
import { User } from '@entities/user/user.entity';

@Entity()
export class Petugas {
  @PrimaryGeneratedColumn()
  id: number;
  @OneToOne(() => User, user => user.petugas)
  @JoinColumn()
  user: User;
  @ManyToOne(() => Instansi, instansi => instansi.petugas)
  instansi: Instansi;
}
