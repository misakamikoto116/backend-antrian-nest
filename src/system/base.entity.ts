import {
  Column,
  BeforeInsert,
  BeforeUpdate,
  PrimaryGeneratedColumn,
} from 'typeorm';
import moment = require('moment');

export class BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: string;
  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: string;

  @BeforeInsert()
  handleBeforeInsert() {
    this.createdAt = timestamp();
    this.updatedAt = timestamp();
  }
  @BeforeUpdate()
  handleBeforeUpdate() {
    this.updatedAt = timestamp();
  }
}
function timestamp(): string {
  return moment().format('YYYY-MM-DD HH:mm:ss');
}
