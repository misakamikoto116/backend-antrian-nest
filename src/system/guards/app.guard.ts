import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { Observable } from 'rxjs';

import { DeviceGuard } from './device.guard';
import { JwtHttpGuard } from './jwt-http.guard';
import { systemConstant } from '@system/constants/system.constant';

@Injectable()
export class AppGuard implements CanActivate {
  constructor(
    private readonly reflector: Reflector,
    private readonly jwtServive: JwtService,
  ) {}
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const MetaHandler =
      this.reflector.get<string[]>('unGuard', context.getHandler()) || [];
    const MetaClass =
      this.reflector.get<string[]>('unGuard', context.getClass()) || [];
    const MetaData = [...MetaClass, ...MetaHandler].filter(
      (v, i, s) => s.indexOf(v) === i,
    );

    return new Observable(subs => {
      subs.next(true);
      if (!MetaData.includes(systemConstant.DEVICE_GUARD)) {
        subs.next(new DeviceGuard(this.reflector).canActivate(
          context,
        ) as boolean);
      }
      if (!MetaData.includes(systemConstant.JWT_HTTP_GUARD)) {
        subs.next(new JwtHttpGuard(this.jwtServive).canActivate(
          context,
        ) as boolean);
      }
      subs.complete();
    });
  }
}
