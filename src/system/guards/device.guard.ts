import { CanActivate, ExecutionContext, Inject, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';

@Injectable()
export class DeviceGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const url: string = context.switchToHttp().getRequest().req.url as string;
    const classDecorator: string[] = this.reflector.get<string[]>(
      'urlProtection',
      context.getClass(),
    );
    const methodDecorator: string[] = this.reflector.get<string[]>(
      'urlProtection',
      context.getHandler(),
    );
    const classAllowed =
      classDecorator !== undefined
        ? classDecorator.map(protector => url.includes(protector))
        : [];
    const methodAllowed =
      methodDecorator !== undefined
        ? methodDecorator.map(protector => url.includes(protector))
        : [];
    if (
      (classAllowed.length > 0 && classAllowed.find(b => b)) ||
      methodAllowed.length === 0
    ) {
      return (
        (methodAllowed.length > 0 && methodAllowed.find(b => b)) ||
        methodAllowed.length === 0
      );
    }
    return false;
  }
}
