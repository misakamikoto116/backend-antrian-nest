import {
  BadRequestException,
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { Observable } from 'rxjs';

@Injectable()
export class JwtHttpGuard implements CanActivate {
  constructor(private readonly jwtServive: JwtService) {}
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const req = context.switchToHttp().getRequest<Request>();
    const token = this.extractToken(req);
    let decryptedToken;
    try {
      decryptedToken = this.jwtServive.verify(token);
    } catch (error) {
      throw new UnauthorizedException(error.message);
    }
    if (!decryptedToken) {
      throw new UnauthorizedException('Token is expired or not falid');
    }
    Object.defineProperty(req, 'user', {
      value: decryptedToken,
      writable: false,
    });
    this.extractToken(req);

    return true;
  }
  extractToken(Request: any): string {
    const { authorization } = Request.headers as {
      authorization: string;
    };
    if (authorization === undefined) {
      throw new BadRequestException('Token not present');
    }
    const prefix = 'Bearer ';
    if (!authorization.includes(prefix)) {
      throw new BadRequestException('Token format not valid');
    }
    const extractedToken = authorization.split(prefix);
    if (extractedToken.length < 1) {
      throw new BadRequestException('Token not present');
    }
    return extractedToken.pop();
  }
}
