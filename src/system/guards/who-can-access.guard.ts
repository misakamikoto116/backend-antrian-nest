import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { RequestWithUser } from '@system/interfaces/request-with-user';
import { Observable } from 'rxjs';

@Injectable()
export class WhoCanAccessGuard implements CanActivate {
  private readonly roles: string[];
  constructor(...roles: string[]) {
    this.roles = roles;
  }
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const req = context.switchToHttp().getRequest() as RequestWithUser;
    if (req.user !== undefined) {
      return req.user.roles
        .map(role => {
          return this.roles.includes(role.title);
        })
        .includes(true);
    }
    return false;
  }
}
