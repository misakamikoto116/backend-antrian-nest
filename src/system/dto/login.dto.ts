import { IsNotEmpty, IsString, IsEnum, ValidateIf } from 'class-validator';
import { MethodLogin } from './methodLogin';
export class LoginDto {
  @IsNotEmpty()
  @IsString()
  // @InDatabase([User])
  identity: string;

  @IsNotEmpty()
  @IsString()
  @IsEnum(MethodLogin)
  type: MethodLogin;

  @IsString()
  @ValidateIf(o => o.type === MethodLogin.PHONE)
  @IsNotEmpty()
  tokenFCM: string;
}
