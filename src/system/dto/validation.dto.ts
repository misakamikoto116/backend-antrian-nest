import { LoginDto } from './login.dto';
import { IsNotEmpty, IsNumber, IsString, Validate } from 'class-validator';
import { DateStringValidation } from '../decorators/validation/datestring.validation';

export class ValidationDto {
  @IsString()
  @IsNotEmpty()
  pin: string;

  @IsString()
  @IsNotEmpty()
  token: string;
}
