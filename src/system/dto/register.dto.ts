import { User } from '@entities/user/user.entity';
import {
  IsString,
  IsDefined,
  IsOptional,
  IsNotEmpty,
  IsEmail,
  Validate,
  ValidateIf,
} from 'class-validator';
import { UniqueInDatabase } from '@system/decorators/validation/unique.database.validation';
import { DateFormated } from '@system/decorators/validation/datestring.validation';

export class RegisterDto {
  @IsString()
  @IsDefined()
  @IsNotEmpty()
  namaDepan: string;

  @IsString()
  namaBelakang: string;

  @IsString()
  @IsDefined()
  @IsNotEmpty()
  @UniqueInDatabase(User)
  noKTP: string;

  @IsString()
  //  @IsDefined()
  @IsOptional()
  // @IsNotEmpty()
  // @UniqueInDatabase(User)
  noHp: string;

  @IsString()
  @IsDefined()
  @IsNotEmpty()
  @DateFormated('YYYY-MM-DD')
  tanggalLahir: string;

  @IsString()
  @IsDefined()
  @IsNotEmpty()
  tempatLahir: string;

  @IsString()
  @IsOptional()
  @IsEmail()
  @UniqueInDatabase(User)
  email: string;
}
