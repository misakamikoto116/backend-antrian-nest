import {
  BadRequestException,
  Inject,
  Injectable,
  NestMiddleware,
  Optional,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class JwtMiddleware implements NestMiddleware {
  constructor(
    private readonly jwtService: JwtService,
    @Optional()
    @Inject('PROVIDE_LINK_PATTERN')
    private readonly protectedLinkPattern: string = '',
  ) {}
  use(req: any, res: any, next: () => void): any {
    const isIncluded = req.url.includes(this.protectedLinkPattern);
    if (!isIncluded || this.protectedLinkPattern === '') {
      return next();
    }
    const token = this.extractToken(req);
    let decryptedToken;
    try {
      decryptedToken = this.jwtService.verify(token);
    } catch (error) {
      throw new UnauthorizedException(error.message);
    }
    if (!decryptedToken) {
      throw new UnauthorizedException('Token is expired or not falid');
    }
    Object.defineProperty(req, 'user', {
      value: decryptedToken,
      writable: false,
    });
    return next();
  }
  extractToken(Request: any): string {
    const { authorization } = Request.headers as {
      authorization: string;
    };
    if (authorization === undefined) {
      throw new BadRequestException('Token not present');
    }
    const prefix = 'Bearer ';
    if (!authorization.includes(prefix)) {
      throw new BadRequestException('Token format not valid');
    }
    const extractedToken = authorization.split(prefix);
    if (extractedToken.length < 1) {
      throw new BadRequestException('Token not present');
    }
    return extractedToken.pop();
  }
}
