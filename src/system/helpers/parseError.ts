import { BadRequestException, Logger } from '@nestjs/common';
import { ValidationError } from 'class-validator';
interface ErrorParsed {
  field: string;
  message: string;
}
export function parseError(errors?: ValidationError[]): void {
  Logger.error(errors);
  if (errors === undefined) {
    throw new BadRequestException('Fail validate user input');
  }
  if (errors.length > 0) {
    throw new BadRequestException(
      errors.map(e => {
        let child: ValidationError;
        if (e.children.length > 0) {
          child = e.children.pop();
        }
        const constraints =
          child !== undefined ? child.constraints : e.constraints;
        const field =
          child !== undefined ? e.property + '.' + child.property : e.property;
        return {
          field,
          message: Object.keys(constraints)
            .map(key => constraints[key])
            .pop(),
        };
      }),
    );
  }
}
