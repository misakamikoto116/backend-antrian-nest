import * as URL from 'url';
const AppUrl = (url: string): string => {
  const baseUrl: string = process.env.APP_URL as string;
  return URL.resolve(baseUrl, url);
};
export { AppUrl };
