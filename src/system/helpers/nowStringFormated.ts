import moment = require('moment');

export function todayStringFormated(): any {
  return moment().format('YYYY-MM-DD');
}
