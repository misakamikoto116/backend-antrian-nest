import { AppUrl } from '@system/helpers/urlGenerator';
import { IPaginationOptions } from 'nestjs-typeorm-paginate';
import { PaginationOption } from '@system/interfaces/PaginationOption';
import { FastifyRequest } from 'fastify';
export function paginateDefaultOptions(
  request: FastifyRequest,
  options: PaginationOption,
): IPaginationOptions {
  const page = request.query.page === undefined ? 0 : request.query.page;
  return {
    limit: 10,
    page: options.page || page,
    route: AppUrl(request.req.url),
  };
}
