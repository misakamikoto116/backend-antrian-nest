export function env(key: string, def: any = null): any {
  const val = process.env[key];
  if (val !== undefined) {
    return def;
  }
  return val;
}
