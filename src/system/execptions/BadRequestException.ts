import { BadRequestException as BaseClass } from '@nestjs/common';
import { ValidationError } from 'class-validator';
import { isObject } from 'util';

export class BadRequestException extends BaseClass {
    constructor(message: ValidationError[]| any) {
        if (Array.isArray(message) && !message.map(item => item instanceof ValidationError).includes(false)) {
            const res = (message as ValidationError[]).map(e => {
                const constraints = e.children.length > 0  ? e.children.shift().constraints : e.constraints;
                return {
                field: e.property,
                message: Object.keys(constraints)
                    .map(key => constraints[key])
                    .shift(),
                };
            });
            super(res);
        } else {
            super(message);
        }

    }

}
