import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import { isObject } from 'util';
import dotObject = require('dot-object');
import { parseError } from '@system/helpers/parseError';

@Injectable()
export class ValidationPipe implements PipeTransform<any> {
  async transform(value: any, argMetadata: ArgumentMetadata) {
    const { metatype } = argMetadata;
    if (!metatype && !this.toValidate(metatype)) {
      return value;
    }
    if (!isObject(value)) {
      throw new BadRequestException('Requests must be json');
    }
    dotObject.object(value);

    const object = plainToClass(metatype, value);
    const errors = await validate(object);
    parseError(errors);
    return value;
  }

  private toValidate(metatype) {
    // tslint:disable-next-line: ban-types
    const types: Function[] = [String, Boolean, Number, Array, Object];
    return types.includes(metatype);
  }
}
