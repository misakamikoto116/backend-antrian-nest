import {
  ArgumentMetadata,
  Injectable,
  PipeTransform,
  ParseIntPipe,
} from '@nestjs/common';
import { getConnection, Repository } from 'typeorm';
import { BadRequestException } from '@system/execptions/BadRequestException';

@Injectable()
export class ParseIdItemPipe extends ParseIntPipe implements PipeTransform {
  private repo: Repository<any>;
  constructor(entity: any) {
    super();
    this.repo = getConnection().getRepository(entity);
  }
  transform(value: any, metadata: ArgumentMetadata) {
    return new Promise<number>((resolve, reject) => {
      super
        .transform(value, metadata)
        .then(val => {
          this.repo
            .findOne(val)
            .then(() => resolve(val))
            .catch(() =>
              reject(
                new BadRequestException(
                  'Data with indentifier  = ' + value + ' not found ',
                ),
              ),
            );
        })
        .catch(reject);
    });
  }
}
