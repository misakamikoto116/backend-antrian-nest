import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';
import { isNullOrUndefined } from 'util';

@Injectable()
export class ParsePagePipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata) {
    try {
      const page = parseInt(value, 0);
      if (isNullOrUndefined(page) || isNaN(page)) {
        return 1;
      }
      if (page < 0) {
        return 1;
      }
      return page;
    } catch (error) {
      return 1;
    }
  }
}
