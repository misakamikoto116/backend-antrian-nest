import { User } from '@entities/user/user.entity';

export interface RequestWithUser extends Request {
  user: User;
}
