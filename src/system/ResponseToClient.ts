interface MetaDataPagination {
  nextPage: string;
  prevPage: string;
  pageCount: number;
  total: number;
  itemCount: number;
}

export interface ResponseToClient <T> {
  metaData: {
    error: boolean;
    statusCode: number;
    message: string;
    paginate?: MetaDataPagination | object;
  };
  result: T|object|undefined;
}
