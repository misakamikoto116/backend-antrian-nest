import { Strategy, ExtractJwt } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { systemConstant } from '../constants/system.constant';
@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken,
      ignoreExpiration: false,
      secretOrKey: systemConstant.jwtSecreat,
    });
  }
  async validate(payload: any): Promise<any> {
    return { userId: payload.sub, username: payload.username };
  }
}
