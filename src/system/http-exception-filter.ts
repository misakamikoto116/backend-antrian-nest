import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  Logger,
} from '@nestjs/common';
import { FastifyReply } from 'fastify';
import { isArray } from 'util';

import { ResponseToClient } from './ResponseToClient';
import { isString } from '@sentry/utils';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  private logger = new Logger('HttpException');
  catch(exception: HttpException, host: ArgumentsHost): any {
    this.logger.error(exception.message, exception.stack);

    const exceptionResponse = exception.getResponse() as any;
    const httpResponse = host.switchToHttp().getResponse<Response>() as any;
    let message;
    if (isString(exception.message.message)) {
      message = exception.message.message;
    } else {
      const field = exception.message.message[0].field as string;

      const fieldFixed = field
        .trim()
        .split(/(?=[A-Z])/)
        .join('.')
        .split('.')
        .map((i, k) =>
          k === 0 ? i.substr(0, 1).toUpperCase() + i.substring(1) : i,
        )
        .join(' ');
      const msg = exception.message.message[0].message as string;
      const msgFixed = msg.replace(field, fieldFixed);
      message = `${fieldFixed} : ${msgFixed}`;
    }
    let errorContent;
    if (isArray(exception.message.message)) {
      errorContent = exception.message.message;
    } else {
      errorContent = [
        {
          field: exceptionResponse.error,
          message: exception.message.message,
        },
      ];
    }

    const res = {
      metaData: {
        error: true,
        statusCode: exception.getStatus(),
        statusMessage: exceptionResponse.error,
        message,
      },
      errorContent,
    };
    httpResponse.code(exception.getStatus()).send(res);

    // host.switchToHttp().getResponse().req.code().json({
    //   metaData:{
    //     statusCode: exception.getStatus(),
    //     message: exceptionResponse.message,
    //   },
    //   errorConrent:exception.message
    // });
  }
}
