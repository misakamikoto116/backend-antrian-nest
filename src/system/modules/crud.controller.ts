
import {
  BadRequestException,
  Body,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { JwtHttpGuard } from '@system/guards/jwt-http.guard';
import { plainToClassFromExist } from 'class-transformer';
import { ClassType } from 'class-transformer/ClassTransformer';
import { validate } from 'class-validator';
import { Pagination } from 'nestjs-typeorm-paginate';
import { isObject } from 'util';
import dob = require('dot-object');
import { CrudService } from './crud.service';
import { parseError } from '@system/helpers/parseError';

export abstract class CrudController<Entity, DTO> {
  private dto: DTO;
  constructor(
    private readonly modelService: CrudService<Entity, DTO>,
    dto: ClassType<DTO>,
    private readonly realtions: string[],
  ) {
    this.dto = new dto();
  }

  @Get()
  async fetchAllData(
    @Query('page') page: number = 1,
  ): Promise<Pagination<Entity>> {
    return Promise.resolve(('s' as unknown) as Pagination<Entity>);
    // return await this.modelService.list(this.realtions, page, 10);
  }
  @Get(':id')
  fetchOne(@Param('id') id: number): Promise<Entity> {
    return Promise.resolve(('s' as unknown) as Entity);
    // return this.modelService.findOne(id, this.realtions);
  }

  @Post()
  async postData(@Body() request: DTO): Promise<Entity> {
    if (!isObject(request)) {
      throw new BadRequestException('Body request must json');
    }
    const objectDto = await this.parseToObject(request);
    return await this.modelService.create(objectDto);
  }

  @Get(':id')
  async showData(@Param('id') id: number): Promise<Entity> {
    return await this.modelService.show(id);
  }

  @Put(':id')
  async updateData(
    @Param('id') id: number,
    @Body() request: any,
  ): Promise<Entity> {
    if (!isObject(request)) {
      throw new BadRequestException('Body request must json');
    }
    const objectDto = await this.parseToObject(request);
    return await this.modelService.update(id, objectDto);
  }

  async parseToObject(request: any): Promise<DTO> {
    dob.object(request);
    const objectDto = plainToClassFromExist(this.dto, request);
    const errors = await validate(objectDto);
    parseError(errors);
    return objectDto;
  }

  @Delete(':id')
  async deleteData(@Param('id') id: number): Promise<void> {
    return await this.modelService.delete(id);
  }
}
