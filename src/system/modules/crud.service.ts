import { Repository, Like } from 'typeorm';
import { Pagination, paginate } from 'nestjs-typeorm-paginate';
import { BadRequestException } from '@system/execptions/BadRequestException';

export abstract class CrudService<T, U> {
  findOne(id: number, relations: string[]): Promise<T> {
    return this.modelRepository.findOneOrFail(id, {
      relations,
    });
  }
  private entity: T;
  constructor(
    entity: new () => T,
    private readonly modelRepository: Repository<T>,
    private readonly request: any,
  ) {
    this.entity = new entity();
  }

  async list(
    relations: string[],
    page = 1,
    limit = 10,
  ): Promise<Pagination<T>> {
    const option = {
      page,
      limit,
      route: 'http://' + this.request.hostname + this.request.req.originalUrl,
    };
    const orderBy = {};
    return await paginate(this.modelRepository, option, {
      order: {
        ...orderBy,
      },
      ...this.filterBy(this.request),
      // relations,
    });
  }

  async create(data: U): Promise<T> {
    const entity = this.entity;
    const model: T = this.modelRepository.merge(entity, data);
    return await this.modelRepository.save(model);
  }

  async show(id: number): Promise<any> {
    const model = await this.modelRepository.findOne(id);
    this.dataNotFound(model);
    return await model;
  }

  async update(id: number, data: U): Promise<T> {
    const model: T = await this.modelRepository.findOne(id);
    this.dataNotFound(model);
    this.modelRepository.merge(model, data);
    return await this.modelRepository.save(model);
  }

  async delete(id: number): Promise<void> {
    await this.modelRepository.delete(id);
  }

  dataNotFound(model: T) {
    if (model === undefined) {
      throw new BadRequestException('Data tidak ditemukan');
    }

    return true;
  }

  filterBy(req: any): {} {
    return {
      code: Like(req.code),
      name: Like(req.name),
    };
  }
}
