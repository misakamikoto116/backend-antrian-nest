import {
  ValidationArguments,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationOptions,
  registerDecorator,
} from 'class-validator';
import { getRepository, DeepPartial } from 'typeorm';
import { isObject, isString } from 'util';

@ValidatorConstraint({ async: true })
export class InDatabaseValidation implements ValidatorConstraintInterface {
  validate(
    value: any,
    validationArguments?: ValidationArguments,
  ): Promise<boolean> {
    const validate = {};
    const cPropertyName = validationArguments.constraints[1];
    const propertyName =
      isString(cPropertyName) && cPropertyName !== ''
        ? cPropertyName
        : validationArguments.property;
    validate[propertyName] = value;

    const className = validationArguments.constraints[0];

    const query = getRepository(className)
      .createQueryBuilder('tableName')
      .where(`tableName.${propertyName} = :value`, { value });
    if (validationArguments.constraints.length === 3) {
      const optionalWhere = validationArguments.constraints[2];
      if (![null, undefined].includes(optionalWhere)) {
        Object.keys(optionalWhere).map(key => {
          const qpm = {};
          qpm[key] = optionalWhere[key];
          query.andWhere(`tableName.${key} = :${key}`, qpm);
        });
      }
    }
    return query.getOne().then(inDatabase => {
      return inDatabase ? true : false;
    });
  }
  defaultMessage({
    property,
    value,
    constraints,
  }: ValidationArguments): string {
    const propertyName = constraints.length > 1 ? constraints[1] : property;
    return `this key ${propertyName} with value ${value} not registered in database`;
  }
}
export function InDatabase(
  entity: any,
  field?: string,
  condition?: {},
  validationOptions?: ValidationOptions,
) {
  return (object: object, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [entity, field, condition],
      validator: InDatabaseValidation,
    });
  };
}
