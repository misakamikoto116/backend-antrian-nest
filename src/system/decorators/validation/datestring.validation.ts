import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
  ValidationOptions,
  registerDecorator,
} from 'class-validator';
import * as moment from 'moment';
@ValidatorConstraint({ async: false })
export class DateStringValidation implements ValidatorConstraintInterface {
  validate(text: string, args: ValidationArguments) {
    try {
      return moment(text, args.constraints[0], true).isValid();
    } catch (error) {
      return false;
    }
  }

  defaultMessage(args: ValidationArguments) {
    // here you can provide default error message if validation failed
    return 'Date String from $value not match to format ' + args.constraints[0];
  }
}
export function DateFormated(
  formatDate: string,
  validationOptions?: ValidationOptions,
) {
  return (object: object, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [formatDate],
      validator: DateStringValidation,
    });
  };
}
