import {
  ValidationArguments,
  ValidationError,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationOptions,
  registerDecorator,
} from 'class-validator';
import { getRepository, Not } from 'typeorm';
import { isObject } from 'util';
import { logger } from '@sentry/utils';

@ValidatorConstraint({ async: true })
export class UniqueDatabaseValidation implements ValidatorConstraintInterface {
  validate(
    value: any,
    validationArguments?: ValidationArguments,
  ): Promise<boolean> {
    if (value === null) {
      return Promise.resolve(true);
    }
    let validate = {};
    let propertyName;
    const propertyConstraint = validationArguments.constraints[1];
    if (
      validationArguments.constraints.length > 1 &&
      ![undefined, null].includes(propertyConstraint)
    ) {
      propertyName = propertyConstraint;
    } else {
      propertyName = validationArguments.property;
    }
    const className = validationArguments.constraints[0];
    validate[propertyName] = value;
    if (isObject(validationArguments.constraints[2])) {
      validate = Object.assign(validationArguments.constraints[2], validate);
    }
    const query = getRepository(className).createQueryBuilder('entity');
    query.where(`entity.${propertyName} = :${propertyName}`);
    const anotherValidation = validationArguments.constraints[3];
    if (isObject(anotherValidation)) {
      const okeys = Object.keys(anotherValidation);
      if (okeys.length > 0) {
        okeys.forEach(key => {
          query.andWhere(`entity.${key} = :${key}`);
        });
        validate = { ...validate, ...anotherValidation };
      }
    }
    query.setParameters(validate);

    // if (Reflect.has(validationArguments.object, 'id')) {
    //   const id = Reflect.get(validationArguments.object, 'id');
    //   if (id !== undefined) {
    //     query.where({
    //       id: Not(id),
    //     });
    //   }
    // }

    return query.getCount().then(count => {
      return count < 1;
    });
  }
  defaultMessage?(validationArguments?: ValidationArguments): string {
    return `this field ${validationArguments.property} is unique and ${validationArguments.value} has been used before on other row `;
  }
}
export function UniqueInDatabase(
  entity: any,
  field?: string,
  condition?: {},
  validationOptions?: ValidationOptions,
) {
  return (object: object, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [entity, field, condition],
      validator: UniqueDatabaseValidation,
    });
  };
}
