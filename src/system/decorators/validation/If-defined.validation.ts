import {
  ValidationArguments,
  ValidationTypes,
  getFromContainer,
  MetadataStorage,
} from 'class-validator';
import { registerDecorator, ValidationOptions } from 'class-validator';
import { ValidationMetadataArgs } from 'class-validator/metadata/ValidationMetadataArgs';
import { ValidationMetadata } from 'class-validator/metadata/ValidationMetadata';
export function ValidateIfDefined(validationOptions?: ValidationOptions) {
  return (object: object, propertyName: string) => {
    const args: ValidationMetadataArgs = {
      type: ValidationTypes.CONDITIONAL_VALIDATION,
      target: object.constructor,
      propertyName,
      constraints: [() => Reflect.has(object, propertyName)],
      validationOptions,
    };
    getFromContainer(MetadataStorage).addValidationMetadata(
      new ValidationMetadata(args),
    );
  };
}
