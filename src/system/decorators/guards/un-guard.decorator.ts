import { SetMetadata } from '@nestjs/common';

export const UnGuard = (...args: string[]) => SetMetadata('unGuard', args);
