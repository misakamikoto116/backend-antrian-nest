import { SetMetadata } from '@nestjs/common';

export const allowedAccessFrom = (...args: string[]) =>
  SetMetadata('urlProtection', args);
