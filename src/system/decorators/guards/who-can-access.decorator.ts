import { UseGuards } from '@nestjs/common';
import { WhoCanAccessGuard } from '@system/guards/who-can-access.guard';

export const WhoCanAccess = (...args: string[]) =>
  UseGuards(new WhoCanAccessGuard(...args));
