import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
  Logger,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { isArray, isObject } from 'util';

import { ResponseToClient } from '../ResponseToClient';

@Injectable()
export class TransformInterceptor<T>
  implements NestInterceptor<T, ResponseToClient<T>> {
  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<ResponseToClient<T>> {
    const res: any = context.switchToHttp().getResponse().res;
    return next.handle().pipe(
      map(data => {
        let paginate: {} | undefined;
        let result: any[];
        const isDataArray = isArray(data);
        if (data) {
          if (Reflect.has(data, 'page')) {
            result = data.data;
          } else {
            result = Reflect.has(data, 'items') ? data.items : data;
          }

          if (Reflect.has(data, 'items') || Reflect.has(data, 'page')) {
            paginate = {
              pageCount: data.pageCount || 0,
              total: data.total || data.totalItems || 0,
              itemCount: data.itemCount || data.count || 0,
            };
          } else if (!isDataArray) {
            result = [data];
          }
        } else {
          result = [];
        }
        const output = {
          metaData: {
            error: false,
            statusCode: res.statusCode,
            statusMessage: 'ok',
            message: result.length > 0 ? 'data found' : 'empty data',
            paginate,
          },
          result,
        };
        return output;
      }),
    );
  }
}
