import { NestFactory } from '@nestjs/core';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { RedisIoAdapter } from '@system/redisIoAdapter';
import * as helmet from 'helmet';
import { NotificationCenter } from 'node-notifier';

import { AppModule } from './app.module';
import { resolve, join } from 'path';
import { tmpdir } from 'os';
import { time } from 'uniqid';
import pump = require('pump');
import { createWriteStream } from 'fs';
import mimedb = require('mime-db');
import { init } from '@sentry/node';
import { RewriteFrames } from '@sentry/integrations';
import { Logger, BadRequestException } from '@nestjs/common';
import { env } from './system/helpers/env';
import { Integration } from '@sentry/types';
import moment = require('moment');

if (env('SENTRY_REPORT', true)) {
  init({
    dsn: process.env.SENTRY_DSN,
  });
}
export const table = [];
export async function bootstrap(): Promise<void> {
  const fa = new FastifyAdapter();
  fa.getInstance().addHook(
    'onRoute',
    (routeOtions: { method: any; path: any; handler: any }) => {
      table.push({
        method: routeOtions.method,
        path: routeOtions.path,
      });
    },
  );
  fa.register(require('fastify-multipart'), {
    addToBody: true,
    onFile: (
      fieldName: string,
      stream: any,
      // tslint:disable-next-line:variable-name
      filename: string,
      encoding: string,
      mimetype: string,
      body: any,
    ) => {
      let extension = (mimedb[mimetype].extensions as string[]).shift();
      if (!extension) {
        extension = filename.split('.').shift();
      }
      filename = moment().unix() + '.' + extension;
      const path = join(tmpdir(), filename);
      pump(stream, createWriteStream(path));

      body[fieldName] = {
        path,
        encoding,
        mimetype,
        filename,
      };

      // Manage the file stream like you need
      // By default the data will be added in a Buffer
      // Be careful to accumulate the file in memory!
      // It is MANDATORY consume the stream, otherwise the response will not be processed!
      // The body parameter is the object that will be added to the request
    },
    limits: {
      fieldNameSize: 100, // Max field name size in bytes
      fieldSize: 1000000, // Max field value size in bytes
      fields: 100, // Max number of non-file fields
      fileSize: 1000000, // For multipart forms, the max file size
      files: 100, // Max number of file fields
      headerPairs: 2000, // Max number of header key=>value pairs
    },
  });
  fa.register(require('fastify-rate-limit'), {
    max: 100,
    global: true,
    timeWindow: '1 minute',
    errorResponseBuilder: res => {
      // return {
      //   statusCode: 429,
      //   error: 'Too Many Requests',
      //   message: 'Rate limit exceeded, retry in 1 minute',
      // };
      return {
        metaData: {
          error: true,
          statusCode: 429,
          statusMessage: 'Too Many Requests',
          message: 'Rate limit exceeded, retry in 1 minute',
        },
        errorContent: [],
      };
    },
  });

  const app = await NestFactory.create<NestFastifyApplication>(AppModule, fa, {
    logger: ['warn', 'debug', 'error']
  });
  app.enableCors();
  app.useWebSocketAdapter(new RedisIoAdapter(app));
  app.use(helmet());
  generateAPIDoc(app);
  if (!process.env.NODE_PORT) {
    process.env.NODE_PORT = '3000';
  }
  if (!process.env.GATEAWAY_PORT) {
    process.env.GATEAWAY_PORT = '3100';
  }
  await app.listen(Number(process.env.NODE_PORT), '0.0.0.0', () => {
    new NotificationCenter().notify({
      title: 'Nest started',
      message: 'Ready to testing',
      sound: true, // Case Sensitive string for location of sound file, or use one of macOS' native sounds (see below)
      icon: 'Terminal Icon', // Absolute Path to Triggering Icon
      wait: true,
    });
    // tslint:disable-next-line: no-console
    console.table(table);
    // tslint:disable-next-line: no-console
    Logger.debug(`ready 🚀 http://localhost:${process.env.NODE_PORT}`);
  });
}
bootstrap();
process.on('unhandledRejection', (err: any) => {
  // tslint:disable-next-line:no-console
  console.error(err);
});

function generateAPIDoc(app: NestFastifyApplication) {
  const options = new DocumentBuilder()
    .setTitle('Module Mobile')
    .setDescription('Api untuk module mobile')
    .setVersion('1.0')
    .addBearerAuth()
    .addTag('Mobile')
    .build();
  const document = SwaggerModule.createDocument(app, options, {
    //  include: [
    //    MobileModule,
    //  ],
  });
  SwaggerModule.setup('api/mobile', app, document);
}
