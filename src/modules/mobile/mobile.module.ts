import { AntrianModule } from '@entities/antrian/antrian.module';
import { Module } from '@nestjs/common';

import { MobileLayananModule } from './mobile-layanan/mobile-layanan.module';
import { MobileAntrianModule } from './mobile-antrian/mobile-antrian.module';
import { MobileUserModule } from './mobile-user/mobile-user.module';

@Module({
  imports: [
    MobileUserModule,
    MobileLayananModule,
    MobileAntrianModule,
    AntrianModule,
  ],
  providers: [],
})
export class MobileModule {}
