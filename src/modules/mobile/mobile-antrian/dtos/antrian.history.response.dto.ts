export interface AntrianHistoryResponseDto {
  id: number;
  FeedbackDesc: string | null;
  FeedbackRating: string | null;
  isServed: boolean;
  antrian: {
    sisaAntrian: number;
    nomor: number;
    tanggal: string;
    layanan: {
      nama: string;
      instansi: {
        nama: string;
      };
    };
  };
  petugas: {
    namaDepan: string;
    namaBelakang: string;
  };
}
