import { AntrianUser } from '@entities/antrian/antrian-user.entity';
import { InDatabase } from '@system/decorators/validation/in.database.validation';
import {
  IsDefined,
  IsNumber,
  IsString,
  Max,
  MaxLength,
  Min,
  MinLength,
  Validate,
} from 'class-validator';

export class UpdateFeedBackAntrianDto {
  @InDatabase(AntrianUser, 'id', { isServed: true })
  @IsDefined()
  @IsNumber()
  id: number;
  @IsString()
  @IsDefined()
  @MinLength(30)
  @MaxLength(500)
  FeedbackDesc: string;
  @IsDefined()
  @Min(0)
  @Max(5)
  FeedbackRating: number;
}
