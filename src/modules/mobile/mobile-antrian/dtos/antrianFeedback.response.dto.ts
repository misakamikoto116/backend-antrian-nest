export interface AntrianFeedBackResponseDto {
  FeedbackRating: number;
  FeedbackDesc: string;
}
