import { AntrianUser } from '@entities/antrian/antrian-user.entity';
import { AntrianService } from '@entities/antrian/antrian.service';
import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Request,
  UseGuards,
  Logger,
} from '@nestjs/common';
import { JwtHttpGuard } from '@system/guards/jwt-http.guard';
import { RequestWithUser } from '@system/interfaces/request-with-user';
import { ValidationPipe } from '@system/pipes/validation.pipe';

import { UpdateFeedBackAntrianDto } from './dtos/update-feedback-antrian.dto';
import { MobileAntrianService } from './mobile-antrian.service';
import { AntrianNowResponseDto } from './dtos/antrianNow.response.dto';
import { WhoCanAccess } from '@system/decorators/guards/who-can-access.decorator';

@Controller()
@WhoCanAccess('Admin', 'User')
export class MobileAntrianController {
  constructor(private readonly service: MobileAntrianService) {}
  @Get()
  handleServiceInQueue(
    @Request() request: RequestWithUser,
  ): Promise<AntrianNowResponseDto> {
    return this.service.getAntrianNow(request.user);
  }
  @Get('history')
  handleHistoryQueue(@Request() request: RequestWithUser) {
    return this.service.getHistoryQueue(request.user.id);
  }
  @Post('feedback')
  handleFeedbackPost(
    @Body(new ValidationPipe()) userInput: UpdateFeedBackAntrianDto,
    @Request() request: RequestWithUser,
  ) {
    return this.service.postFeedback(userInput, request.user.id);
  }
}
