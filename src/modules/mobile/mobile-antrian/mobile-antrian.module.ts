import { AntrianModule } from '@entities/antrian/antrian.module';
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { systemConstant } from '@system/constants/system.constant';

import { MobileAntrianController } from './mobile-antrian.controller';
import { MobileAntrianService } from './mobile-antrian.service';
import { FirebaseModule } from '@modules/firebase/firebase.module';

@Module({
  imports: [AntrianModule, FirebaseModule],
  providers: [MobileAntrianService],
  controllers: [MobileAntrianController],
})
export class MobileAntrianModule {}
