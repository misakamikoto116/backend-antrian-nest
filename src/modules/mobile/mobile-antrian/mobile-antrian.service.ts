import { AntrianUser } from '@entities/antrian/antrian-user.entity';
import { AntrianService } from '@entities/antrian/antrian.service';
import { Injectable, Inject, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { UpdateFeedBackAntrianDto } from './dtos/update-feedback-antrian.dto';
import { AntrianNowResponseDto } from './dtos/antrianNow.response.dto';
import { AntrianFeedBackResponseDto } from './dtos/antrianFeedback.response.dto';
import { AntrianHistoryResponseDto } from './dtos/antrian.history.response.dto';
import { Repository } from 'typeorm';
import { TokenFCM } from '@entities/user/tokenFcm.entity';
import { User } from '@entities/user/user.entity';
import { isArray } from 'util';

@Injectable()
export class MobileAntrianService {
  constructor(private readonly antrianService: AntrianService) {}
  async getAntrianNow({ id, tokenFCM }: User): Promise<AntrianNowResponseDto> {
    const queue = await this.antrianService.getAntrianNow(id);
    const lastQueue = await this.antrianService.getLastQueueLayanan(
      queue.layanan.id,
    );
    if (queue !== undefined) {
      const { id: _, ...layanan } = queue.layanan;
      return { ...queue, sisaAntrian: queue.nomor - lastQueue, layanan };
    }
    return null;
  }
  getHistoryQueue(id: number): Promise<AntrianHistoryResponseDto> {
    return this.antrianService.getHistoryQueue(id);
  }
  postFeedback(
    userInput: UpdateFeedBackAntrianDto,
    userId: number,
  ): Promise<AntrianFeedBackResponseDto> {
    return this.antrianService.updateFeedback(userInput, userId);
  }
}
