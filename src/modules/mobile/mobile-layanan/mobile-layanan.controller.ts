import { AntrianService } from '@entities/antrian/antrian.service';
import { LayananService } from '@entities/layanan/layanan.service';
import { LoketService } from '@entities/loket/loket.service';
import { UsersService } from '@entities/user/user.service';
import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Request,
  Req,
  Query,
  Logger,
} from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiUseTags } from '@nestjs/swagger';
import { RequestWithUser } from '@system/interfaces/request-with-user';
import { ValidationPipe } from '@system/pipes/validation.pipe';
import { TakeQueueDto } from './dtos/take-queue.dto';
import { todayStringFormated } from '@system/helpers/nowStringFormated';
import { MobileLayananService } from './mobile-layanan.service';
import { ParsePagePipe } from '@system/pipes/parse-page.pipe';
import { RethinkDbService } from '@modules/rethink-db/rethinkdb.provider';
import { User } from '@entities/user/user.entity';
import { QueueResponseDto } from './dtos/queue-response.dto';
import { WhoCanAccess } from '@system/decorators/guards/who-can-access.decorator';
import { isNumber } from 'util';

@Controller()
@WhoCanAccess('Admin', 'User')
@ApiBearerAuth()
@ApiUseTags('Mobile')
export class MobileLayananController {
  constructor(
    private readonly mainService: MobileLayananService,
    private readonly rethinkDbService: RethinkDbService,
  ) { }
  @Get()
  getTopTenLayanan(): any {
    return this.mainService.topTen();
  }
  @Get('iklan')
  handleListIklan(): any {
    return this.mainService.getListIklan(5);
  }
  @Get('search')
  handleSearchSerive(
    @Query('nama') nama: string,
    @Query('page', new ParsePagePipe()) page: number,
  ): any {
    if (!nama) {
      nama = '';
    }
    return this.mainService.searchService(`%${nama}%`, page);
  }

  @Get('/:id/detail')
  async getListLoket(
    @Param('id', new ParseIntPipe()) layananId: number,
    date: string = todayStringFormated(),
  ): Promise<any> {
    return this.mainService.getListLoket(layananId, date);
  }

  @Post()
  async takeQueue(
    @Body(new ValidationPipe()) userInput: TakeQueueDto,
    @Req() request: RequestWithUser,
  ): Promise<QueueResponseDto> {
    return this.mainService.takeQueue(request, userInput).then(result => {
      const { layanan } = result;
      this.rethinkDbService.addInQueue({
        ...result,
        antrianId: result.id,
        user: {
          namaDepan: request.user.namaDepan,
          namaBelakang: request.user.namaBelakang,
          tokenFCM: request.user.tokenFCM || [{ token: '' }],
          id: request.user.id,
        },
        layanan: {
          id: userInput.layananId,
          ...layanan,
        },
      });
      this.rethinkDbService.updateInformationService(userInput.layananId);
      const { id: _, ...data } = result;
      return data as QueueResponseDto;
    });
  }
}
