import { LayananModule } from '@entities/layanan/layanan.module';

import { Module } from '@nestjs/common';

import { TypeOrmModule } from '@nestjs/typeorm';

import { MobileLayananController } from './mobile-layanan.controller';
import { MobileLayananService } from './mobile-layanan.service';
import { RethinkDbModule } from '@modules/rethink-db/rethink-db.module';
import { Iklan } from '@entities/iklan/iklan.entity';
import { Layanan } from '@entities/layanan/layanan.entity';
import { AntrianUser } from '@entities/antrian/antrian-user.entity';
import { Antrian } from '@entities/antrian/antrian.entity';
import { AntrianModule } from '@entities/antrian/antrian.module';
import { IklanModule } from '@entities/iklan/iklan.module';

@Module({
  imports: [LayananModule, AntrianModule, RethinkDbModule, IklanModule],
  controllers: [MobileLayananController],
  providers: [MobileLayananService],
})
export class MobileLayananModule {}
