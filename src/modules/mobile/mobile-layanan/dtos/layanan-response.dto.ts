export interface LayananResponseDto {
  id: number;
  nama: string;
  nama_instansi: string;
  wilayah: string;
  logo: string;
}
