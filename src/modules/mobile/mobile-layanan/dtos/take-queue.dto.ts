import { Layanan } from '@entities/layanan/layanan.entity';
import { InDatabase } from '@system/decorators/validation/in.database.validation';
import { IsDefined, IsNumber, Validate } from 'class-validator';

export class TakeQueueDto {
  @IsDefined()
  @IsNumber()
  @InDatabase(Layanan, 'id')
  layananId: number;
}
