export interface QueueResWithID {
  id: number;
  sisaAntrian: number;
  nomor: number;
  estimasiPemanggilan: number;
  layanan: {
    nama: string;
    keterangan: string;
    instansi: {
      nama: string;
    };
  };
}
