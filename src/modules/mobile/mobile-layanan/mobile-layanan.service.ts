import {
  Injectable,
  BadRequestException,
  ConflictException,
  Logger,
} from '@nestjs/common';
import { LayananService } from '@entities/layanan/layanan.service';

import { RethinkDbService } from '@modules/rethink-db/rethinkdb.provider';
import { RequestWithUser } from '@system/interfaces/request-with-user';
import { TakeQueueDto } from './dtos/take-queue.dto';
import { Layanan } from '@entities/layanan/layanan.entity';
import { Pagination } from 'nestjs-typeorm-paginate';
import { Iklan } from '@entities/iklan/iklan.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { LayananResponseDto } from './dtos/layanan-response.dto';
import { AntrianService } from '@entities/antrian/antrian.service';
import { IklanService } from '@entities/iklan/iklan.service';
import { QueueResponseDto } from './dtos/queue-response.dto';
import { QueueResWithID } from './dtos/queue-res-with-id';
import * as moment from 'moment-timezone';

@Injectable()
export class MobileLayananService {
  getListIklan(limit: number): any {
    return this.iklanService.getIklanInMobile(limit);
  }
  searchService(
    nama: string,
    page: number,
  ): Promise<Pagination<LayananResponseDto>> {
    return this.layananService.searchService(nama, page);
  }
  topTen(): Promise<LayananResponseDto[]> {
    return this.layananService.topTen();
  }
  getListLoket(layananId: number, date: string): Promise<any> {
    return this.layananService.getListLoket(layananId, date);
  }
  takeQueue(
    { user }: RequestWithUser,
    { layananId }: TakeQueueDto,
  ): Promise<QueueResWithID> {
    return new Promise<QueueResWithID>((resolve, reject) => {
      this.antrianService.isUserInQueue(user).then(userInQueue => {
        if (userInQueue) {
          reject(
            new BadRequestException(
              `User ${user.namaDepan} ${user.namaBelakang} masi sedang mengantri di layanan lain`,
            ),
          );
        } else {
          this.layananService.findOne(layananId).then(layanan => {
            const now = moment().tz('Asia/Jakarta');
            const jamBuka = moment(layanan.jamBukaPelayanan, 'HH:mm');
            const jamTutup = moment(layanan.jamTutupPelayanan, 'HH:mm');

            if (jamBuka.diff(now, 'minute') > 0) {
              reject(
                new BadRequestException(
                  `Layanan ${layanan.nama} belum terbuka, akan buka pukul ${layanan.jamBukaPelayanan}`,
                ),
              );
            }
            if (jamTutup.diff(now, 'minute') < 0) {
              reject(
                new BadRequestException(
                  `Layanan ${layanan.nama} telah tutup sejak pukul ${layanan.jamTutupPelayanan}`,
                ),
              );
            }
            this.antrianService.createUserQueue(layanan, user).then(res => {
              const {
                nomor,
                estimasiPemanggilan,
                layanan: { nama, keterangan, instansi },
              } = res;
              this.antrianService
                .getLastQueueLayanan(layananId)
                .then(lastQueue => {
                  resolve({
                    id: res.id,
                    sisaAntrian: nomor - lastQueue,
                    nomor,
                    estimasiPemanggilan,
                    layanan: {
                      nama,
                      keterangan,
                      instansi: {
                        nama: instansi.nama,
                      },
                    },
                  });
                }, reject);
            }, reject);
          }, reject);
        }
      }, reject);
    });
  }
  constructor(
    private readonly layananService: LayananService,
    private readonly antrianService: AntrianService,
    private readonly iklanService: IklanService,
  ) {}
}
