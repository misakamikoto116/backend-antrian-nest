import { AuthModule } from '@modules/auth/auth.module';
import { TokenFCM } from '@entities/user/tokenFcm.entity';
import { User } from '@entities/user/user.entity';
import { UserModule } from '@entities/user/user.module';
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { systemConstant } from '@system/constants/system.constant';

import { MobileUserController } from './mobile-user.controller';
import { Role } from '@entities/role/role.entity';

@Module({
  imports: [
    UserModule,
    AuthModule,
    JwtModule.register({
      secret: systemConstant.jwtSecreat,
    }),
  ],
  controllers: [MobileUserController],
})
export class MobileUserModule {}
