import { AuthService } from '@modules/auth/auth.service';
import { UsersService } from '@entities/user/user.service';
import {
  Body,
  Controller,
  Get,
  Post,
  Request,
  BadRequestException,
} from '@nestjs/common';
import { ValidationPipe } from '@system/pipes/validation.pipe';
import { UpdateDto } from './dto/update.dto';
import { RequestWithUser } from '@system/interfaces/request-with-user';
import { uploadImage } from '@modules/web/web-instansi/uploadImage';
import { WhoCanAccess } from '@system/decorators/guards/who-can-access.decorator';

@Controller()
@WhoCanAccess('Admin', 'User')
export class MobileUserController {
  constructor(
    private readonly userService: UsersService,
    private readonly authService: AuthService,
  ) {}
  @Get()
  getProfile(@Request() req: RequestWithUser): any {
    return this.userService.findOneWithRelation(req.user.id, []);
  }
  @Post('update')
  async updateProfile(
    @Request() req: RequestWithUser,
    @Body(ValidationPipe)
    updateData: UpdateDto,
  ): Promise<any> {
    const user = await this.userService.findOneWithRelation(req.user.id, [
      'tokenFCM',
    ]);
    const noKtpUsed = await this.userService.isNoKtpUsed(
      req.user.id,
      updateData.noKTP,
    );
    if (noKtpUsed) {
      throw new BadRequestException('No. KTP is already used');
    }
    let data = {};
    Object.keys(updateData).map(key => {
      if (updateData[key] !== '' || key === 'namaBelakang') {
        data[key] = updateData[key];
      }
    });

    if (updateData.fotoProfil !== undefined) {
      const foto = await uploadImage('userProfile', updateData.fotoProfil);
      data = { ...data, foto };
    }

    return this.userService.updateUser(data, user).then(async (res: any) => {
      const dataToken = await this.userService.findOneWithRelation(res.id, [
        'tokenFCM',
        'roles',
      ]);
      return this.authService.createNewJwtToken(dataToken);
    });
  }
}
