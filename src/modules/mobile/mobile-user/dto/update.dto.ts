import {
  IsString,
  Validate,
  MaxLength,
  MinLength,
  IsEmpty,
  ValidateIf,
  IsNotEmpty,
  IsNumberString,
  IsNumber,
} from 'class-validator';
import {
  UniqueDatabaseValidation,
  UniqueInDatabase,
} from '@system/decorators/validation/unique.database.validation';
import {
  DateStringValidation,
  DateFormated,
} from '@system/decorators/validation/datestring.validation';
import { User } from '@entities/user/user.entity';
import { FileUpload } from 'graphql-upload';
import { FileUploadDto } from '@modules/web/web-instansi/dto/file-upload.dto';
import { Type } from 'class-transformer';

export class UpdateDto {
  @ValidateIf((obj, val) => val !== undefined && val !== '')
  @IsString()
  @IsNotEmpty()
  namaDepan?: string;

  @ValidateIf((obj, val) => val !== undefined && val !== '')
  @IsString()
  namaBelakang?: string;

  @ValidateIf((obj, val) => val !== undefined && val !== '')
  @IsNumberString()
  // @UniqueInDatabase(User, 'noKtp', {})
  noKTP: string;

  // @ValidateIf((obj, val) => val !== undefined && val !== '')
  // @MaxLength(13)
  // @MinLength(10)
  // @IsNumberString()
  // @UniqueInDatabase(User)
  // noHp: string;

  @ValidateIf((obj, val) => val !== undefined && val !== '')
  @IsString()
  @DateFormated('YYYY-MM-DD')
  tanggalLahir?: string;

  @ValidateIf((obj, val) => val !== undefined && val !== '')
  @IsString()
  tempatLahir?: string;

  fotoProfil?: FileUploadDto;
}
