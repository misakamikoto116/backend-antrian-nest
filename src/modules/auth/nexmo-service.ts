import Nexmo, { Success } from '@lib/Nexmo';
import { UsersService } from '@entities/user/user.service';
import { BadRequestException, Injectable } from '@nestjs/common';
import { systemConstant } from '@system/constants/system.constant';
import { MethodLogin } from '@system/dto/methodLogin';
import * as jwt from 'jsonwebtoken';

import LoginResponseInterface from './login.response.interaface';

@Injectable()
export class NexmoService {
  private nexmo: Nexmo;
  constructor(private readonly userService: UsersService) {
    const { NEXMO_API_KEY, NEXMO_API_SECREAT } = process.env;
    this.nexmo = new Nexmo(NEXMO_API_KEY, NEXMO_API_SECREAT);
  }
  async login(noHp: string, tokenFCM: string): Promise<LoginResponseInterface> {
    const user = await this.userService.findOneByNoHp(noHp);
    if (user) {
      const nomorHp: string = noHp.replace('+62', '').replace('62', '');
      const response = (await this.nexmo.request({
        brand: 'App Lingkaran',
        number: nomorHp,
        // eslint-disable-next-line @typescript-eslint/camelcase
        code_length: 4,
        country: 'ID',
        // eslint-disable-next-line @typescript-eslint/camelcase
        pin_expiry: 3 * 60,
      })) as Success;

      return {
        token: jwt.sign(
          {
            request_id: response.request_id,
            user_id: user.id,
            method_login: MethodLogin.PHONE,
            fcm: tokenFCM,
          },
          systemConstant.jwtSecreat,
        ),
      };
    }
    throw new BadRequestException('No Hp tidak terdaftar');
  }
  async verify(requestId: string, code: string): Promise<boolean> {
    const res = (await this.nexmo.verify({
      code,
      request_id: requestId,
    })) as Success;

    return res.status === '0';
  }
}
