import { User } from '@entities/user/user.entity';

export class UserSessionDto extends User {
  toPlain(): object {
    return Object.assign({}, this);
  }
}
