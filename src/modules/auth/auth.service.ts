import { User } from '@entities/user/user.entity';
import { UsersService } from '@entities/user/user.service';
import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { systemConstant } from '@system/constants/system.constant';
import { LoginDto } from '@system/dto/login.dto';
import { MethodLogin } from '@system/dto/methodLogin';
import { RegisterDto } from '@system/dto/register.dto';
import { ValidationDto } from '@system/dto/validation.dto';
import { verify } from 'jsonwebtoken';
import { Repository } from 'typeorm';

import { LoginValidateInterface } from './login.validate.interface';
import { MailService } from './mail/mail.service';
import { NexmoService } from './nexmo-service';
import { inspect } from 'util';

@Injectable()
export class AuthService {
  constructor(
    private readonly nexmoService: NexmoService,
    private readonly userService: UsersService,
    private readonly jwtService: JwtService,
    private readonly emailService: MailService,
  ) {}
  validateUser({ token, pin }: ValidationDto) {
    return new Promise((resolve, reject) => {
      const {
        request_id: requestId,
        user_id: userId,
        method_login: methodLogin,
        fcm,
      } = verify(token, systemConstant.jwtSecreat) as LoginValidateInterface;

      new Promise(resulRes => {
        switch (methodLogin) {
          case MethodLogin.EMAIL:
            resulRes(this.emailService.verify(requestId, pin));
            break;
          case MethodLogin.PHONE:
            this.nexmoService
              .verify(requestId, pin)
              .then(resulRes)
              .catch(err => {
                Logger.error(err);
                reject(new BadRequestException('Validation not match'));
              });
            break;
        }
      }).then(resVerify => {
        if (resVerify) {
          this.userService
            .findOne(userId)
            .then(user => {
              if (fcm !== undefined) {
                this.userService.addFcm(user, fcm);
              }
              resolve(user);
            })
            .catch(err => {
              reject(new BadRequestException('User not found'));
            });
        } else {
          reject(new BadRequestException('Validation not match'));
        }
      }, reject);
    });
  }
  async login({ identity, type, tokenFCM }: LoginDto): Promise<object> {
    let res;
    if (type === 'phone') {
      res = await this.nexmoService.login(identity, tokenFCM);
    } else {
      res = await this.emailService.login(identity, tokenFCM);
    }
    return res;
  }
  // async createToken(user: User): Promise<object> {
  //   const now = moment();
  //   let token = new TokenOTP();
  //   token.token = bcrypt.hashSync(
  //     `${user.noHp}${now.format()}`,
  //     systemConstant.bcrptjs,
  //   );
  //   token.pin = Math.floor(100000 + Math.random() * 900000);
  //   token.createdAt = now.toDate();
  //   token.expireAt = now
  //     .clone()
  //     .add(20, 'm')
  //     .toDate();
  //   token.user = user;
  //   token = await this.tokenRepository.save(token);
  //   return { token: token.token, expireAt: token.expireAt };
  // }
  register(userInput: RegisterDto): Promise<User> {
    return this.userService.createUser(userInput as User);
  }
  createNewJwtToken(userData: User): {} {
    return {
      accessToken: this.jwtService.sign(userData.toPlain()),
      ...userData,
    };
  }
}
