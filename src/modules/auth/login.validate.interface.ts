import { MethodLogin } from '@system/dto/methodLogin.ts';
export interface LoginValidateInterface {
  request_id: string;
  user_id: number;
  method_login: MethodLogin;
  fcm?: string;
}
