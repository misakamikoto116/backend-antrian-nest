import { User } from '@entities/user/user.entity';
import {
  BadRequestException,
  Body,
  Controller,
  Post,
  Logger,
  UseFilters,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UnGuard } from '@system/decorators/guards/un-guard.decorator';
import { LoginDto } from '@system/dto/login.dto';
import { RegisterDto } from '@system/dto/register.dto';
import { ValidationDto } from '@system/dto/validation.dto';
import { ValidationPipe } from '@system/pipes/validation.pipe';

import { AuthService } from './auth.service';
import { systemConstant } from '@system/constants/system.constant';
import { inspect } from 'util';

@Controller()
export class AuthController {
  constructor(
    private authService: AuthService,
    private jwtService: JwtService,
  ) {}
  @UnGuard(systemConstant.JWT_HTTP_GUARD)
  @Post('login')
  login(@Body(new ValidationPipe()) userInput: LoginDto) {
    return this.authService.login(userInput);
  }
  @UnGuard(systemConstant.JWT_HTTP_GUARD)
  @Post('validate')
  async validateLogin(@Body(new ValidationPipe()) data: ValidationDto) {
    try {
      const user = await this.authService.validateUser(data);
      if (user) {
        return this.authService.createNewJwtToken(user as User);
      }
    } catch (err) {
      Logger.error(err);
      if (err instanceof BadRequestException) {
        throw err;
      }

      throw new BadRequestException('Token not match');
    }
  }
  @UnGuard(systemConstant.JWT_HTTP_GUARD)
  @Post('register')
  register(@Body(new ValidationPipe()) userInput: RegisterDto) {
    return this.authService.register(userInput);
  }
}
