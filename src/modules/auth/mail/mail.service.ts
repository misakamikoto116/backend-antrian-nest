import { Injectable, BadRequestException } from '@nestjs/common';
import * as bcryptjs from 'bcryptjs';
import { systemConstant } from '@system/constants/system.constant';
import { sign } from 'jsonwebtoken';
import { UsersService } from '@entities/user/user.service';
import { MethodLogin } from '@system/dto/methodLogin';
import LoginResponseInterface from '../login.response.interaface';
import mailgun = require('mailgun-js');
import moment = require('moment');

@Injectable()
export class MailService {
  private ratelimitValidate = [];
  private mailgun: mailgun.Mailgun;
  constructor(private readonly userService: UsersService) {
    const { MAILGUN_API_KEY } = process.env;
    this.mailgun = mailgun({
      apiKey: MAILGUN_API_KEY,
      domain: 'email.antrian.thortech.asia',
    });
  }
  async login(email: string, fcm: string): Promise<LoginResponseInterface> {
    const user = await this.userService.findByEmail(email);
    if (user) {
      const pin = Math.floor(1000 + Math.random() * 9000).toString();
      await this.sendEmail(email, pin);

      const token = bcryptjs.hashSync(pin, systemConstant.bcrptjs);
      return {
        token: sign(
          {
            request_id: token,
            user_id: user.id,
            method_login: MethodLogin.EMAIL,
            fcm,
          },
          systemConstant.jwtSecreat,
          {
            expiresIn: moment()
              .add(2, 'm')
              .unix(),
          },
        ),
      };
    }
    throw new BadRequestException('Email tidak terdaftar');
  }
  async sendEmail(email: string, pin: string): Promise<void> {
    await this.mailgun.messages().send({
      from: 'admin antrian <admin@email.antrian.thortech.asia>',
      to: email,
      subject: 'MAIL OTP',
      text: 'your otp code is ' + pin,
    });
  }
  verify(requestId: string, pin: string): boolean {
    this.ratelimitValidate = this.ratelimitValidate.filter(i => {
      const diff = moment().diff(i.time, 'minute');
      return diff < 10;
    });
    const indexRv = this.ratelimitValidate.findIndex(
      i => i.reqId === requestId,
    );
    if (indexRv !== -1) {
      const { time, attemp } = this.ratelimitValidate[indexRv];
      if (moment().diff(moment(time), 'minute') < 10 && attemp < 10) {
        this.ratelimitValidate[indexRv].attemp += 1;
      } else {
        throw new BadRequestException('Too many request validation in time');
      }
    } else {
      this.ratelimitValidate.push({
        reqId: requestId,
        time: new Date(),
        attemp: 1,
      });
    }
    if (bcryptjs.compareSync(pin, requestId)) {
      const index = this.ratelimitValidate.findIndex(
        i => i.reqId === requestId,
      );
      this.ratelimitValidate.slice(index, 1);
      return true;
    }
    return false;
  }
}
