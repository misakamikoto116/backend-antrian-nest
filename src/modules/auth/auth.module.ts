import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserModule } from '@entities/user/user.module';

import { AuthController } from './auth.controller';
import { ValidationPipe } from '@system/pipes/validation.pipe';
import { TypeOrmModule } from '@nestjs/typeorm';

import { JwtStrategy } from '@system/strategy/jwt.strategy';
import { JwtModule } from '@nestjs/jwt';
import { systemConstant } from '@system/constants/system.constant';
import { LayananModule } from '@entities/layanan/layanan.module';
import { NexmoService } from './nexmo-service';
import { MailService } from './mail/mail.service';

@Module({
  imports: [
    UserModule,
    LayananModule,
    JwtModule.register({
      secret: systemConstant.jwtSecreat,
      signOptions: { expiresIn: '2160h' },
    }),
  ],
  providers: [
    AuthService,
    ValidationPipe,
    JwtStrategy,
    NexmoService,
    MailService,
  ],
  exports: [AuthService],
  controllers: [AuthController],
})
export class AuthModule {}
