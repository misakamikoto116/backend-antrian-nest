import {
  SubscribeMessage,
  WebSocketGateway,
  WsResponse,
  WsException,
} from '@nestjs/websockets';
import { UnGuard } from '@system/decorators/guards/un-guard.decorator';
import { systemConstant } from '@system/constants/system.constant';
import { Logger, UseFilters } from '@nestjs/common';
import { Socket } from 'socket.io';
import { RethinkDbService } from '@modules/rethink-db/rethinkdb.provider';
import { JwtService } from '@nestjs/jwt';
import { Printer } from '@entities/printer/printer.entity';
import { WsExceptionFilterFilter } from '@system/ws-exception-filter.filter';
import { PrinterEvent } from 'src/events/Printer.event';
import { InjectEventEmitter } from 'nest-emitter';
@UnGuard(systemConstant.DEVICE_GUARD)
@UnGuard(systemConstant.JWT_HTTP_GUARD)
@UseFilters(WsExceptionFilterFilter)
@WebSocketGateway(Number(process.env.GATEAWAY_PORT), {
  origins: '*',
  // transports: ['websocket'],
})
export class PrinterClientGateway {
  constructor(
    private readonly redbService: RethinkDbService,
    private readonly jwtService: JwtService,
    @InjectEventEmitter() private readonly printTicketEvent: PrinterEvent,
  ) {}
  @SubscribeMessage('registerPrinter')
  handleMessage(client: Socket, payload: any) {
    let printer;
    try {
      printer = this.jwtService.verify(payload) as Printer;
    } catch (error) {
      throw new WsException(error.message);
    }
    this.redbService.registerPrinter(client.id, printer.id).then(
      () => {
        client.emit('registerPrinterReply', { status: 'ok' });
        this.emitPrintCommand(client, printer.id);
      },
      err => {
        throw new WsException(err.message);
      },
    );
  }

  emitPrintCommand(client: Socket, printerId: number) {
    this.printTicketEvent.on('printTicket', dataPrint => {
      if (dataPrint.printerId === printerId) {
        client.emit('printTicket', dataPrint);
      }
    });
  }
}
