import {
  SubscribeMessage,
  WebSocketGateway,
  OnGatewayDisconnect,
  WebSocketServer,
  WsException,
} from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';
import { JwtService } from '@nestjs/jwt';
import { RethinkDbService } from '@modules/rethink-db/rethinkdb.provider';
import { systemConstant } from '@system/constants/system.constant';
import { UnGuard } from '@system/decorators/guards/un-guard.decorator';
import { UseFilters, Logger } from '@nestjs/common';
import { WsExceptionFilterFilter } from '@system/ws-exception-filter.filter';
import { BadRequestException } from '@system/execptions/BadRequestException';
import { LoketSchema } from '@modules/rethink-db/schemas/loket.schema';
import { TextToSpeachService } from '@modules/firebase/text-to-speach-service/text-to-speach-service.service';
import { LayarEvent } from 'src/events/Layar.event';
import { InjectEventEmitter } from 'nest-emitter';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Layar } from '@entities/layar/layar.entity';
import { CallerEvent } from 'src/events/Caller.event';

@WebSocketGateway(Number(process.env.GATEAWAY_PORT), {
  transports: ['websockets'],
})
@UnGuard(systemConstant.DEVICE_GUARD)
@UnGuard(systemConstant.JWT_HTTP_GUARD)
@UseFilters(WsExceptionFilterFilter)
export class ClientLayarGateway implements OnGatewayDisconnect {
  @WebSocketServer()
  private io: Server;
  private readonly LoketLayar = new Map<number, string>();
  handleDisconnect(client: Socket) {
    this.rethinkDBSerivce.unRegisterTV(client.id);
    for (const [key, value] of this.LoketLayar.entries()) {
      if (value === client.id) {
        this.LoketLayar.delete(key);
      }
    }
  }
  constructor(
    private readonly jwtService: JwtService,
    private readonly rethinkDBSerivce: RethinkDbService,
    private readonly textToSpeachService: TextToSpeachService,
    @InjectRepository(Layar) private readonly layarRepo: Repository<Layar>,
    @InjectEventEmitter() private readonly event: LayarEvent,
    @InjectEventEmitter() recallEventService: CallerEvent,
  ) {
    recallEventService.on('makeVoice', (voiceText, loketId) => {
      if (this.LoketLayar.has(Number(loketId))) {
        this.textToSpeachService.speak(voiceText).then(
          arrayBuffer => {
            this.io
              .to(this.LoketLayar.get(Number(loketId)))
              .emit('audioCall', arrayBuffer);
          },
          err => {
            throw new WsException(err);
          },
        );
      }
    });
  }
  @SubscribeMessage('onInitTv')
  async handleOnInit(socket: Socket, token: string) {
    // socket.emit('test', socket.id);
    this.textToSpeachService.testClient().then(
      arrayBuffer => {
        socket.emit('audioCall', arrayBuffer);
      },
      err => {
        throw new WsException(err);
      },
    );
    let client: any;
    let loketids: number[];
    try {
      Logger.log(token);
      client = this.jwtService.verify(token);
      this.event.on('updateLayar', (id, stringToken, lkts) => {
        if (client.id === id) {
          socket.emit('updateToken', stringToken, lkts);
          this.sendUpdateQueue(socket, lkts.map(l => l.id));
        }
      });
      this.event.on('deleteLayar', layarId => {
        if (client.id === layarId) {
          socket.emit('deleteLayar');
        }
      });
      const { loket: lokets } = await this.layarRepo
        .findOne(client.id, {
          relations: ['instansi', 'loket'],
        })
        .then(layar => {
          socket.emit(
            'updateToken',
            this.jwtService.sign({ ...layar }),
            layar.loket,
          );
          return layar;
        });

      loketids = lokets.map(loket => loket.id);
      loketids.forEach(it => {
        this.LoketLayar.set(it, socket.id);
      });
    } catch (error) {
      throw new WsException(error);
    }
    this.rethinkDBSerivce.registerTV(socket.id, client.id, loketids).then();
    this.sendUpdateQueue(socket, loketids);
  }
  sendUpdateQueue(socket: Socket, lokets: number[]) {
    // Logger.log({ socket: socket.id, lokets });
    let state;
    let lastLoketState = [];
    this.rethinkDBSerivce.getLasUpdateLokets(lokets).subscribe(
      statusLoket => {
        Logger.log(statusLoket);
        if (Reflect.has(statusLoket, 'state')) {
          state = statusLoket.state;
        }
        if (Reflect.has(statusLoket, 'new_val')) {
          if (state === 'initializing') {
            socket.emit('updateQueue', statusLoket.new_val);
            lastLoketState.push(statusLoket.new_val);
          } else if (state === 'ready') {
            const loketUpdated = statusLoket.new_val;
            const lastItemLoketState = lastLoketState.find(
              l => l.id === loketUpdated.id,
            );
            if (lastItemLoketState) {
              const callNextQueue =
                lastItemLoketState.antrianTerakhir !==
                loketUpdated.antrianTerakhir;
              if (callNextQueue) {
                lastLoketState = lastLoketState.map(l => {
                  if (l.id === loketUpdated.id) {
                    return loketUpdated;
                  }
                  return l;
                });
                this.textToSpeachService
                  .audioCaller(
                    loketUpdated.antrianTerakhir.toString(),
                    loketUpdated.nama,
                    loketUpdated.kode,
                  )
                  .then(audio => {
                    socket.emit('audioCall', audio);
                  });
              }
              socket.emit('updateQueue', loketUpdated);
            } else {
              lastLoketState.push(loketUpdated);
            }
          }
        }
      },
      err => {
        throw new WsException(err);
      },
    );
  }
  @SubscribeMessage('recallNextQueue')
  handleRecallNextQueue(
    clientPetugas: Socket,
    { loketId, recallFor }: { loketId: number; recallFor: number },
  ) {
    this.rethinkDBSerivce.findSocket(loketId).then((clientTv: any) => {
      this.io
        .to(clientTv.clientId)
        .emit('recallNextQueue', { loketId, recallFor });
    });

    clientPetugas.emit('recallNexQueueReply', {
      recallFor: recallFor + 1,
    });
  }
}
