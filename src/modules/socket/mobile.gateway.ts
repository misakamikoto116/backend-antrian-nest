import {
  WebSocketGateway,
  OnGatewayDisconnect,
  SubscribeMessage,
  WsResponse,
  OnGatewayConnection,
  WsException,
} from '@nestjs/websockets';
import { Logger, UseGuards, UseFilters } from '@nestjs/common';
import { Socket } from 'socket.io';
import { AuthSocketIoGuard } from './auth-socket-io.guard';
import { User } from '@entities/user/user.entity';
import { RedisService } from './redis/redis.service';
import { todayStringFormated } from '@system/helpers/nowStringFormated';
import { RethinkDbService } from '@modules/rethink-db/rethinkdb.provider';
import { UnGuard } from '@system/decorators/guards/un-guard.decorator';
import { Observable } from 'rxjs';
import { JwtService } from '@nestjs/jwt';
import { map } from 'rxjs/operators';
import { systemConstant } from '@system/constants/system.constant';
import { WsExceptionFilterFilter } from '@system/ws-exception-filter.filter';

interface OnLayananQueueData {
  layananId: number;
}
@UnGuard(systemConstant.DEVICE_GUARD)
@UnGuard(systemConstant.JWT_HTTP_GUARD)
@UseFilters(WsExceptionFilterFilter)
@WebSocketGateway(Number(process.env.GATEAWAY_PORT), {
  // origins: '*:*',
  transports: ['websocket'],
  // transports: ['websocket'],
})
export class MobileGateAway
  implements OnGatewayDisconnect, OnGatewayConnection {
  handleConnection(client: Socket) {
    Logger.log('clinet connected ' + client.id);
  }
  private logger = new Logger('AppGateWay');
  constructor(
    private readonly rethinkdbService: RethinkDbService,
    private readonly jwtService: JwtService,
  ) {}

  handleDisconnect(client: Socket): any {
    this.rethinkdbService.unRegisterUser(client.id);
  }

  @SubscribeMessage('onInit')
  onInitHandle(
    client: Socket,
    data: any,
  ): Promise<WsResponse<{ status: any }>> {
    let userToken;
    try {
      userToken = this.jwtService.verify(data) as User;
      this.logger.log({ userToken });
    } catch (error) {
      throw new WsException(error);
    }
    // check token

    // this.listenOnQueueUser(userToken.id, client);
    return this.rethinkdbService.registerUser(client.id, userToken.id).then(
      () => {
        return { event: 'onInit', data: { status: 'ok' } };
      },
      err => {
        this.logger.error(err);
        throw new WsException(
          `user dengan id ${userToken.namaDepan} ${userToken.namaBelakang} gagal terdaftar  dengan client id ${client.id} ${err}`,
        );
      },
    );
  }
  @SubscribeMessage('serviceProgress')
  onListenServiceProgress(client: Socket) {
    this.rethinkdbService.listernProgressAllLayanan().subscribe(
      resLayanan => {
        this.logger.log({ resLayanan });
        client.emit('serviceProgressReply', resLayanan);
      },
      err => {
        throw new WsException(`serviceProgressReply error ${err}`);
      },
    );
  }
  @SubscribeMessage('layananQueue')
  onGetLayananQueueHandler(
    _,
    { layananId }: OnLayananQueueData,
  ): Observable<WsResponse<any>> {
    return this.rethinkdbService
      .getLayananQueue(layananId, todayStringFormated())
      .pipe(
        map(item => {
          this.logger.log({ layananQueueReply: item.new_val });
          const event = 'layananQueueReply';
          return { event, data: item.new_val };
        }),
      );
  }
  @SubscribeMessage('inQueue')
  handleInQueue(client: Socket) {
    this.logger.log({ evnet: 'inQueue', clientId: client.id });
    this.rethinkdbService.getUser(client.id).then(
      (clientUser: any) => {
        this.logger.log(clientUser);
        this.rethinkdbService.listenChangeUserInQueue(clientUser.id).subscribe(
          res => {
            this.logger.log({ inQueueRes: res });
            if (res) {
              let loket = null;
              const {
                user: _,
                // tslint:disable-next-line:variable-name
                id: _id,
                // tslint:disable-next-line:variable-name
                antrianId: _an,
                // tslint:disable-next-line:variable-name
                layanan: { id: _idl, ...layanan },
                ...resData
              } = res;
              if (res.loket) {
                loket = res.loket;
              }
              this.logger.log({ inQueue: { ...resData, layanan, loket } });
              client.emit('inQueueReply', { ...resData, layanan, loket });
            }
          },
          error => {
            Logger.error(error);
          },
        );
      },
      err => {
        Logger.error(err);
      },
    );
  }
}
