import { SubscribeMessage, WebSocketGateway } from '@nestjs/websockets';
import { WsExceptionFilterFilter } from '@system/ws-exception-filter.filter';
import { UseFilters, Logger } from '@nestjs/common';
import { UnGuard } from '@system/decorators/guards/un-guard.decorator';
import { systemConstant } from '@system/constants/system.constant';
import { Socket } from 'socket.io';
import { RethinkDbService } from '@modules/rethink-db/rethinkdb.provider';
import { todayStringFormated } from '@system/helpers/nowStringFormated';

@WebSocketGateway(Number(process.env.GATEAWAY_PORT), {
  transports: ['websockets'],
})
@UnGuard(systemConstant.JWT_HTTP_GUARD)
@UnGuard(systemConstant.DEVICE_GUARD)
@UseFilters(WsExceptionFilterFilter)
export class WebPetugasGateway {
  constructor(private readonly rethinkDBService: RethinkDbService) {}
  @SubscribeMessage('antrianNow')
  handleAntrianNow(
    client: Socket,
    data: { layananId: number; loketId: number },
  ) {
    this.rethinkDBService
      .getLayananQueue(data.layananId, todayStringFormated())
      .subscribe(({ new_val }) => {
        let loket;
        if (Reflect.has(new_val, 'loket')) {
          loket = new_val.loket;
        }
        const { sisaAntrian } = new_val;
        if (loket === undefined) {
          loket = [];
        }
        const res = {
          sisaAntrian,
          loket: loket.find(l => (l.id = data.loketId)) || [],
        };
        Logger.log({ res });
        client.emit('antrianNowReply', res);
      });
  }
}
