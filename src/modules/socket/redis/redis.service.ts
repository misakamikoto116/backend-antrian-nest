import { Injectable } from '@nestjs/common';
import * as asyncRedis from 'async-redis';
@Injectable()
export class RedisService {
  private clientRedis = asyncRedis.createClient({
    port: 6379,
    host: '127.0.0.1',
  });

  async regiser(clientId: string, userId: number): Promise<void> {
    await this.clientRedis.set(userId, clientId);
    await this.clientRedis.set(clientId, userId);
  }
  async unRegister(clinetId: string): Promise<void> {
    const userId: number = (await this.clientRedis.get(clinetId)) as number;
    await this.clientRedis.clear(userId.toString());
    await this.clientRedis.clear(clinetId);
  }

  async getContent<T>(content: string, userId: string): Promise<T> {
    return await this.clientRedis(`${content}-${userId}`) as T;
  }
}
