import { Module } from '@nestjs/common';
import { MobileGateAway } from './mobile.gateway';
import { RethinkDbModule } from '@modules/rethink-db/rethink-db.module';
import { JwtModule } from '@nestjs/jwt';
import { systemConstant } from '@system/constants/system.constant';
import { PrinterClientGateway } from './printer-client.gateway';
import { ClientLayarGateway } from './client-layar.gateway';
import { FirebaseModule } from '@modules/firebase/firebase.module';
import { WebPetugasGateway } from './web-petugas.gateway';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Layar } from '@entities/layar/layar.entity';
import { TestGateway } from './test.gateway';

@Module({
  imports: [
    RethinkDbModule,
    FirebaseModule,
    TypeOrmModule.forFeature([Layar]),
    JwtModule.register({
      secret: systemConstant.jwtSecreat,
    }),
  ],
  providers: [
    MobileGateAway,
    PrinterClientGateway,
    ClientLayarGateway,
    WebPetugasGateway,
    TestGateway,
  ],
})
export class SocketModule {}
