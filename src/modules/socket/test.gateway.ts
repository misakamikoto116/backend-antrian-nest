import { SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';

@WebSocketGateway(Number(process.env.GATEWAY_PORT), {
  transports: ['websocket']
})

export class TestGateway {
  @WebSocketServer()
  private readonly io: Server;
  @SubscribeMessage('message')
  handleMessage(client: Socket, payload: any) {
    this.io.emit('message-reply', payload);
    client.emit('message-reply', payload);
  }
}
