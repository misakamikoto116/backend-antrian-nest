import {
  CanActivate,
  ExecutionContext,
  Injectable,
  Logger,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { verify } from 'jsonwebtoken';
import { Socket } from 'socket.io';
import { systemConstant } from '@system/constants/system.constant';
import { Validator } from 'class-validator';
import { WsException } from '@nestjs/websockets';

@Injectable()
export class AuthSocketIoGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const client = context.switchToWs().getClient<Socket>();
    const hasTokenHeader = Reflect.has(
      client.handshake.headers,
      'authorization',
    );
    const isTokenString = new Validator().isString(
      client.handshake.headers.authorization,
    );
    if (!hasTokenHeader || !isTokenString) {
      client.error('unauthorize1');
      return false;
    }
    const token = (client.handshake.headers.authorization as string).replace(
      'Barer ',
      '',
    );
    const data = verify(token, systemConstant.jwtSecreat);
    if (!data) {
      client.error('unauthorize2');
      return false;
    }
    Reflect.set(client, 'user', data);
    return true;
  }
}
