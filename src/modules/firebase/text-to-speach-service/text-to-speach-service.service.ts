import { basePath } from '@system/helpers/basePath';
import { Injectable, Logger } from '@nestjs/common';
import * as textToSpeach from '@google-cloud/text-to-speech';
import { join } from 'path';

@Injectable()
export class TextToSpeachService {
  private client;
  constructor() {
    this.client = new textToSpeach.TextToSpeechClient({
      projectId: 'antrianapp-d011a',
      keyFilename: join(basePath(), 'private/fcm-adminsdk.json'),
    });
  }
  testClient() {
    return this.speak('tes... tes... ini suara pemanggilan dari tv');
  }
  audioCaller(nomor: string, namaLoket: string, kodeLoket: string) {
    const text = `Panggilan kepada, nomor antrian ${nomor} silahkan menuju ke ${namaLoket} atau ${kodeLoket}`;
    return this.speak(text);
  }

  public speak(text: string) {
    return this.client
      .synthesizeSpeech({
        input: { text },
        // Select the language and SSML Voice Gender (optional)
        voice: { languageCode: 'id-ID', name: 'id-ID-Wavenet-A' },
        // Select the type of audio encoding
        audioConfig: { audioEncoding: 'MP3' },
      })
      .then(([response]) => {
        return response.audioContent;
      });
  }
}
