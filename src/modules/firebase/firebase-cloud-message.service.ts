import { Injectable } from '@nestjs/common';
import { messaging, initializeApp, credential } from 'firebase-admin';
import { join } from 'path';
import { basePath } from '@system/helpers/basePath';

interface SendNotifOpts {
  body: string;
  title: string;
}

@Injectable()
export class FirebaseCloudMessageService {
  private fcm: messaging.Messaging;
  constructor() {
    const serviceAccount = require(join(
      basePath(),
      'private',
      'fcm-adminsdk.json',
    ));
    const app = initializeApp({
      credential: credential.cert(serviceAccount),
      databaseURL: 'https://antrianapp-d011a.firebaseio.com',
    });
    this.fcm = messaging(app);
  }
  sendToUser(fcmTokens: string[], data: SendNotifOpts) {
    return this.fcm.sendToDevice(fcmTokens, {
      data: { ...data },
    });
  }
}
