import { Module, OnApplicationBootstrap } from '@nestjs/common';
import { FirebaseCloudMessageService } from './firebase-cloud-message.service';
import { TextToSpeachService } from './text-to-speach-service/text-to-speach-service.service';

@Module({
  providers: [FirebaseCloudMessageService, TextToSpeachService],
  exports: [FirebaseCloudMessageService, TextToSpeachService],
})
export class FirebaseModule {}
