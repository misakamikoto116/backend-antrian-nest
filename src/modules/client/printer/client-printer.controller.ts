import { Controller, Get, Query, Post, Body } from '@nestjs/common';
import { PrinterDto } from './dtos/printer.dto';
import { ClientPrinterService } from './client-printer.service';
import { ParsePagePipe } from '@system/pipes/parse-page.pipe';
import { JwtService } from '@nestjs/jwt';
import { ValidationPipe } from '@system/pipes/validation.pipe';
import { WhoCanAccess } from '@system/decorators/guards/who-can-access.decorator';
@Controller('')
export class ClientPrinterController {
  constructor(
    public service: ClientPrinterService,
    private readonly jwtService: JwtService,
  ) {}
  @Post()
  @WhoCanAccess('Admin')
  handleRegisterPrinter(@Body(new ValidationPipe()) dto: PrinterDto) {
    return this.service.createClient(dto).then(res => {
      const { createdAt, updatedAt, ...result } = res;
      return {
        token: this.jwtService.sign(result),
      };
    });
  }
  @Get('instansi')
  @WhoCanAccess('Admin')
  getInstansi(
    @Query('nama') name: string,
    @Query('page', new ParsePagePipe()) page: number,
  ) {
    return this.service.getInstansi(name, page);
  }
}
