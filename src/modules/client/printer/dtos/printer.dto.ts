import { Type } from 'class-transformer';
import { IsDefined, IsString, ValidateNested } from 'class-validator';
import { InstansiDto } from './instansi.dto';
import { CrudValidationGroups } from '@nestjsx/crud';
import { Printer } from '@entities/printer/printer.entity';
import { UniqueInDatabase } from '@system/decorators/validation/unique.database.validation';
const { CREATE, UPDATE } = CrudValidationGroups;
export class PrinterDto {
  @Type(() => InstansiDto)
  @IsDefined({ groups: [CREATE, UPDATE] })
  @ValidateNested({ groups: [CREATE, UPDATE] })
  instansi: InstansiDto;
}
