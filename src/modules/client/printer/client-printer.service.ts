import { Injectable, Logger } from '@nestjs/common';
import { CrudService } from '@nestjsx/crud';
import { Printer } from '@entities/printer/printer.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Instansi } from '@entities/instansi/instansi.entity';
import { paginate, Pagination } from 'nestjs-typeorm-paginate';
import { PrinterDto } from './dtos/printer.dto';

@Injectable()
export class ClientPrinterService extends TypeOrmCrudService<Printer> {
  createClient(dto: PrinterDto) {
    const data = this.repo.merge(new Printer(), dto);
    return this.repo.save(data);
  }
  constructor(
    @InjectRepository(Printer) repo: Repository<Printer>,
    @InjectRepository(Instansi)
    private readonly instansiRepo: Repository<Instansi>,
  ) {
    super(repo);
  }
  getInstansi(
    namaInstansi: string,
    page: number,
  ): Promise<Pagination<Instansi>> {
    const query = this.instansiRepo.createQueryBuilder('instansi');
    query.leftJoin('instansi.provinsi', 'provinsi');
    query.leftJoin('instansi.kabupaten', 'kabupaten');
    query.leftJoin('instansi.kelurahan', 'kelurahan');
    query.leftJoin('instansi.kecamatan', 'kecamatan');
    query.addSelect('instansi.provinsi');
    query.addSelect('provinsi.name');
    query.addSelect('instansi.kabupaten');
    query.addSelect('kabupaten.name');
    query.addSelect('instansi.kelurahan');
    query.addSelect('kelurahan.name');
    query.addSelect('instansi.kecamatan');
    query.addSelect('kecamatan.name');

    if (namaInstansi) {
      query.where('instansi.nama like :nama ', { nama: `%${namaInstansi}%` });
    }
    return paginate(query, { page, limit: 10 });
  }
}
