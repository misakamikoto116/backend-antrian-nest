import { Module } from '@nestjs/common';
import { ClientPrinterService } from './client-printer.service';
import { ClientPrinterController } from './client-printer.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PrinterModule } from '@entities/printer/printer.module';
import { InstansiModule } from '@entities/instansi/instansi.module';
import { JwtModule } from '@nestjs/jwt';
import { systemConstant } from '@system/constants/system.constant';

@Module({
  imports: [
    PrinterModule,
    InstansiModule,
    JwtModule.register({
      secret: systemConstant.jwtSecreat,
    }),
  ],
  providers: [ClientPrinterService],
  controllers: [ClientPrinterController],
})
export class ClientPrinterModule {}
