import { Module } from '@nestjs/common';
import { ClientPrinterModule } from './printer/client-printer.module';
import { ClientAnjunganModule } from './anjungan/client-anjungan.module';
import { ClientLayarTvModule } from './layar-tv/cllient-layar-tv.module';

@Module({
  imports: [ClientPrinterModule, ClientAnjunganModule, ClientLayarTvModule],
})
export class ClientModule {}
