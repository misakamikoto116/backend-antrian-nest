import { Module } from '@nestjs/common';
import { ClientAnjunganService } from './client-anjungan.service';
import { ClientAnjunganController } from './client-anjungan.controller';
import { AnjunganModule } from '@entities/anjungan/anjungan.module';
import { WebPrinterModule } from '@modules/web/web-printer/web-printer.module';
import { InstansiModule } from '@entities/instansi/instansi.module';
import { LayananModule } from '@entities/layanan/layanan.module';
import { JwtModule } from '@nestjs/jwt';
import { systemConstant } from '@system/constants/system.constant';
import moment = require('moment');
import { AntrianModule } from '@entities/antrian/antrian.module';
import { RethinkDbModule } from '@modules/rethink-db/rethink-db.module';

@Module({
  imports: [
    AnjunganModule,
    AntrianModule,
    WebPrinterModule,
    InstansiModule,
    LayananModule,
    RethinkDbModule,
    JwtModule.register({
      secret: systemConstant.jwtSecreat,
      signOptions: {
        expiresIn: moment()
          .add(3, 'y')
          .unix(),
      },
    }),
  ],
  providers: [ClientAnjunganService],
  controllers: [ClientAnjunganController],
})
export class ClientAnjunganModule {}
