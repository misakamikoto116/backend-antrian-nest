import {
  Controller,
  UseInterceptors,
  UsePipes,
  Get,
  Request,
  Post,
  Param,
  Query,
  ParseIntPipe,
  Body,
} from '@nestjs/common';
import { parseError } from '@system/helpers/parseError';
import { ClientAnjunganService } from './client-anjungan.service';
import { AnjunganDto } from './dtos/anjungan.dto';
import {
  Crud,
  ParsedRequest,
  CrudRequest,
  CrudRequestInterceptor,
  Override,
  ParsedBody,
} from '@nestjsx/crud';

import { WebPrinterService } from '@modules/web/web-printer/web-printer.service';
import { LayananCrudService } from '@entities/layanan/layanan-crud.service';
import { InstansiCrudService } from '@entities/instansi/instansi-crud.service';
import { Anjungan } from '@entities/anjungan/anjungan.entity';
import { JwtService } from '@nestjs/jwt';
import { RequestWithUser } from '@system/interfaces/request-with-user';
import { UnGuard } from '@system/decorators/guards/un-guard.decorator';
import { systemConstant } from '@system/constants/system.constant';
import { ParsePagePipe } from '@system/pipes/parse-page.pipe';
import { FastifyRequest } from 'fastify';
import { BadRequestException } from '@system/execptions/BadRequestException';
import bodyParser = require('body-parser');
import { InjectEventEmitter } from 'nest-emitter';
import { PrinterEvent } from 'src/events/Printer.event';
import { WhoCanAccess } from '@system/decorators/guards/who-can-access.decorator';

@Crud({
  model: {
    type: AnjunganDto,
  },
  validation: {
    validateCustomDecorators: true,
    exceptionFactory: parseError,
  },
  routes: {
    only: ['createOneBase'],
  },
})
@Controller()
export class ClientAnjunganController {
  constructor(
    public service: ClientAnjunganService,
    private readonly instansi: InstansiCrudService,
    private readonly printer: WebPrinterService,
    private readonly layanan: LayananCrudService,
    private readonly jwt: JwtService,
    @InjectEventEmitter() private readonly printerEvent: PrinterEvent,
  ) {}
  @WhoCanAccess('Admin')
  @UseInterceptors(CrudRequestInterceptor)
  @Get('instansi')
  getInstansiList(
    @Request() req: FastifyRequest,
    @Query('page', new ParsePagePipe()) page: number,
  ) {
    return this.service.findInstansi(req.query, page);
  }
  @WhoCanAccess('Admin')
  @Get('printer/')
  getAnjunganList(
    @Query('instansi', new ParseIntPipe()) instansiId: number,
    @Query('page', new ParsePagePipe()) page: number = 1,
  ) {
    return this.printer.getFreePrinterInInstansi(instansiId, page);
  }
  @WhoCanAccess('Admin')
  @UseInterceptors(CrudRequestInterceptor)
  @Get('lisLayanan')
  getListLayanan(@ParsedRequest() req: CrudRequest) {
    return this.layanan.getMany(req);
  }

  @Post()
  @UseInterceptors(CrudRequestInterceptor)
  @Override()
  @WhoCanAccess('Admin')
  createOne(
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dto: AnjunganDto,
    @Request() reqCon: RequestWithUser,
  ) {
    return this.service
      .createOne(req, (dto as object) as Anjungan)
      .then(anjugan => {
        return {
          token: this.jwt.sign({
            createdBy: reqCon.user.id,
            instansiId: dto.instansi.id,
            printerId: dto.printer.id,
            anjunganId: anjugan.id,
          }),
        };
      });
  }
  @Get('layanan')
  @UnGuard(systemConstant.JWT_HTTP_GUARD)
  getLayananInstansi(
    @Request() req: FastifyRequest,
    @Query('page', new ParsePagePipe()) page: number = 1,
  ) {
    return this.isHeaderValid(req, data =>
      this.service.getListLayananInstansi(data.instansiId, page),
    );
  }
  @Get('takeQueue')
  @UnGuard(systemConstant.JWT_HTTP_GUARD)
  takeQueueHandler(
    @Request() req: FastifyRequest,
    @Query('layananId', new ParseIntPipe()) layananId: number,
  ) {
    return this.isHeaderValid(req, data =>
      this.service.accessAble(data.instansiId, layananId, () => {
        return this.service
          .takeQueue(layananId, data.anjunganId)
          .then(antrian => {
            this.service.getPrintTicketData(antrian).then(printTicket => {
              this.printerEvent.emit('printTicket', {
                ...printTicket,
                printerId: data.printerId,
              });
            });

            return antrian;
          });
      }),
    );
  }

  private isHeaderValid(
    req: FastifyRequest,
    cb: (data: AnjunganToken) => Promise<any>,
  ) {
    if (Reflect.has(req.headers, 'anjungantoken')) {
      const data: AnjunganToken = this.jwt.verify(req.headers.anjungantoken);
      return cb(data);
    }
    throw new BadRequestException('Token anjungan not found');
  }
}

interface AnjunganToken {
  createdBy: number;
  instansiId: number;
  printerId: number;
  anjunganId: number;
}
