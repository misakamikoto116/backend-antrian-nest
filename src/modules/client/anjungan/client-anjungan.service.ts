import { Injectable, Query, Scope, Logger } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Anjungan } from '@entities/anjungan/anjungan.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getConnection } from 'typeorm';
import { Layanan } from '@entities/layanan/layanan.entity';
import { paginate } from 'nestjs-typeorm-paginate';
import { ParsePagePipe } from '@system/pipes/parse-page.pipe';
import { Antrian } from '@entities/antrian/antrian.entity';
import { todayStringFormated } from '@system/helpers/nowStringFormated';
import { RethinkDbService } from '@modules/rethink-db/rethinkdb.provider';
import { DefaultQuery } from 'fastify';
import { Instansi } from '@entities/instansi/instansi.entity';

@Injectable({
  scope: Scope.REQUEST,
})
export class ClientAnjunganService extends TypeOrmCrudService<Anjungan> {
  getPrintTicketData(
    antrian: {
      layanan: { id: number };
      anjungan: { id: number };
      nomor: number;
      estimasiPemanggilan: number;
      terpanggil: false;
      tanggal: any;
    } & Antrian,
  ) {
    return this.antrianRepo
      .createQueryBuilder('antrian')
      .where({ id: antrian.id })
      .leftJoin('antrian.layanan', 'layanan')
      .leftJoin('layanan.instansi', 'instansi')
      .select([
        'instansi.nama',
        'layanan.nama',
        'antrian.nomor',
        'instansi.tagline',
        'instansi.alamat',
        'antrian.estimasiPemanggilan',
        'antrian.tanggal',
      ])
      .getOne()
      .then(res => {
        return {
          namaInstansi: res.layanan.instansi.nama,
          namaLayanan: res.layanan.nama,
          nomorAntrian: res.nomor,
          taglineInstansi: res.layanan.instansi.tagline,
          AlamatInstansi: res.layanan.instansi.alamat,
          estimasiPemanggilan: res.estimasiPemanggilan,
          tanggal: res.tanggal,
        };
      });
  }
  constructor(
    @InjectRepository(Anjungan) repo: Repository<Anjungan>,
    @InjectRepository(Layanan)
    private readonly layananRepo: Repository<Layanan>,
    @InjectRepository(Antrian)
    private readonly antrianRepo: Repository<Antrian>,
    @InjectRepository(Instansi)
    private readonly instansiRepo: Repository<Instansi>,
    private readonly rethink: RethinkDbService,
  ) {
    super(repo);
  }
  findInstansi(query: DefaultQuery, page: number) {
    const qdb = this.instansiRepo.createQueryBuilder('q');
    if (query.provinsiId) {
      qdb.orWhere('q.provinsiId = :provinsiId', {
        provinsiId: query.provinsiId || 0,
      });
    }
    if (query.kabupatenId) {
      qdb.orWhere('q.kabupatenId = :kabupatenId', {
        kabupatenId: query.kabupatenId || 0,
      });
    }
    if (query.kecamatanId) {
      qdb.orWhere('q.kecamatanId = :kecamatanId', {
        kecamatanId: query.kecamatanId || 0,
      });
    }
    if (query.kelurahanId) {
      qdb.orWhere('q.kelurahanId = :kelurahanId', {
        kelurahanId: query.kelurahanId || 0,
      });
    }
    if (query.nama) {
      qdb.orWhere('q.nama LIKE :nama', { nama: `%${query.nama}%` });
    }
    qdb.leftJoin('q.provinsi', 'provinsi');
    qdb.leftJoin('q.kecamatan', 'kecamatan');
    qdb.leftJoin('q.kabupaten', 'kabupaten');
    qdb.leftJoin('q.kelurahan', 'kelurahan');
    qdb.addSelect('q.provinsi');
    qdb.addSelect('provinsi.name');
    qdb.addSelect('q.kecamatan');
    qdb.addSelect('kecamatan.name');
    qdb.addSelect('q.kabupaten');
    qdb.addSelect('kabupaten.name');
    qdb.addSelect('q.kelurahan');
    qdb.addSelect('kelurahan.name');
    return paginate(qdb, { limit: 10, page });
  }
  getListLayananInstansi(instansiId: number, page: number) {
    const query = this.layananRepo.createQueryBuilder('l');
    query.where('l.instansiId = :instansiId', { instansiId });
    return paginate(query, { page, limit: 10 });
  }
  accessAble(instansiId: number, layananId: number, cb: () => any) {
    return this.layananRepo
      .findOne({
        where: {
          id: layananId,
          instansi: {
            id: instansiId,
          },
        },
      })
      .then(cb);
  }
  takeQueue(layananId: number, anjunganId: number) {
    return this.antrianRepo
      .save({
        layanan: {
          id: layananId,
        },
        anjungan: {
          id: anjunganId,
        },
        nomor: 0,
        estimasiPemanggilan: 0,
        terpanggil: false,
        tanggal: todayStringFormated(),
      })
      .then(async antrian => {
        this.rethink.updateInformationService(layananId);
        return this.antrianRepo
          .count({
            terpanggil: false,
            tanggal: todayStringFormated(),
            layanan: {
              id: layananId,
            },
          })
          .then(count => {
            antrian.sisaAntrian = count - 1;
            return antrian;
          });
      });
  }
}
