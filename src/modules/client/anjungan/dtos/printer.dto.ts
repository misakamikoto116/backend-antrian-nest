import { IsDefined, IsNumber, IsString } from 'class-validator';
import { CrudValidationGroups } from '@nestjsx/crud';
import { InDatabase } from '@system/decorators/validation/in.database.validation';
import { Printer } from '@entities/printer/printer.entity';
import { Anjungan } from '@entities/anjungan/anjungan.entity';
import { UniqueInDatabase } from '@system/decorators/validation/unique.database.validation';
const { CREATE, UPDATE } = CrudValidationGroups;
export class PrinterDto {
  @IsDefined({ groups: [CREATE, UPDATE] })
  @IsNumber({}, { groups: [CREATE, UPDATE] })
  @InDatabase(Printer, 'id', {}, { groups: [CREATE, UPDATE] })
  @UniqueInDatabase(Anjungan, 'printerId', {}, { groups: [CREATE, UPDATE] })
  id: number;
}
