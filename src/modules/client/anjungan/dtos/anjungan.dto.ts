import { IsDefined, IsString, ValidateNested, IsEmpty } from 'class-validator';
import { CrudValidationGroups } from '@nestjsx/crud';
import { Type, Transform } from 'class-transformer';
import { InstansiDto } from './instansi.dto';
import { PrinterDto } from './printer.dto';
const { CREATE, UPDATE } = CrudValidationGroups;
export class AnjunganDto {
  @Type(() => InstansiDto)
  @IsDefined({ groups: [CREATE, UPDATE] })
  @ValidateNested({ groups: [CREATE, UPDATE] })
  instansi: InstansiDto;
  @Type(() => PrinterDto)
  @IsDefined({ groups: [CREATE, UPDATE] })
  @ValidateNested({ groups: [CREATE, UPDATE] })
  printer: PrinterDto;
}
