import { IsNumber, IsDefined } from 'class-validator';

import { InDatabase } from '@system/decorators/validation/in.database.validation';

import { Loket } from '@entities/loket/loket.entity';

export class LoketDto {
  @IsNumber()
  @IsDefined()
  @InDatabase(Loket)
  id: number;
}
