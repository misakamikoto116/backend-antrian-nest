import { InstansiDto } from './instansi.dto';
import { Type } from 'class-transformer';
import { ValidateNested } from 'class-validator';
import { CrudValidationGroups } from '@nestjsx/crud';
import { LoketDto } from './loket.dto';
const { CREATE, UPDATE } = CrudValidationGroups;
export class RegisterlayarDto {
  @Type(() => InstansiDto)
  @ValidateNested({ groups: [CREATE, UPDATE] })
  instansi: InstansiDto;
  @Type(() => LoketDto)
  @ValidateNested({ groups: [CREATE, UPDATE] })
  loket: LoketDto[];
}
