import { Injectable, Inject, Logger } from '@nestjs/common';
import { Layar } from '@entities/layar/layar.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { paginate } from 'nestjs-typeorm-paginate';
import { Instansi } from '@entities/instansi/instansi.entity';
import { RegisterlayarDto } from './dtos/register-layar.dto';
import { JwtService } from '@nestjs/jwt';
import * as uniqid from 'uniqid';
import { Loket } from '@entities/loket/loket.entity';
@Injectable()
export class ClientLayarTvService {
  getListLoket(page: number, id: any) {
    return paginate(
      this.loketRepository,
      { page, limit: 15 },
      {
        where: {
          instansi: {
            id,
          },
        },
      },
    );
  }
  getListTvInstansi(page: number, instansiId: number) {
    Logger.log({ page });
    return paginate(
      this.layarRepository,
      { page, limit: 15 },
      {
        where: {
          instansi: {
            id: instansiId,
          },
        },
        join: {
          alias: 'layar',
          innerJoinAndSelect: {
            instansi: 'layar.instansi',
          },
          leftJoinAndSelect: {
            loket: 'layar.loket',
          },
        },
      },
    ).then(pag => {
      const items = pag.items.map(layar => {
        return { ...layar, tokenLayar: this.jwtService.sign({ ...layar }) };
      });
      return { ...pag, ...{ items } };
    });
  }
  register(userInput: RegisterlayarDto) {
    return this.layarRepository
      .save({
        ...userInput,
        kode: uniqid('clientLayar-'),
        status: true,
      })
      .then(async data => {
        const instansi = await this.instansiRepository.findOne(
          data.instansi.id,
        );
        return {
          ...data,
          instansi: { ...instansi },
          tokenLayar: this.jwtService.sign({ ...data }),
        };
      });
  }
  update(userInput: RegisterlayarDto, layarId: number) {
    return this.layarRepository.findOneOrFail(layarId).then(layar => {
      return this.layarRepository
        .save(this.layarRepository.merge(layar, userInput))
        .then(updatedLayar => {
          return {
            ...updatedLayar,
            tokenLayar: this.jwtService.sign({ ...updatedLayar }),
          };
        });
    });
  }
  getListInstansi(page: number, namaInstansi: string) {
    if (!namaInstansi) {
      namaInstansi = '';
    }
    return paginate(
      this.instansiRepository,
      {
        page,
        limit: 15,
      },
      {
        where: {
          nama: Like(`%${namaInstansi}%`),
        },
      },
    );
  }

  constructor(
    @InjectRepository(Layar)
    private readonly layarRepository: Repository<Layar>,
    @InjectRepository(Instansi)
    private readonly instansiRepository: Repository<Instansi>,
    @InjectRepository(Loket)
    private readonly loketRepository: Repository<Loket>,
    private readonly jwtService: JwtService,
  ) {}
}
