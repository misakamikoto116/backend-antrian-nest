import {
  Controller,
  Post,
  Param,
  Query,
  Body,
  Get,
  ParseIntPipe,
  Header,
  Headers,
  BadRequestException,
  Logger,
} from '@nestjs/common';
import { ParsePagePipe } from '@system/pipes/parse-page.pipe';
import { ClientLayarTvService } from './client-layar-tv.service';
import { RegisterlayarDto } from './dtos/register-layar.dto';
import { JwtService } from '@nestjs/jwt';
import { UnGuard } from '@system/decorators/guards/un-guard.decorator';
import { systemConstant } from '@system/constants/system.constant';
import { WhoCanAccess } from '@system/decorators/guards/who-can-access.decorator';

@Controller()
export class ClientLayarTvController {
  constructor(
    private readonly service: ClientLayarTvService,
    private readonly jwtService: JwtService,
  ) {}
  @Post('register')
  @WhoCanAccess('Admin')
  register(@Body() userInput: RegisterlayarDto) {
    return this.service.register(userInput);
  }
  @Post('update')
  @WhoCanAccess('Admin')
  update(
    @Body() userInput: RegisterlayarDto,
    @Headers('tokenlayar') token: string,
  ) {
    try {
      const tokenContent = this.jwtService.verify(token);
      return this.service.update(userInput, tokenContent.id);
    } catch (err) {
      throw new BadRequestException(err);
    }
  }
  @Get('instansi')
  @WhoCanAccess('Admin')
  listInstansi(
    @Query('nama') namaInstansi: string,
    @Query('page', new ParsePagePipe()) page: number,
  ) {
    return this.service.getListInstansi(page, namaInstansi);
  }
  @UnGuard(systemConstant.JWT_HTTP_GUARD)
  @Get('loket')
  listLoket(
    @Headers('tokenlayar') token: string,
    @Query('page', new ParsePagePipe())
    page: number,
  ) {
    try {
      const { instansi } = this.jwtService.verify(token);
      return this.service.getListLoket(page, instansi.id);
    } catch (error) {
      throw new BadRequestException(error);
    }
  }
  @Get('listTv')
  @WhoCanAccess('Admin')
  handleListTv(
    @Query('instansi', new ParseIntPipe()) instansiId: number,
    @Query('page', new ParsePagePipe()) page: number,
  ) {
    return this.service.getListTvInstansi(page, instansiId);
  }
}
