import { Module } from '@nestjs/common';
import { ClientLayarTvController } from './client-layar-tv.controller';
import { ClientLayarTvService } from './client-layar-tv.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Layar } from '@entities/layar/layar.entity';
import { Instansi } from '@entities/instansi/instansi.entity';
import { JwtModule } from '@nestjs/jwt';
import { systemConstant } from '@system/constants/system.constant';
import moment = require('moment');
import { Loket } from '@entities/loket/loket.entity';

@Module({
  controllers: [ClientLayarTvController],
  imports: [
    TypeOrmModule.forFeature([Layar, Instansi, Loket]),
    JwtModule.register({
      secret: systemConstant.jwtSecreat,
      signOptions: {
        expiresIn: moment()
          .add(3, 'y')
          .unix(),
      },
    }),
  ],
  providers: [ClientLayarTvService],
})
export class ClientLayarTvModule {}
