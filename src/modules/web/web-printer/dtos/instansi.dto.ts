import { IsDefined, IsNumber } from 'class-validator';
import { CrudValidationGroups } from '@nestjsx/crud';
import { InDatabase } from '@system/decorators/validation/in.database.validation';
import { Instansi } from '@entities/instansi/instansi.entity';
const { CREATE, UPDATE } = CrudValidationGroups;
export class InstansiDto {
  @IsDefined({ groups: [CREATE, UPDATE] })
  @IsNumber({}, { groups: [CREATE, UPDATE] })
  @InDatabase(Instansi, 'id', {}, { groups: [CREATE, UPDATE] })
  id: number;
}
