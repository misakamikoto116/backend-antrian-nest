import { Type } from 'class-transformer';
import { IsDefined, IsString, ValidateNested } from 'class-validator';
import { InstansiDto } from './instansi.dto';
import { CrudValidationGroups } from '@nestjsx/crud';
const { CREATE, UPDATE } = CrudValidationGroups;
export class PrinterDto {
  @Type(() => InstansiDto)
  @IsDefined({ groups: [CREATE, UPDATE] })
  @ValidateNested({ groups: [CREATE, UPDATE] })
  instansi: InstansiDto;
}
