import { Injectable, Logger } from '@nestjs/common';
import { Printer } from '@entities/printer/printer.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { paginate } from 'nestjs-typeorm-paginate';

@Injectable()
export class WebPrinterService extends TypeOrmCrudService<Printer> {
  constructor(@InjectRepository(Printer) protected repo: Repository<Printer>) {
    super(repo);
  }
  getFreePrinterInInstansi(instansiId: number, page: number) {
    const query = this.repo.createQueryBuilder('printer').where(qb => {
      const sq = qb
        .subQuery()
        .select('anjungan.printerId')
        .from('anjungan', 'anjungan')
        .innerJoin('anjungan.printer', 'print');
      return 'printer.id NOT IN (' + sq.getQuery() + ')';
    });
    query.innerJoin('printer.instansi', 'instansi', 'instansi.id = :id', {
      id: instansiId,
    });
    return paginate(query, {
      limit: 10,
      page,
    });
  }
}
