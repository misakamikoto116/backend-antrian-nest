import { Controller } from '@nestjs/common';
import { Crud, CrudController } from '@nestjsx/crud';
import { PrinterDto } from './dtos/printer.dto';
import { parseError } from '@system/helpers/parseError';
import { WebPrinterService } from './web-printer.service';
import { Printer } from '@entities/printer/printer.entity';
import { WhoCanAccess } from '@system/decorators/guards/who-can-access.decorator';

@Crud({
  model: {
    type: PrinterDto,
  },
  validation: {
    validateCustomDecorators: true,
    exceptionFactory: parseError,
  },
  query: {
    limit: 10,
    join: {
      instansi: {
        eager: true,
      },
    },
  },
})
@Controller('')
@WhoCanAccess('Admin')
export class WebPrinterController implements CrudController<Printer> {
  constructor(public service: WebPrinterService) {}
  get base(): CrudController<Printer> {
    return this;
  }
}
