import { Module } from '@nestjs/common';
import { WebPrinterService } from './web-printer.service';
import { WebPrinterController } from './web-printer.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PrinterModule } from '@entities/printer/printer.module';

@Module({
  imports: [PrinterModule],
  providers: [WebPrinterService],
  exports: [WebPrinterService],
  controllers: [WebPrinterController],
})
export class WebPrinterModule {}
