import { Injectable } from '@nestjs/common';
import { User } from '@entities/user/user.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class InfoPajakService {
  async find(id: number): Promise<User> {
    const user = await this.repoUser.findOne(id);
    return user;
  }

  constructor(
    @InjectRepository(User)
    private readonly repoUser: Repository<User>
  ) { }

}
