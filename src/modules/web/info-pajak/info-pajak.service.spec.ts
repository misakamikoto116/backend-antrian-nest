import { Test, TestingModule } from '@nestjs/testing';
import { InfoPajakService } from './info-pajak.service';

describe('InfoPajakService', () => {
  let service: InfoPajakService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [InfoPajakService],
    }).compile();

    service = module.get<InfoPajakService>(InfoPajakService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
