import { Test, TestingModule } from '@nestjs/testing';
import { InfoPajakController } from './info-pajak.controller';

describe('InfoPajak Controller', () => {
  let controller: InfoPajakController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [InfoPajakController],
    }).compile();

    controller = module.get<InfoPajakController>(InfoPajakController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
