import { Module } from '@nestjs/common';
import { InfoPajakController } from './info-pajak.controller';
import { InfoPajakService } from './info-pajak.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '@entities/user/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User])],
  controllers: [InfoPajakController],
  providers: [InfoPajakService]
})
export class InfoPajakModule { }
