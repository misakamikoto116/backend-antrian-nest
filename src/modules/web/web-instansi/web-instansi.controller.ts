import {
  Controller,
  Post,
  Req,
  Res,
  Body,
  UseInterceptors,
  Get,
  Query,
  Param,
  Patch,
  Delete,
  ParseIntPipe,
} from '@nestjs/common';

import { InstansiDto } from './dto/instansi.dto';
import { InstansiService } from '@entities/instansi/instansi.service';
import { ValidationPipe } from '@system/pipes/validation.pipe';
import { Instansi } from '@entities/instansi/instansi.entity';
import { uploadImage } from './uploadImage';
import { FileUploadDto } from './dto/file-upload.dto';
import { BadRequestException } from '@system/execptions/BadRequestException';
import { WhoCanAccess } from '@system/decorators/guards/who-can-access.decorator';

@Controller()
@WhoCanAccess('Admin')
export class WebInstansiController {
  constructor(private readonly modelService: InstansiService) {}
  @Post()
  async createData(@Body(ValidationPipe) userInput: InstansiDto): Promise<any> {
    const data = { ...userInput };
    if (!userInput.logo) {
      throw new BadRequestException('Logo not allowed empty');
    }
    if (!(userInput.logo as FileUploadDto).mimetype.startsWith('image')) {
      throw new BadRequestException('Logo must be image');
    }
    data.logo = await uploadImage(
      userInput.nama,
      userInput.logo as FileUploadDto,
    );

    return this.modelService.store(data as Instansi);
  }
  @Get()
  async getItems(@Query('page') page: number = 0): Promise<{}> {
    return await this.modelService.getAll({ page, limit: 10 });
  }

  @Get(':id')
  getSingleItem(@Param('id', new ParseIntPipe()) itemId: number): any {
    return this.modelService.getSingleItem(itemId);
  }

  @Patch(':id')
  async updateItem(
    @Body(new ValidationPipe())
    userInput: InstansiDto,
    @Param('id', new ParseIntPipe()) itemId: number,
  ): Promise<any> {
    const data = { ...userInput };
    if (userInput.logo) {
      if (!(userInput.logo as FileUploadDto).mimetype.startsWith('image')) {
        throw new BadRequestException('Logo must be image');
      }
      data.logo = await uploadImage(
        userInput.nama,
        userInput.logo as FileUploadDto,
      );
    }
    return await this.modelService.udpate(itemId, data as Instansi);
  }

  @Delete(':id')
  async deleteItem(
    @Param('id', new ParseIntPipe()) itemId: number,
  ): Promise<any> {
    return this.modelService.delete(itemId);
  }
}
