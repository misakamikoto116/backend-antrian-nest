import { Module } from '@nestjs/common';
import { WebInstansiController } from './web-instansi.controller';
import { JwtModule } from '@nestjs/jwt';
import { systemConstant } from '@system/constants/system.constant';
import { InstansiModule } from '@entities/instansi/instansi.module';

@Module({
  imports: [
    InstansiModule,
    JwtModule.register({
      secret: systemConstant.jwtSecreat,
    }),
  ],
  controllers: [WebInstansiController],
})
export class WebInstansiModule {}
