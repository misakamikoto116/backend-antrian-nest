import { Type } from 'class-transformer';
import { ProvinsiDto } from '@entities/wilayah/kabupaten/dtos/provinsi.dto';
import { KabupatenDto } from '@entities/wilayah/kecamatan/dtos/kabupaten.dto';
import { KecamatanDto } from '@entities/wilayah/kelurahan/dtos/kecamatan.dto';
import { KelurahanDto } from '@entities/wilayah/kelurahan/dtos/kelurahan.dto';
import {
  ValidateIf,
  ValidateNested,
  IsDefined,
  IsString,
  IsNumber,
} from 'class-validator';
import { FileUploadDto } from './file-upload.dto';

export class InstansiDto {
  @IsDefined()
  @IsString()
  nama: string;

  logo: FileUploadDto | string;

  @IsDefined()
  @IsString()
  tagline: string;

  @Type(() => ProvinsiDto)
  @ValidateIf(o => !Reflect.has(o, 'kabupaten'))
  @ValidateNested()
  provinsi: ProvinsiDto;

  @Type(() => KabupatenDto)
  @ValidateIf(o => !Reflect.has(o, 'kecamatan'))
  @ValidateNested()
  kabupaten: KabupatenDto;

  @Type(() => KecamatanDto)
  @ValidateIf(o => !Reflect.has(o, 'kecamatan'))
  @ValidateNested()
  kecamatan: KecamatanDto;

  @Type(() => KelurahanDto)
  @ValidateIf(o => !Reflect.has(o, 'kelurahan'))
  @ValidateNested()
  kelurahan: KelurahanDto;
}
