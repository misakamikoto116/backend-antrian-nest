export class FileUploadDto {
  path: string;
  encoding: string;
  filename: string;
  mimetype: string;
}
