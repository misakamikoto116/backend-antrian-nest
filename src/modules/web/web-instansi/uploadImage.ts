import { BadRequestException, Logger } from '@nestjs/common';
import { InstansiDto } from './dto/instansi.dto';
import space = require('aws-sdk/clients/s3');
import { createReadStream } from 'fs';
import { FileUploadDto } from './dto/file-upload.dto';
export function uploadImage(folderName: string, userInput: FileUploadDto) {
  const sp = new space({
    endpoint: process.env.SPACE_ENDPOINT,
    accessKeyId: process.env.SPACE_KEY,
    secretAccessKey: process.env.SPACE_SECREAT,
  });
  return new Promise<string>((resolve, reject) => {
    sp.upload(
      {
        ACL: 'public-read',
        ContentEncoding: userInput.encoding,
        ContentType: userInput.mimetype,
        Bucket: 'thortechspace/belajar-pakai-s3/' + folderName,
        Key: userInput.filename,
        Body: createReadStream(userInput.path),
      },
      (err, data) => {
        if (err !== null) {
          reject(new BadRequestException(err.message));
        } else {
          resolve(data.Location);
        }
      },
    );
  });
}
