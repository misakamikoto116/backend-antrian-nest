import {
  MiddlewareConsumer,
  Module,
  NestModule,
  UseInterceptors,
} from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { systemConstant } from '@system/constants/system.constant';

import { WebInstansiModule } from './web-instansi/web-instansi.module';
import { WebLayananModule } from './web-layanan/web-layanan.module';
import { WebLayarModule } from './web-layar/web-layar.module';
import { WebLoketModule } from './web-loket/web-loket.module';
import { WebPelayananModule } from './web-pelayanan/web-pelayanan.module';
import { WebRoleModule } from './web-role/web-role.module';
import { WebWilayahModule } from './wilayah/web.wilayah.module';
import { WebUserModule } from './web-user/web-user.module';
import { WebAnjunganModule } from './web-anjungan/web-anjungan.module';
import { WebPrinterModule } from './web-printer/web-printer.module';
import { WebPetugasModule } from './web-petugas/web-petugas.module';
import { WebIklanModule } from './web-iklan/web-iklan.module';
import { InfoPajakModule } from './info-pajak/info-pajak.module';

@Module({
  imports: [
    WebLayananModule,
    WebLayarModule,
    WebPelayananModule,
    WebIklanModule,
    WebInstansiModule,
    WebWilayahModule,
    WebLoketModule,
    WebRoleModule,
    WebUserModule,
    WebAnjunganModule,
    WebPrinterModule,
    WebPetugasModule,
    InfoPajakModule,
  ],
  providers: [],
})
export class WebModule {}
