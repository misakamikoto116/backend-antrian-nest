import { Module } from '@nestjs/common';
import { WebIklanService } from './web-iklan.service';
import { WebIklanController } from './web-iklan.controller';
import { IklanModule } from '@entities/iklan/iklan.module';

@Module({
  imports: [IklanModule],
  providers: [WebIklanService],
  controllers: [WebIklanController],
})
export class WebIklanModule {}
