import { Injectable, Logger, BadRequestException } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { Repository, DeepPartial } from 'typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { paginate } from 'nestjs-typeorm-paginate';
import { Iklan } from '@entities/iklan/iklan.entity';
import { IklanDto } from './dtos/iklan.dto';

@Injectable()
export class WebIklanService {
  constructor(@InjectRepository(Iklan) protected repo: Repository<Iklan>) {}

  getList(page: number, title: string) {
    return paginate(
      this.repo,
      { page, limit: 15 },
      {
        title,
      },
    );
  }
  getItem(id: number) {
    return this.repo.findOneOrFail(id).catch(() => {
      throw new BadRequestException('Data not found');
    });
  }
  update(id: number, data: DeepPartial<Iklan>) {
    return new Promise<Iklan>((resolve, reject) => {
      this.repo
        .findOneOrFail(id)
        .then(iklan => {
          this.repo
            .save(this.repo.merge(iklan, data))
            .then(resolve)
            .catch(reject);
        })
        .catch(() => {
          reject(new BadRequestException('Data not found'));
        });
    });
  }
  save(data: DeepPartial<Iklan>) {
    return this.repo.save(data);
  }
  delete(id: number) {
    return this.repo.delete(id);
  }
}
