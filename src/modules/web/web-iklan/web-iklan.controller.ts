import {
  Controller,
  Get,
  Delete,
  Post,
  Query,
  Param,
  Body,
  Patch,
  ParseIntPipe,
  BadRequestException,
} from '@nestjs/common';
import { Crud, CrudController } from '@nestjsx/crud';
import { IklanDto } from './dtos/iklan.dto';
import { parseError } from '@system/helpers/parseError';
import { WebIklanService } from './web-iklan.service';
import { Printer } from '@entities/printer/printer.entity';
import { ParsePagePipe } from '@system/pipes/parse-page.pipe';
import { ParseIdItemPipe } from '@system/pipes/parse-id-item.pipe';
import { ValidationPipe } from '@system/pipes/validation.pipe';
import { uploadImage } from '../web-instansi/uploadImage';
import { FileUploadDto } from '../web-instansi/dto/file-upload.dto';
import { DeepPartial } from 'typeorm';
import { Iklan } from '@entities/iklan/iklan.entity';

@Controller('')
export class WebIklanController {
  constructor(private readonly service: WebIklanService) {}
  @Get()
  handleList(
    @Query('page', new ParsePagePipe()) page: number,
    @Query('title') title: string,
  ) {
    return this.service.getList(page, title);
  }
  @Post()
  async handleStore(@Body(ValidationPipe) userInput: IklanDto) {
    if (!userInput.foto) {
      throw new BadRequestException('Foto not allowed empty');
    }
    if (!(userInput.foto as FileUploadDto).mimetype.startsWith('image')) {
      throw new BadRequestException('Foto must be image');
    }
    return uploadImage('iklan', userInput.foto).then(url => {
      return this.service.save({
        title: userInput.title,
        url,
      });
    });
  }
  @Get(':id')
  handleDetail(@Param('id', new ParseIntPipe()) id: number) {
    return this.service.getItem(id);
  }
  @Patch(':id')
  async handleUpdate(
    @Param('id', new ParseIntPipe()) id: number,
    @Body(ValidationPipe) userInput: IklanDto,
  ) {
    const data: DeepPartial<Iklan> = {
      title: userInput.title,
    };
    if (userInput.foto) {
      if (!userInput.foto.mimetype.startsWith('image')) {
        throw new BadRequestException('Foto must be image');
      }
      data.url = await uploadImage('iklan', userInput.foto);
    }

    return this.service.update(id, data);
  }
  @Delete(':id')
  handleDelete(@Param('id', new ParseIntPipe()) id: number) {
    return this.service.delete(id);
  }
}
