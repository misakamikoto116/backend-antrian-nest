import { Type } from 'class-transformer';
import { IsDefined, IsString, ValidateNested } from 'class-validator';
import { InstansiDto } from './instansi.dto';
import { CrudValidationGroups } from '@nestjsx/crud';
import { FileUploadDto } from '@modules/web/web-instansi/dto/file-upload.dto';
const { CREATE, UPDATE } = CrudValidationGroups;
export class IklanDto {
  @IsDefined()
  @IsString()
  title: string;
  foto: FileUploadDto;
}
