import { Module } from '@nestjs/common';
import { WebLayananController } from './web-layanan.controller';
import { LayananModule } from '@entities/layanan/layanan.module';

@Module({
  imports: [LayananModule],
  controllers: [WebLayananController],
})
export class WebLayananModule {}
