import { Validate } from 'class-validator';
import { Instansi } from '@entities/instansi/instansi.entity';
import { InDatabase } from '@system/decorators/validation/in.database.validation';
export class InstansiDto {
  @InDatabase(Instansi)
  id: number;
}
