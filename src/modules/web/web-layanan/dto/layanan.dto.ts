import { Layanan } from '@entities/layanan/layanan.entity';
import { IsString, IsBoolean, Validate, IsNumber } from 'class-validator';
import { DateFormated } from '@system/decorators/validation/datestring.validation';
import { Type } from 'class-transformer';
import { InstansiDto } from './Instansi.dto';
export class LayananDto {
  @IsString()
  nama: string;

  @IsString()
  keterangan: string;
  @IsBoolean()
  registrasiOnline: boolean;

  @Type(() => InstansiDto)
  instansi: InstansiDto;

  @IsNumber()
  estimasiWaktuPelayanan: number;

  @DateFormated('HH:mm')
  jamBukaPelayanan: string;

  @DateFormated('HH:mm')
  jamTutupPelayanan: string;
}
