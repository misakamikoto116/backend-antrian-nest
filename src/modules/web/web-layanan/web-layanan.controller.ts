import { LayananService } from '@entities/layanan/layanan.service';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
} from '@nestjs/common';
import { ValidationPipe } from '@system/pipes/validation.pipe';

import { LayananDto } from './dto/layanan.dto';
import { Layanan } from '@entities/layanan/layanan.entity';
import { FastifyRequest } from 'fastify';
import { WhoCanAccess } from '@system/decorators/guards/who-can-access.decorator';

@Controller()
@WhoCanAccess('Admin')
export class WebLayananController {
  constructor(private readonly modelService: LayananService) { }

  @Get()
  async getItems(@Req() req: FastifyRequest): Promise<{}> {
    return await this.modelService.getItems(req);
  }

  @Post()
  async storeItems(
    @Body(new ValidationPipe())
    layananRequest: LayananDto,
  ): Promise<any> {
    return await this.modelService.storeItems(layananRequest as Layanan);
  }

  @Get(':id')
  getSingleItem(@Param('id') itemId: string): any {
    return this.modelService.getSingleItem(itemId);
  }

  @Patch(':id')
  async updateItem(
    @Body(new ValidationPipe())
    layananRequest: LayananDto,
    @Param('id') itemId: string,
  ): Promise<any> {
    return await this.modelService.UpdateItem(
      layananRequest as Layanan,
      itemId,
    );
  }

  @Delete(':id')
  async deleteItem(@Param('id') itemId: string): Promise<any> {
    return this.modelService.deleteItem(itemId);
  }
}
