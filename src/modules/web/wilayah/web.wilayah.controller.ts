import { WilayahService } from '@entities/wilayah/wilayah.service';
import {
  Body,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  UseGuards,
  Inject,
  Logger,
} from '@nestjs/common';
import { ApiImplicitBody } from '@nestjs/swagger';
import { BadRequestException } from '@system/execptions/BadRequestException';
import { JwtHttpGuard } from '@system/guards/jwt-http.guard';
import { ValidationPipe } from '@system/pipes/validation.pipe';
import { plainToClass, plainToClassFromExist } from 'class-transformer';
import { ClassType } from 'class-transformer/ClassTransformer';
import { validate, Validator } from 'class-validator';
import { Pagination } from 'nestjs-typeorm-paginate';
import { isObject } from 'util';

export abstract class WebWilayahController<Entity, DTO> {
  private dto: DTO;
  constructor(
    private readonly wilayahService: WilayahService<Entity, DTO>,
    dto: ClassType<DTO>,
    private readonly relations: string[],
  ) {
    this.dto = new dto();
  }

  @Get()
  async fetchAllData(
    @Query('page') page: number = 1,
  ): Promise<Pagination<Entity>> {
    return await this.wilayahService.list(page, 10, this.relations);
  }
  @Get(':id')
  async fetchOne(@Param('id', ParseIntPipe) id: number): Promise<Entity> {
    return this.wilayahService.findOne(id, this.relations);
  }

  @Post()
  async postData(@Body() userInput: DTO): Promise<Entity> {
    const objectDto = await this.validation(userInput);
    return await this.wilayahService.create(objectDto);
  }
  private async validation(userInput: DTO) {
    if (!isObject(userInput)) {
      throw new BadRequestException('Body request must json');
    }
    const objectDto = plainToClassFromExist(this.dto, userInput);
    const errors = await validate(objectDto);
    if (errors.length > 0) {
      throw new BadRequestException(errors);
    }
    return objectDto;
  }

  @Put(':id')
  async updateData(
    @Param('id', new ParseIntPipe()) id: number,
    @Body() userInput: DTO,
  ): Promise<Entity> {
    const objectDto = await this.validation(userInput);
    return await this.wilayahService.update(id, objectDto);
  }
  @Delete(':id')
  async deleteData(@Param('id', new ParseIntPipe()) id: number): Promise<void> {
    return await this.wilayahService.delete(id);
  }
}
