import { WilayahModule } from '@entities/wilayah/wilayah.module';
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { systemConstant } from '@system/constants/system.constant';

import { KabupatenModule as WebKabupatenModule } from './kabupaten/kabupaten.module';
import { KecamatanModule as WebKecamatanModule } from './kecamatan/kecamatan.module';
import { KelurahanModule as WebKelurahanModule } from './kelurahan/kelurahan.module';
import { ProvinsiModule as ProvinsiModule } from './provinsi/provinsi.module';

@Module({
  imports: [ProvinsiModule, WebKabupatenModule, WebKecamatanModule, WebKelurahanModule, WilayahModule],
})
export class WebWilayahModule {}
