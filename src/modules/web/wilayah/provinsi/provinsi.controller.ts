import { ProvinsiDto } from '@entities/wilayah/provinsi/dtos/provinsi.dto';
import { Provinsi } from '@entities/wilayah/provinsi/provinsi.entity';
import { ProvinsiService } from '@entities/wilayah/provinsi/provinsi.service';
import { Controller } from '@nestjs/common';

import { WebWilayahController } from '../web.wilayah.controller';
import { WhoCanAccess } from '@system/decorators/guards/who-can-access.decorator';

@Controller()
@WhoCanAccess('Admin')
export class ProvinsiController extends WebWilayahController<
  Provinsi,
  ProvinsiDto
> {
  constructor(provinsiService: ProvinsiService) {
    super(provinsiService, ProvinsiDto, []);
  }
}
