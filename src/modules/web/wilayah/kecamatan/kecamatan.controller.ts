import { KecamatanDto } from '@entities/wilayah/kecamatan/dtos/kecamatan.dto';
import { Kecamatan } from '@entities/wilayah/kecamatan/kecamatan.entity';
import { KecamatanService } from '@entities/wilayah/kecamatan/kecamatan.service';
import { Controller } from '@nestjs/common';

import { WebWilayahController } from '../web.wilayah.controller';
import { WhoCanAccess } from '@system/decorators/guards/who-can-access.decorator';

@Controller()
@WhoCanAccess('Admin')
export class KecamatanController extends WebWilayahController<
  Kecamatan,
  KecamatanDto
> {
  constructor(provinsiService: KecamatanService) {
    super(provinsiService, KecamatanDto, ['kabupaten']);
  }
}
