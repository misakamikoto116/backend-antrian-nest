import { KelurahanDto } from '@entities/wilayah/kelurahan/dtos/kelurahan.dto';
import { Kelurahan } from '@entities/wilayah/kelurahan/kelurahan.entity';
import { KelurahanService } from '@entities/wilayah/kelurahan/kelurahan.service';
import { Controller } from '@nestjs/common';

import { WebWilayahController } from '../web.wilayah.controller';
import { WhoCanAccess } from '@system/decorators/guards/who-can-access.decorator';

@Controller()
@WhoCanAccess('Admin')
export class KelurahanController extends WebWilayahController<
  Kelurahan,
  KelurahanDto
> {
  constructor(provinsiService: KelurahanService) {
    super(provinsiService, KelurahanDto, ['kecamatan']);
  }
}
