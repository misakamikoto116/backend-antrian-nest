import { WilayahModule } from '@entities/wilayah/wilayah.module';
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { systemConstant } from '@system/constants/system.constant';

import { KelurahanController } from './kelurahan.controller';

@Module({
  imports: [WilayahModule,
    JwtModule.register({
      secret: systemConstant.jwtSecreat,
    })],
  controllers: [KelurahanController],
})
export class KelurahanModule {}
