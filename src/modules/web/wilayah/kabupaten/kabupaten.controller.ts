import { KabupatenDto } from '@entities/wilayah/kabupaten/dtos/kabupaten.dto';
import { Kabupaten } from '@entities/wilayah/kabupaten/kabupaten.entity';
import { KabupatenService } from '@entities/wilayah/kabupaten/kabupaten.service';
import { Controller } from '@nestjs/common';

import { WebWilayahController } from '../web.wilayah.controller';
import { WhoCanAccess } from '@system/decorators/guards/who-can-access.decorator';

@Controller()
@WhoCanAccess('Admin')
export class KabupatenController extends WebWilayahController<
  Kabupaten,
  KabupatenDto
> {
  constructor(wilayahService: KabupatenService) {
    super(wilayahService, KabupatenDto, ['provinsi']);
  }
}
