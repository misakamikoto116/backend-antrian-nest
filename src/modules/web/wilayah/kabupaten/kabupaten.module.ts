import { WilayahModule } from '@entities/wilayah/wilayah.module';
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { systemConstant } from '@system/constants/system.constant';

import { KabupatenController } from './kabupaten.controller';

@Module({
  imports: [
    WilayahModule,
    JwtModule.register({
      secret: systemConstant.jwtSecreat,
    }),
  ],
  controllers: [KabupatenController],
})
export class KabupatenModule {}
