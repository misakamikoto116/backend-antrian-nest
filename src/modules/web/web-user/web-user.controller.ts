import {
  Controller,
  ValidationPipe,
  UseInterceptors,
  Req,
  BadRequestException,
} from '@nestjs/common';
import {
  Crud,
  CrudRequestInterceptor,
  Override,
  CrudRequest,
  CrudController,
  ParsedBody,
  ParsedRequest,
} from '@nestjsx/crud';
import { WebUserService } from './web-user.service';
import { UserDto } from './dtos/user.dto';
import { parseError } from '@system/helpers/parseError';
import { WhoCanAccess } from '@system/decorators/guards/who-can-access.decorator';
import { User } from '@entities/user/user.entity';
import { RequestWithUser } from '@system/interfaces/request-with-user';

@Crud({
  model: {
    type: UserDto,
  },
  validation: {
    validateCustomDecorators: true,
    exceptionFactory: parseError,
  },
  query: {
    limit: 10,
    join: {
      roles: {
        persist: ['title'],
        eager: true,
      },
    },
  },
})
@Controller()
@WhoCanAccess('Admin')
export class WebUserController implements CrudController<UserDto> {
  constructor(public service: WebUserService) {}

  public get base(): CrudController<UserDto> {
    return this;
  }

  @Override('createOneBase')
  async create(
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dto: UserDto,
    @Req() request: RequestWithUser,
  ) {
    if (!dto.roles) {
      dto.roles = await this.service.findRole('User');
    } else {
      if (request.user.roles.findIndex(u => u.title === 'Admin') === -1) {
        throw new BadRequestException('Can\'t add user with other role');
      }
    }
    return this.base.createOneBase(req, dto);
  }
  @Override('updateOneBase')
  async replace(
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dto: UserDto,
    @Req() request: RequestWithUser,
  ) {
    if (request.user.roles.findIndex(u => u.title === 'Admin') === -1) {
      throw new BadRequestException('Can\'t add user with other role');
    }
    return this.base.updateOneBase(req, dto);
  }
}
