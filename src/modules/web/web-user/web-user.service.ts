import {
  Injectable,
  UseInterceptors,
  Scope,
  Req,
  BadRequestException,
} from '@nestjs/common';
import { User } from '@entities/user/user.entity';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, In } from 'typeorm';
import { CrudRequestInterceptor, Override, CrudRequest } from '@nestjsx/crud';
import { Role } from '@entities/role/role.entity';
import { RequestWithUser } from '@system/interfaces/request-with-user';

@Injectable()
export class WebUserService extends TypeOrmCrudService<User> {
  findRole(role: string) {
    return this.roleRepo.find({
      where: {
        title: 'User',
      },
    });
  }
  constructor(
    @InjectRepository(User) repo: Repository<User>,
    @InjectRepository(Role) private readonly roleRepo: Repository<Role>,
  ) {
    super(repo);
  }

  @UseInterceptors(CrudRequestInterceptor)
  @Override()
  replaceOne(req: CrudRequest, dto: User) {
    return super.createOne(req, dto);
  }
}
