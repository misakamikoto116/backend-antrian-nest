import { IsDefined, IsNumber, Validate } from 'class-validator';
import { Role } from '@entities/role/role.entity';
import { InDatabase } from '@system/decorators/validation/in.database.validation';
import { ValidateIfDefined } from '@system/decorators/validation/If-defined.validation';
import { CrudValidationGroups } from '@nestjsx/crud';
const { CREATE, UPDATE } = CrudValidationGroups;
export class RoleDto {
  @IsDefined({ groups: [UPDATE, CREATE] })
  @IsNumber({}, { groups: [UPDATE, CREATE] })
  @ValidateIfDefined({ groups: [UPDATE, CREATE] })
  @InDatabase(Role, undefined, {}, { groups: [UPDATE, CREATE] })
  id: number;
}
