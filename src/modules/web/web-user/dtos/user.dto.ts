import {
  IsDefined,
  ValidateNested,
  ValidateIf,
  IsArray,
  IsString,
  IsNumberString,
} from 'class-validator';

import { RoleDto } from './role.dto';

import { Type } from 'class-transformer';
import { ValidateIfDefined } from '@system/decorators/validation/If-defined.validation';
import { CrudValidationGroups } from '@nestjsx/crud';
import { UniqueInDatabase } from '@system/decorators/validation/unique.database.validation';
import { User } from '@entities/user/user.entity';
const { CREATE, UPDATE } = CrudValidationGroups;

export class UserDto {
  @IsDefined({ groups: [CREATE] })
  @IsString({ groups: [UPDATE, CREATE] })
  @ValidateIfDefined({ groups: [UPDATE] })
  namaDepan?: string;
  @IsDefined({ groups: [CREATE] })
  @IsString({ groups: [UPDATE, CREATE] })
  @ValidateIfDefined({ groups: [UPDATE] })
  namaBelakang: string;
  @IsDefined({ groups: [CREATE] })
  @IsString({ groups: [UPDATE, CREATE] })
  @ValidateIfDefined({ groups: [UPDATE] })
  @IsNumberString({ groups: [UPDATE, CREATE] })
  @UniqueInDatabase(User, 'noKTP', {}, { groups: [UPDATE, CREATE] })
  noKTP: string;
  @IsDefined({ groups: [CREATE] })
  @IsString({ groups: [UPDATE, CREATE] })
  @ValidateIfDefined({ groups: [UPDATE] })
  @IsNumberString({ groups: [UPDATE, CREATE] })
  noHp: string;
  @IsDefined({ groups: [CREATE] })
  @IsString({ groups: [UPDATE, CREATE] })
  @ValidateIfDefined({ groups: [UPDATE] })
  tanggalLahir: string;
  @IsDefined({ groups: [CREATE] })
  @IsString({ groups: [UPDATE, CREATE] })
  @ValidateIfDefined({ groups: [UPDATE] })
  tempatLahir: string;
  @IsDefined({ groups: [CREATE] })
  @IsString({ groups: [UPDATE, CREATE] })
  @ValidateIfDefined({ groups: [UPDATE] })
  email: string;
  @ValidateIfDefined({ groups: [UPDATE, CREATE] })
  @IsArray({ groups: [UPDATE, CREATE] })
  @IsDefined({ groups: [CREATE] })
  @ValidateNested({ groups: [UPDATE, CREATE] })
  @Type(() => RoleDto)
  roles: RoleDto[];
}
