import { Module } from '@nestjs/common';
import { WebUserService } from './web-user.service';
import { WebUserController } from './web-user.controller';
import { User } from '@entities/user/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Role } from '@entities/role/role.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User, Role])],
  providers: [WebUserService],
  controllers: [WebUserController],
})
export class WebUserModule {}
