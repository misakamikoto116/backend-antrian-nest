import { Module } from '@nestjs/common';
import { WebAnjunganService } from './web-anjungan.service';
import { WebAnjunganController } from './web-anjungan.controller';
import { AnjunganModule } from '@entities/anjungan/anjungan.module';
import { PrinterModule } from '@entities/printer/printer.module';

@Module({
  imports: [AnjunganModule, PrinterModule],
  providers: [WebAnjunganService],
  controllers: [WebAnjunganController],
})
export class WebAnjunganModule {}
