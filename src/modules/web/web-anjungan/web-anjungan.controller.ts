import { Controller, Get, Param, Request } from '@nestjs/common';
import { parseError } from '@system/helpers/parseError';
import { WebAnjunganService } from './web-anjungan.service';
import { AnjunganDto } from './dtos/anjungan.dto';
import {
  Crud,
  ParsedRequest,
  CrudRequest,
  Override,
  GetManyDefaultResponse,
} from '@nestjsx/crud';
import { FastifyRequest } from 'fastify';

@Crud({
  model: {
    type: AnjunganDto,
  },
  validation: {
    validateCustomDecorators: true,
    exceptionFactory: parseError,
  },
  routes: {},

  query: {
    limit: 10,
    join: {
      instansi: {
        eager: true,
      },
    },
  },
})
@Controller('')
export class WebAnjunganController {
  constructor(public service: WebAnjunganService) {}
  @Get(':id/printer')
  getListPrinterInstansi(
    @Param('id') instansiId: number,
    @Request() req: FastifyRequest,
  ) {
    return this.service.getListPrinterInInstansi(instansiId, req);
  }
  @Override('getOneBase')
  async findOne(@ParsedRequest() req: CrudRequest) {
    const item = await this.service.getOne(req);
    const printer = await this.service.findPrinter(item.id);
    return { ...item, printer };
  }
  @Override('getManyBase')
  async findMany(@ParsedRequest() req: CrudRequest) {
    const res = ((await this.service.getMany(
      req,
    )) as unknown) as GetManyDefaultResponse<AnjunganDto>;
    res.data = await Promise.all(
      res.data.map(async rItem => {
        const printer = await this.service.findPrinter(rItem.id);
        return { ...rItem, printer };
      }),
    );
    return res;
  }
}
