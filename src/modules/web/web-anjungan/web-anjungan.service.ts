import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Anjungan } from '@entities/anjungan/anjungan.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { WebPrinterService } from '../web-printer/web-printer.service';
import { paginate } from 'nestjs-typeorm-paginate';
import { Printer } from '@entities/printer/printer.entity';

@Injectable()
export class WebAnjunganService extends TypeOrmCrudService<Anjungan> {
  findPrinter(id: number) {
    return this.printer.findOne({
      where: {
        anjungan: {
          id,
        },
      },
    });
  }
  constructor(
    @InjectRepository(Anjungan) repo: Repository<Anjungan>,
    @InjectRepository(Printer) private readonly printer: Repository<Printer>,
  ) {
    super(repo);
  }
  public getListPrinterInInstansi(instansiId: number, req) {
    const qb = this.printer
      .createQueryBuilder('printer')
      .where('printer.instansiId = :instansiId', { instansiId });
    return paginate(qb, {
      limit: 10,
      page: req.query.page || 0,
    });
  }
}
