import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { LoketDto } from './dtos/loket.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Loket } from '@entities/loket/loket.entity';
import { Layar } from '@entities/layar/layar.entity';
import { In } from 'typeorm';

@Injectable()
export class WebLoketService extends TypeOrmCrudService<LoketDto> {
  findLayar(id: number) {
    return this.repoLayar.findOne(id);
  }
  constructor(
    @InjectRepository(Loket) repo,
    @InjectRepository(Layar) private readonly repoLayar,
  ) {
    super(repo);
  }
}
