import { LoketModule } from '@entities/loket/loket.module';
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { systemConstant } from '@system/constants/system.constant';

import { WebLoketController } from './web-loket.controller';
import { WebLoketService } from './web-loket.service';
import { Loket } from '@entities/loket/loket.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Layar } from '@entities/layar/layar.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Loket, Layar]),
    LoketModule,
    JwtModule.register({
      secret: systemConstant.jwtSecreat,
    }),
  ],
  controllers: [WebLoketController],
  providers: [WebLoketService],
})
export class WebLoketModule {}
