import { Controller, Query } from '@nestjs/common';
import { CrudController } from '@system/modules/crud.controller';
import { Loket } from '@entities/loket/loket.entity';
import { LoketDto } from './dtos/loket.dto';
import { LoketService } from '@entities/loket/loket.service';
import {
  Crud,
  Override,
  ParsedRequest,
  CrudRequest,
  GetManyDefaultResponse,
} from '@nestjsx/crud';
import { WebLoketService } from './web-loket.service';
import { WhoCanAccess } from '@system/decorators/guards/who-can-access.decorator';
import { ParsePagePipe } from '@system/pipes/parse-page.pipe';
import { async } from 'rxjs/internal/scheduler/async';
import { parseError } from '@system/helpers/parseError';
@Crud({
  model: {
    type: LoketDto,
  },
  validation: {
    validateCustomDecorators: true,
    exceptionFactory: parseError,
  },
  query: {
    limit: 10,
    join: {
      // layar: {
      //   eager: true,
      //   exclude: ['instansi'],
      // },
      layanan: {
        eager: true,
        exclude: ['instansi'],
      },
    },
  },
})
@Controller()
@WhoCanAccess('Admin')
export class WebLoketController {
  constructor(private service: WebLoketService) {}
  @Override('getOneBase')
  async findOne(@ParsedRequest() req: CrudRequest) {
    const item = await this.service.getOne(req);
    const layar = await this.service.findLayar(item.id);
    return { ...item, layar };
  }
  @Override('getManyBase')
  async findMany(@ParsedRequest() req: CrudRequest) {
    const res = (await this.service.getMany(req)) as GetManyDefaultResponse<
      LoketDto
    >;
    res.data = await Promise.all(
      res.data.map(async rItem => {
        const layar = await this.service.findLayar(rItem.id);
        return { ...rItem, layar };
      }),
    );
    return res;
  }
}
