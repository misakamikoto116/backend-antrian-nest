import { Layanan } from '@entities/layanan/layanan.entity';
import { Validate, IsNumber } from 'class-validator';
import { InDatabase } from '@system/decorators/validation/in.database.validation';
import { CrudValidationGroups } from '@nestjsx/crud';
const { CREATE, UPDATE } = CrudValidationGroups;
export class LayananDto {
  @InDatabase(Layanan, 'id', {}, { groups: [CREATE, UPDATE] })
  @IsNumber({}, { groups: [CREATE, UPDATE] })
  id: number;
}
