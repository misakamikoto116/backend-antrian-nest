import { Layar } from '@entities/layar/layar.entity';
import { IsNumber } from 'class-validator';
import { InDatabase } from '@system/decorators/validation/in.database.validation';
import { CrudValidationGroups } from '@nestjsx/crud';
import { ValidateIfDefined } from '@system/decorators/validation/If-defined.validation';
const { CREATE, UPDATE } = CrudValidationGroups;
export class LayarDto {
  @ValidateIfDefined()
  @InDatabase(Layar, 'id', {}, { groups: [CREATE, UPDATE] })
  @IsNumber({}, { groups: [CREATE, UPDATE] })
  id: number;
}
