import {
  IsDefined,
  IsString,
  IsBoolean,
  ValidateNested,
  ValidateIf,
} from 'class-validator';
import { Type } from 'class-transformer';
import { LayananDto } from './Layanan.dto';
import { LayarDto } from './Layar.dto';
import { CrudValidationGroups } from '@nestjsx/crud';
import { isNullOrUndefined } from 'util';
const { CREATE, UPDATE } = CrudValidationGroups;
export class LoketDto {
  id: number;
  @IsDefined({ groups: [CREATE, UPDATE] })
  @IsString({ groups: [CREATE, UPDATE] })
  kode: string;

  @IsDefined({ groups: [CREATE, UPDATE] })
  @IsString({ groups: [CREATE, UPDATE] })
  nama: string;

  @IsDefined({ groups: [CREATE, UPDATE] })
  @IsBoolean({ groups: [CREATE, UPDATE] })
  aktif: boolean;

  @ValidateIf((o, v) => isNullOrUndefined(v), {
    groups: [CREATE, UPDATE],
  })
  @ValidateNested({ groups: [CREATE, UPDATE] })
  @Type(() => LayarDto)
  layar: LayarDto;

  @IsDefined({ groups: [CREATE, UPDATE] })
  @ValidateNested({ groups: [CREATE, UPDATE] })
  @Type(() => LayananDto)
  layanan: LayananDto;
}
