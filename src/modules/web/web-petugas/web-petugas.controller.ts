import { Controller } from '@nestjs/common';
import { WebPetugasService } from './web-petugas.service';
import { Crud } from '@nestjsx/crud';
import { PetugasDto } from './dtos/petugas.dto';
import { parseError } from '@system/helpers/parseError';
import { WhoCanAccess } from '@system/decorators/guards/who-can-access.decorator';

@Controller()
@Crud({
  model: {
    type: PetugasDto,
  },
  query: {
    join: {
      user: {
        eager: true,
      },
      instansi: {
        eager: true,
        allow: ['nama', 'id'],
      },
    },
    limit: 10,
  },
  validation: {
    validateCustomDecorators: true,
    exceptionFactory: parseError,
  },
})
@WhoCanAccess('Admin')
export class WebPetugasController {
  constructor(public service: WebPetugasService) {}
}
