import { Role } from '@entities/role/role.entity';
import { IsString, IsDefined, ValidateNested } from 'class-validator';
import { CrudValidationGroups } from '@nestjsx/crud';
import { InstansiDto } from './instansi.dto';
import { RegisterDto } from '@system/dto/register.dto';
import { UserDto } from './user.dto';
import { Type } from 'class-transformer';
const { CREATE, UPDATE } = CrudValidationGroups;
export class PetugasDto {
  @IsDefined({ groups: [CREATE, UPDATE] })
  @ValidateNested({ groups: [UPDATE, CREATE] })
  @Type(() => InstansiDto)
  instansi: InstansiDto;

  @IsDefined({ groups: [CREATE, UPDATE] })
  @ValidateNested({ groups: [UPDATE, CREATE] })
  @Type(() => UserDto)
  user: UserDto;
}
