import {
  IsDefined,
  IsNumber,
  IsString,
  IsEmail,
  IsNotEmpty,
} from 'class-validator';
import { CrudValidationGroups } from '@nestjsx/crud';
import { RegisterDto } from '@system/dto/register.dto';
import { UniqueInDatabase } from '@system/decorators/validation/unique.database.validation';
import { User } from '@entities/user/user.entity';
import { DateFormated } from '@system/decorators/validation/datestring.validation';
import { Not } from 'typeorm';
const { CREATE, UPDATE } = CrudValidationGroups;
export class UserDto {
  id: number;
  @IsString({ groups: [UPDATE, CREATE] })
  @IsDefined({ groups: [UPDATE, CREATE] })
  @IsEmail({}, { groups: [UPDATE, CREATE] })
  @UniqueInDatabase(User, 'email', {}, { groups: [CREATE] })
  email: string;

  @IsString({ groups: [UPDATE, CREATE] })
  @IsDefined({ groups: [UPDATE, CREATE] })
  @IsNotEmpty({ groups: [UPDATE, CREATE] })
  namaDepan: string;

  @IsString({ groups: [UPDATE, CREATE] })
  @IsDefined({ groups: [UPDATE, CREATE] })
  @IsNotEmpty({ groups: [UPDATE, CREATE] })
  namaBelakang: string;

  @IsString({ groups: [UPDATE, CREATE] })
  @IsDefined({ groups: [UPDATE, CREATE] })
  @IsNotEmpty({ groups: [UPDATE, CREATE] })
  @UniqueInDatabase(User, 'noKTP', {}, { groups: [CREATE] })
  noKTP: string;

  @IsString({ groups: [UPDATE, CREATE] })
  @IsDefined({ groups: [UPDATE, CREATE] })
  @IsNotEmpty({ groups: [UPDATE, CREATE] })
  @UniqueInDatabase(User, 'noHp', {}, { groups: [CREATE] })
  noHp: string;

  @IsString({ groups: [UPDATE, CREATE] })
  @IsDefined({ groups: [UPDATE, CREATE] })
  @IsNotEmpty({ groups: [UPDATE, CREATE] })
  @DateFormated('YYYY-MM-DD', { groups: [UPDATE, CREATE] })
  tanggalLahir: string;

  @IsString({ groups: [UPDATE, CREATE] })
  @IsDefined({ groups: [UPDATE, CREATE] })
  @IsNotEmpty({ groups: [UPDATE, CREATE] })
  tempatLahir: string;
}
