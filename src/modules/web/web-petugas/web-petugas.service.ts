import { Injectable } from '@nestjs/common';
import { CrudService, CrudRequest } from '@nestjsx/crud';
import { PetugasDto } from './dtos/petugas.dto';
import { Petugas } from '@entities/petugas/petugas.entity';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '@entities/user/user.entity';
import {
  Repository,
  Not,
  Brackets,
  getConnection,
  EntityManager,
} from 'typeorm';
import { UserDto } from './dtos/user.dto';
import { Role } from '@entities/role/role.entity';
import { BadRequestException } from '@system/execptions/BadRequestException';

@Injectable()
export class WebPetugasService extends TypeOrmCrudService<PetugasDto> {
  constructor(
    @InjectRepository(Petugas) repo: Repository<Petugas>,
    @InjectRepository(User) private readonly user: Repository<User>,
    @InjectRepository(Role) private readonly role: Repository<Role>,
  ) {
    super(repo as any);
  }
  async createOne(req: CrudRequest, dto: PetugasDto): Promise<PetugasDto> {
    return getConnection().transaction(
      manager =>
        new Promise((resolve, reject) => {
          this.updateUser(dto.user as User, manager)
            .then(user => {
              return this.repo.merge(new Petugas(), { ...dto, ...{ user } });
            })
            .then(newDto => {
              manager.save(newDto).then(resolve, reject);
            })
            .catch(reject);
        }),
    );
  }
  async updateOne(req: CrudRequest, dto: PetugasDto): Promise<PetugasDto> {
    return getConnection().transaction(
      manager =>
        new Promise((resolve, reject) => {
          super.getOne(req).then(model => {
            this.user
              .createQueryBuilder('u')
              .where({
                id: Not(model.user.id),
              })
              .andWhere(
                new Brackets(qb => {
                  qb.orWhere('u.noKTP = :noKTP');
                  qb.orWhere('u.noHp = :noHp');
                  qb.orWhere('u.email = :email');
                }),
              )
              .setParameters({
                noKTP: dto.user.noKTP,
                noHp: dto.user.noHp,
                email: dto.user.email,
              })
              .getCount()
              .then(count => {
                if (count > 0) {
                  reject(
                    new BadRequestException(
                      'Data updated is duplicate with other data',
                    ),
                  );
                } else {
                  this.updateUser(
                    dto.user as User,
                    manager,
                    model.user.id,
                  ).then(async user => {
                    const petugas = await this.repo.findOne({
                      where: { user },
                    });
                    petugas.instansi = dto.instansi;
                    manager.save(petugas).then(resolve, reject);
                  }, reject);
                }
              }, reject);
          }, reject);
        }),
    );
  }
  updateUser(
    user: User,
    manager: EntityManager,
    userId?: number,
  ): Promise<UserDto> {
    return new Promise((resolve, reject) => {
      this.role
        .findOne({
          where: {
            title: 'Petugas',
          },
        })
        .then(role => {
          if (userId) {
            this.user
              .findOne({ where: { id: userId }, relations: ['roles'] })
              .then(userDb => {
                userDb = this.user.merge(userDb, user);
                const hasRole = userDb.roles.filter(
                  roleUser => roleUser.title === 'Petugas',
                );
                if (hasRole.length < 1) {
                  userDb.roles.push(role);
                }
                manager
                  .save(this.user.merge(new User(), userDb))
                  .then(resolve, reject);
              })
              .catch(reject);
          } else {
            user.roles = [role];
            manager
              .save(this.user.merge(new User(), user))
              .then(resolve, reject);
          }
        })
        .catch(reject);
    });
  }
}
