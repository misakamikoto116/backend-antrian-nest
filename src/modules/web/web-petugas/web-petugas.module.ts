import { Module } from '@nestjs/common';
import { RoleModule } from '@entities/role/role.module';
import { PetugasModule } from '@entities/petugas/petugas.module';
import { WebPetugasController } from './web-petugas.controller';
import { WebPetugasService } from './web-petugas.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '@entities/user/user.entity';
import { Role } from '@entities/role/role.entity';

@Module({
  controllers: [WebPetugasController],
  imports: [RoleModule, PetugasModule, TypeOrmModule.forFeature([User, Role])],
  providers: [WebPetugasService],
})
export class WebPetugasModule {}
