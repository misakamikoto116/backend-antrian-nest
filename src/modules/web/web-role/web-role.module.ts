import { Module } from '@nestjs/common';
import { WebRoleController } from './web-role.controller';
import { RoleModule } from '@entities/role/role.module';
import { WebRoleService } from './web-role.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Role } from '@entities/role/role.entity';

@Module({
  imports: [RoleModule, TypeOrmModule.forFeature([Role])],
  controllers: [WebRoleController],
  providers: [WebRoleService],
})
export class WebRoleModule {}
