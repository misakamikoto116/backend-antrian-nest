import { Controller } from '@nestjs/common';
import { CrudController } from '@system/modules/crud.controller';
import { Role } from '@entities/role/role.entity';
import { RoleDto } from './dto/role.dto';
import { RoleService } from '@entities/role/role.service';
import { Crud } from '@nestjsx/crud';
import { WebRoleService } from './web-role.service';
import { WhoCanAccess } from '@system/decorators/guards/who-can-access.decorator';
import { parseError } from '@system/helpers/parseError';
@Crud({
  model: {
    type: RoleDto,
  },
  validation: {
    validateCustomDecorators: true,
    exceptionFactory: parseError,
  },
  query: {
    limit: 10,
  },
})
@WhoCanAccess('Admin')
@Controller()
export class WebRoleController {
  constructor(private service: WebRoleService) {}
}
