import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { RoleDto } from './dto/role.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Role } from '@entities/role/role.entity';

@Injectable()
export class WebRoleService extends TypeOrmCrudService<RoleDto> {
  constructor(@InjectRepository(Role) repo) {
    super(repo);
  }
}
