import { Role } from '@entities/role/role.entity';
import { isNumber } from 'util';
import { IsString, IsDefined } from 'class-validator';

export class RoleDto {
  @IsString()
  @IsDefined()
  title: string;
}
