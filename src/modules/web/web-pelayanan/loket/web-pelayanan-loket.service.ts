import { Antrian } from '@entities/antrian/antrian.entity';
import { AntrianService } from '@entities/antrian/antrian.service';
import { Injectable } from '@nestjs/common';
import { RethinkDbService } from '@modules/rethink-db/rethinkdb.provider';
import { Repository } from 'typeorm';
import { Layanan } from '@entities/layanan/layanan.entity';
import { Loket } from '@entities/loket/loket.entity';
import { paginate } from 'nestjs-typeorm-paginate';
import { InjectRepository } from '@nestjs/typeorm';
import { TokenFCM } from '@entities/user/tokenFcm.entity';
import { isObject } from 'util';
import { todayStringFormated } from '@system/helpers/nowStringFormated';

@Injectable()
export class WebPelayananLoketService {
  getRecallDataAtLoket(id: number): Promise<Antrian> {
    return this.antrianRepo
      .createQueryBuilder('antrian')
      .innerJoinAndSelect('antrian.antrianUser', 'antrianUser')
      .innerJoinAndSelect('antrianUser.user', 'user')
      .innerJoinAndSelect('user.tokenFCM', 'tokenFCM')
      .where('antrian.loketId = :id', { id })
      .orderBy('antrian.id', 'DESC')
      .limit(1)
      .getOne();
  }
  tokenUserNextQueue(calledNext: number[], calledNow: number) {
    return this.tokenRepository
      .createQueryBuilder('token')
      .innerJoin('token.user', 'user')
      .innerJoin('user.antrianUser', 'antrianUser')
      .innerJoin(
        'antrianUser.antrian',
        'antrian',
        'antrian.nomor in (:nomors) and tanggal = :tanggal',
        {
          nomors: calledNext.map(i => i + calledNow),
          tanggal: todayStringFormated(),
        },
      )
      .select([
        'token',
        'token.user',
        'user.namaDepan',
        'user.namaBelakang',
        'antrianUser.id',
        'antrianUser.antrian',
        'antrian.nomor',
      ])
      .getMany();
  }
  getLayanan(instansiId: number, page: number) {
    return paginate(
      this.layananRepository
        .createQueryBuilder('layanan')
        .innerJoin('layanan.loket', 'loket', 'loket.aktif = :aktif', {
          aktif: true,
        }),
      {
        page,
        limit: 15,
      },
    );
  }
  getLoket(layananId: number, page: number) {
    return paginate(
      this.loketRepository,
      {
        page,
        limit: 15,
      },
      {
        where: {
          layanan: {
            id: layananId,
          },
          aktif: true,
        },
      },
    );
  }

  constructor(
    @InjectRepository(Layanan)
    private readonly layananRepository: Repository<Layanan>,
    @InjectRepository(Loket)
    private readonly loketRepository: Repository<Loket>,
    @InjectRepository(TokenFCM)
    private readonly tokenRepository: Repository<TokenFCM>,
    @InjectRepository(Antrian)
    private readonly antrianRepo: Repository<Antrian>,
    private readonly antrianService: AntrianService,
  ) {}
  callNext(petugasId: number, layananId: number, loketId: number) {
    return this.antrianService.callNextAntrian(petugasId, layananId, loketId);
  }
  findOneLoket(loketId: number) {
    return this.loketRepository.findOneOrFail({ where: { id: loketId } });
  }
}
