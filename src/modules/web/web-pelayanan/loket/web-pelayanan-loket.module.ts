import { AntrianModule } from '@entities/antrian/antrian.module';
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { systemConstant } from '@system/constants/system.constant';

import { WebPelayananLoketController } from './web-pelayanan-loket.controller';
import { WebPelayananLoketService } from './web-pelayanan-loket.service';
import { RethinkDbModule } from '@modules/rethink-db/rethink-db.module';
import { LoketModule } from '@entities/loket/loket.module';
import { LayananModule } from '@entities/layanan/layanan.module';
import { UserModule } from '@entities/user/user.module';
import { FirebaseModule } from '@modules/firebase/firebase.module';

@Module({
  imports: [
    AntrianModule,
    UserModule,
    FirebaseModule,
    RethinkDbModule,
    LoketModule,
    LayananModule,
    JwtModule.register({
      secret: systemConstant.jwtSecreat,
    }),
  ],
  controllers: [WebPelayananLoketController],
  providers: [WebPelayananLoketService],
})
export class WebPelayananLoketModule {}
