import { IsNumber, IsDefined } from 'class-validator';
import { InDatabase } from '@system/decorators/validation/in.database.validation';
import { Layanan } from '@entities/layanan/layanan.entity';
import { Loket } from '@entities/loket/loket.entity';
export class CallNextQueueDto {
  @IsNumber()
  @IsDefined()
  @InDatabase(Loket, 'id')
  loketId: number;
  @IsNumber()
  @IsDefined()
  @InDatabase(Layanan, 'id')
  layananId: number;
}
