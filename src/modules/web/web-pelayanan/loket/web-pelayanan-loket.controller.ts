import { Antrian } from '@entities/antrian/antrian.entity';
import {
  Body,
  Controller,
  Get,
  Post,
  Request,
  UseGuards,
  ParseIntPipe,
  Param,
  Query,
  Logger,
} from '@nestjs/common';
import { WhoCanAccess } from '@system/decorators/guards/who-can-access.decorator';
import { RequestWithUser } from '@system/interfaces/request-with-user';

import { WebPelayananLoketService } from './web-pelayanan-loket.service';

import { CallNextQueueDto } from './dtos/callNextQueue.dto';
import { ValidationPipe } from '@system/pipes/validation.pipe';
import { ParsePagePipe } from '@system/pipes/parse-page.pipe';
import { BadRequestException } from '@system/execptions/BadRequestException';
import { timeout } from 'cron';
import { isObject } from 'util';
import { FirebaseCloudMessageService } from '@modules/firebase/firebase-cloud-message.service';
import { RethinkDbService } from '@modules/rethink-db/rethinkdb.provider';
import { InjectEventEmitter } from 'nest-emitter';
import { CallerEvent } from 'src/events/Caller.event';

// @WhoCanAccess('Petugas')
@Controller()
@WhoCanAccess('Admin', 'Petugas')
export class WebPelayananLoketController {
  constructor(
    private readonly webLoketService: WebPelayananLoketService,
    private readonly firebaseService: FirebaseCloudMessageService,
    private readonly rethinkDBService: RethinkDbService,
    @InjectEventEmitter() private readonly eventService: CallerEvent,
  ) {}
  @Post('nextcall')
  handleNextCall(
    @Request() request: RequestWithUser,
    @Body(new ValidationPipe()) userInput: CallNextQueueDto,
  ) {
    const petugasId = request.user.id;
    const { loketId, layananId } = userInput;

    return new Promise((resolve, reject) => {
      this.webLoketService
        .callNext(petugasId, layananId, loketId)
        .then(calledQueue => {
          resolve(calledQueue);
          if (isObject(calledQueue)) {
            const calledNumber = calledQueue.nomor;
            this.rethinkDBService.updateInformationService(layananId);
            if (calledQueue.antrianUser !== undefined) {
              this.webLoketService.findOneLoket(loketId).then(loket => {
                this.rethinkDBService
                  .setLoket(calledQueue.id, loket)
                  .then(() => {
                    this.rethinkDBService.updateAntrianNow(layananId);
                  });
              });
            }

            this.webLoketService
              .tokenUserNextQueue([0, 1, 2, 3, 4, 5, 10, 15], calledNumber)
              .then(tokens => {
                tokens.forEach(({ token, user }) => {
                  if (token === '') {
                    return;
                  }
                  const nomorAntrian = user.antrianUser[0].antrian.nomor;
                  const namaUser = user.nama;
                  let body = `${namaUser} antrian kamu tinggal ${nomorAntrian -
                    calledNumber} orang lagi, siap-siap ya`;
                  let title = `${namaUser} antrian kamu sudah dekat`;
                  if (nomorAntrian - calledNumber === 0) {
                    body = `${namaUser}, sekarang giliran kamu untuk maju`;
                    title = `${namaUser} kamu udah di panggil tuh`;
                  }

                  const data = { body, title };

                  this.firebaseService
                    .sendToUser([token], data)
                    .then(result => {
                      Logger.log(result);
                    })
                    .catch(err => {
                      Logger.error(err);
                    });
                });
              }, reject);
          }
        }, reject);
    });
  }
  @Get('layanan')
  handleGetLayanan(
    @Request() req: RequestWithUser,
    @Query('page', new ParsePagePipe()) page: number,
  ) {
    if (
      req.user.petugas === undefined ||
      req.user.petugas.instansi === undefined ||
      req.user.roles.filter(role => role.title === 'Petugas').length < 1
    ) {
      throw new BadRequestException('User dont have role "petugas"');
    }
    const instansiId = req.user.petugas.instansi.id;
    return this.webLoketService.getLayanan(instansiId, page);
  }
  @Get('loket/:layanan')
  handleGetLoket(
    @Param('layanan', new ParseIntPipe()) layanan: number,
    @Query('page', new ParsePagePipe()) page: number,
  ) {
    return this.webLoketService.getLoket(layanan, page);
  }
  @Get('recall/:loketId/:nCall')
  async handleRecallInLoket(
    @Param('loketId') loketId: number,
    @Param('nCall') nCall: number,
  ) {
    const loket = await this.webLoketService.findOneLoket(loketId).catch(e => {
      Logger.error(e);
      throw new BadRequestException('Loket tidak ditemukan');
    });
    const antrianAtLoket = await this.webLoketService.getRecallDataAtLoket(
      loketId,
    );
    const voice: string = `Panggilan Ke- ${nCall}, untuk nomor antrian ${antrianAtLoket.nomor} di ${loket.nama} `;
    if (antrianAtLoket.antrianUser) {
      const userAntrian = antrianAtLoket.antrianUser.user;
      const tokens = userAntrian.tokenFCM.map(it => it.token);
      if (tokens.length > 0) {
        this.firebaseService.sendToUser(tokens, {
          body: `${userAntrian.nama} kamu sudah di panggil untuk ke ${loket.nama} (${loket.kode})`,
          title: `Panggilan ke ${nCall}`,
        });
      }
    }

    this.eventService.emit('makeVoice', voice, loketId);
  }
}
