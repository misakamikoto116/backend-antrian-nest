import { Module } from '@nestjs/common';
import { WebPelayananLoketModule } from './loket/web-pelayanan-loket.module';

@Module({
  imports: [WebPelayananLoketModule],
})
export class WebPelayananModule {}
