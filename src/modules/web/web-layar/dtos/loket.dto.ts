import { IsDefined, IsNumber } from 'class-validator';
import { Loket } from '@entities/loket/loket.entity';
import { InDatabase } from '@system/decorators/validation/in.database.validation';
import { CrudValidationGroups } from '@nestjsx/crud';
const { UPDATE, CREATE } = CrudValidationGroups;
export class LoketDto {
  // @IsDefined({ groups: [UPDATE, CREATE] })
  // @IsNumber({}, { groups: [UPDATE, CREATE] })
  @InDatabase(Loket, 'id', {}, { groups: [UPDATE, CREATE] })
  id: number;
}
