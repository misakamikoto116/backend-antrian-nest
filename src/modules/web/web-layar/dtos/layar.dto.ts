import {
  IsDefined,
  IsBoolean,
  IsString,
  IsArray,
  ValidateIf,
  ValidateNested,
  IsOptional,
} from 'class-validator';
import { Type } from 'class-transformer';
import { LoketDto } from './loket.dto';
import { InstansiDto } from './instansi.dto';
import { CrudValidationGroups } from '@nestjsx/crud';
import { isNullOrUndefined } from 'util';
const { UPDATE, CREATE } = CrudValidationGroups;
export class LayarDto {
  id?: number;
  @IsString({ groups: [CREATE] })
  kode?: string;

  @IsBoolean({ groups: [CREATE] })
  status?: boolean;
  @IsOptional()
  @IsArray()
  @Type(() => LoketDto)
  @ValidateIf((o, v) => !isNullOrUndefined(v), {
    groups: [UPDATE, CREATE],
  })
  @ValidateNested({ groups: [UPDATE, CREATE] })
  loket: LoketDto[];

  @Type(() => InstansiDto)
  @ValidateNested({ groups: [UPDATE, CREATE] })
  @IsDefined({ groups: [CREATE] })
  @IsOptional({ groups: [UPDATE] })
  instansi: InstansiDto;
}
