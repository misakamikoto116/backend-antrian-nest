import { IsDefined, IsNumber } from 'class-validator';
import { Instansi } from '@entities/instansi/instansi.entity';
import { InDatabase } from '@system/decorators/validation/in.database.validation';
import { CrudValidationGroups } from '@nestjsx/crud';
const { UPDATE, CREATE } = CrudValidationGroups;
export class InstansiDto {
  @IsNumber({}, { groups: [UPDATE, CREATE] })
  @IsDefined({ groups: [UPDATE, CREATE] })
  @InDatabase(Instansi, 'id', {}, { groups: [UPDATE, CREATE] })
  id: number;
}
