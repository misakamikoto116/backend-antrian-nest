import { Module } from '@nestjs/common';
import { LayarModule } from '@entities/layar/layar.module';
import { WebLayarController } from './web-layar.controller';
import { JwtModule } from '@nestjs/jwt';
import { systemConstant } from '@system/constants/system.constant';
import { WebLayarService } from './web-layar.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Layar } from '@entities/layar/layar.entity';
import { Loket } from '@entities/loket/loket.entity';
import { LoketModule } from '@entities/loket/loket.module';

@Module({
  imports: [
    LayarModule,
    JwtModule.register({
      secret: systemConstant.jwtSecreat,
    }),
    LoketModule,
    TypeOrmModule.forFeature([Layar]),
  ],
  controllers: [WebLayarController],
  providers: [WebLayarService],
})
export class WebLayarModule {}
