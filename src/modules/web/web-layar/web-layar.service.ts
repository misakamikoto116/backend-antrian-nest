import { Injectable, Logger } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';

import { InjectRepository } from '@nestjs/typeorm';
import { Layar } from '@entities/layar/layar.entity';
import { Override, ParsedRequest, CrudRequest } from '@nestjsx/crud';
import { LayarEvent } from 'src/events/Layar.event';
import { InjectEventEmitter } from 'nest-emitter';
import { JwtService } from '@nestjs/jwt';
import { LayarDto } from './dtos/layar.dto';
import { Repository } from 'typeorm';
import { Loket } from '@entities/loket/loket.entity';
import { inspect } from 'util';

@Injectable()
export class WebLayarService extends TypeOrmCrudService<LayarDto> {
  private readonly repoLayar: Repository<Layar>;
  constructor(
    @InjectRepository(Layar) public readonly repo,
    @InjectRepository(Loket) public readonly repoLoket: Repository<Loket>,
    @InjectEventEmitter() private readonly event: LayarEvent,
    private readonly jwtService: JwtService,
  ) {
    super(repo);
    this.repoLayar = repo;
  }
  replaceOne(req: CrudRequest, dto: LayarDto) {
    return super.replaceOne(req, dto).then(res => {
      this.repo
        .findOne(res.id, {
          join: {
            alias: 'layar',
            innerJoinAndSelect: {
              instansi: 'layar.instansi',
            },
            leftJoinAndSelect: {
              loket: 'layar.loket',
            },
          },
        })
        .then(layar => {
          this.event.emit(
            'updateLayar',
            layar.id,
            this.jwtService.sign({ ...layar }),
            layar.loket,
          );
        });

      return res;
    });
  }
  async deleteOne(req: CrudRequest) {
    const id = req.parsed.paramsFilter.find(it => it.field === 'id').value;
    const layar = await this.repoLayar.findOne(id, { relations: ['loket'] });
    console.log(inspect(layar, false, 10));
    if (layar.loket.length > 0) {
      await this.repoLoket.update(layar.loket.map(it => it.id), {
        layar: {
          id: null,
        },
      });
    }

    return super.deleteOne(req).then(res => {
      this.event.emit('deleteLayar', id);
      return res;
    });
  }
}
