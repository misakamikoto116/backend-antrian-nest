import { Controller, UseInterceptors } from '@nestjs/common';
import { Layar } from '@entities/layar/layar.entity';
import { LayarDto } from './dtos/layar.dto';

import {
  Crud,
  Override,
  ParsedRequest,
  CrudRequest,
  CrudController,
  ParsedBody,
  CreateManyDto,
  CrudRequestInterceptor,
} from '@nestjsx/crud';
import { WebLayarService } from './web-layar.service';
import { InjectEventEmitter } from 'nest-emitter';
import { LayarEvent } from 'src/events/Layar.event';
import { WhoCanAccess } from '@system/decorators/guards/who-can-access.decorator';
import { parseError } from '@system/helpers/parseError';
@Crud({
  model: {
    type: LayarDto,
  },
  validation: {
    validateCustomDecorators: true,
    exceptionFactory: parseError,
  },
  query: {
    limit: 10,
    join: {
      instansi: {
        eager: true,
        allow: ['nama'],
      },
      loket: {
        eager: true,
        allow: ['nama'],
      },
    },
  },
})
@Controller()
@WhoCanAccess('Admin')
export class WebLayarController {
  constructor(public service: WebLayarService) {}
}
