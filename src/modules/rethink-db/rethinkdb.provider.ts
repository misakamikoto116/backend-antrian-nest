import * as r from 'rethinkdb';
import {
  Logger,
  Injectable,
  OnApplicationBootstrap,
  BadRequestException,
  ConflictException,
} from '@nestjs/common';
import { Observable, from, Subject } from 'rxjs';
import { Repository } from 'typeorm';
import { Layanan } from '@entities/layanan/layanan.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { todayStringFormated } from '@system/helpers/nowStringFormated';
import { Antrian } from '@entities/antrian/antrian.entity';
import { CronJob } from 'cron';
import { AntrianNowSchema } from './schemas/antrian-now.schema';
import { WsException } from '@nestjs/websockets';

import { Loket } from '@entities/loket/loket.entity';
import moment = require('moment');

@Injectable()
export class RethinkDbService implements OnApplicationBootstrap {
  setLoket(id: number, loket: Loket) {
    return new Promise((resolve, reject) => {
      this.con.subscribe(con => {
        r.db('antrian')
          .table('antrianNow')
          .filter({
            antrianId: id,
          })
          .update(
            {
              loket,
              lastUpdate: moment().unix(),
            },
            {
              returnChanges: true,
            },
          )
          .run(con)
          .then(res => {
            resolve({ resAntrianNow: res });
          })
          .catch(err => {
            reject(new ConflictException(err));
          });
      });
    });
  }
  findSocket(loketId: number) {
    return new Promise((res, rej) => {
      this.con.subscribe(con => {
        r.db('antrian')
          .table('clientTv')
          .filter(item => {
            return item('loket')('id').contains(`${loketId}`);
          })
          .run(con)
          .then(
            cur => {
              cur.next((err, row) => {
                if (err) {
                  rej(err);
                } else {
                  res(row);
                }
              });
            },
            err => {
              rej(new ConflictException(err));
            },
          );
      });
    });
  }
  getLasUpdateLokets(lokets: number[]) {
    return new Observable<any>(sub => {
      this.con.subscribe(
        con => {
          r.db('antrian')
            .table('lokets')
            .filter(item => {
              return r.expr(lokets).contains((item('id') as unknown) as any);
            })
            .changes({
              includeInitial: true,
              includeStates: true,
            } as r.ChangesOptions)
            .run(con)
            .then(
              cur => {
                cur.each((err, row) => {
                  if (err) {
                    sub.error(err);
                  } else {
                    sub.next(row);
                  }
                });
              },
              err => sub.error(new ConflictException(err)),
            );
        },
        err => {
          sub.error(new ConflictException(err));
        },
      );
    });
  }
  unRegisterTV(clientId: string) {
    return new Promise((res, rej) => {
      this.con.subscribe(con => {
        r.db('antrian')
          .table('clientTv')
          .filter({
            clientId,
          })
          .update(row => {
            return row.pluck('clientId');
          })
          .run(con)
          .then(res, err => {
            rej(new ConflictException(err));
          });
      });
    });
  }
  registerTV(clientId: string, id: number, loket: number[]) {
    this.logger.log({ clientId, id, loket });
    return new Promise((res, rej) => {
      this.con.subscribe(con => {
        r.db('antrian')
          .table('clientTv')
          .insert(
            {
              clientId,
              id,
              loket,
            },
            {
              conflict: 'update',
            },
          )
          .run(con)
          .then(res, err => {
            rej(new ConflictException(err));
          });
      });
    });
  }
  getUser(id: string) {
    return new Promise((resolve, reject) => {
      this.con.subscribe(con => {
        r.db('antrian')
          .table('client')
          .filter({
            socketId: id,
          })
          .run(con)
          .then(
            cursor => {
              cursor.next((err, item) => {
                if (err) {
                  reject(
                    new WsException(
                      `User dengan client id ${id} tidak ditemukan`,
                    ),
                  );
                } else {
                  resolve(item);
                }
              });
            },
            err => {
              throw new ConflictException(err);
            },
          );
      });
    });
  }
  updateAntrianNow(layananId: number) {
    return new Promise((res, rej) => {
      this.con.subscribe(con => {
        r.db('antrian')
          .table('antrianNow')
          .filter(
            r
              .row('sisaAntrian')
              .gt(-1)
              .and(
                r
                  .row('layanan')('id')
                  .eq(layananId),
              ),
          )
          .update(
            {
              sisaAntrian: r.row('sisaAntrian').sub(1),
              lastUpdate: moment().unix(),
            },
            {
              returnChanges: true,
            },
          )
          .run(con)
          .then(
            log => {
              res();
            },
            err => {
              rej(new ConflictException(err));
            },
          );
      });
    });
  }
  listernProgressAllLayanan() {
    return new Observable(sub => {
      this.con.subscribe(con => {
        r.db('antrian')
          .table('loketAntrian')
          .orderBy({ index: r.desc('lastUpdate') })
          .filter({ tanggal: todayStringFormated() })
          .map(item => {
            return ({
              namaLayanan: r.branch(
                (item as any).hasFields('namaLayanan'),
                item('namaLayanan'),
                r.expr(''),
              ),
              sisaAntrian: item('sisaAntrian'),
              id: item('id'),
              panggilanTerakhir: r.branch(
                (item('loket') as any)('antrianTerakhir')
                  .count()
                  .gt(0),
                (item('loket') as any)('antrianTerakhir').max(),
                r.expr(0),
              ),
            } as unknown) as r.Expression<any>;
          })
          .limit(8)
          .changes({
            includeInitial: true,
          } as r.ChangesOptions)

          .run(con)
          .then(res => {
            res.each((err, row) => {
              if (err) {
                sub.error(err);
              } else {
                const id = (row.new_val.id as string).substring(11);
                sub.next({ ...row.new_val, id });
              }
            });
          })
          .catch(err => {
            sub.error(err);
          });
      });
    });
  }
  addInQueue(queueRepsonse: AntrianNowSchema) {
    this.con.subscribe(con => {
      r.db('antrian')
        .table('antrianNow')
        .insert(
          { ...queueRepsonse, lastUpdate: moment().unix() },
          {
            conflict: 'update',
          },
        )
        .run(con)
        .catch(err => {
          throw new ConflictException(err);
        });
    });
  }
  listenChangeUserInQueue(userId: number) {
    return new Observable<any>(subscribe => {
      this.con.subscribe(con => {
        r.db('antrian')
          .table('antrianNow')
          .filter(
            r
              .row('sisaAntrian')
              .gt(-1)
              .and(
                r
                  .row('user')('id')
                  .eq(userId),
              ),
          )
          .changes({
            includeInitial: true,
          } as r.ChangesOptions)
          .run(con)
          .then(
            res => {
              res.each((err, row) => {
                if (err) {
                  subscribe.error(new WsException(err));
                  // subscribe.error(err);
                } else {
                  subscribe.next(row.new_val);
                }
              });
            },
            err => {
              subscribe.error(new ConflictException(err));
            },
          );
      });
    });
  }
  private con: Observable<r.Connection>;
  private logger: Logger;
  constructor(
    @InjectRepository(Layanan)
    private readonly layananRepo: Repository<Layanan>,
    @InjectRepository(Antrian)
    private readonly antrianRepo: Repository<Antrian>,
  ) {
    this.logger = new Logger('RethinkDbProvider');
    this.con = from(r.connect({ db: 'antrian' }));
  }
  getLayananQueue(layananId: number, tanggal: string): Observable<any> {
    const layananQueue = new Subject<any>();
    const id = tanggal + '-' + layananId;
    this.con.subscribe(con => {
      r.db('antrian')
        .table('loketAntrian')
        .filter({
          id,
        })
        .changes({ includeInitial: true } as r.ChangesOptions)
        .run(con)
        .then(curr => {
          curr.each((err, item) => {
            if (err !== null) {
              layananQueue.error(new ConflictException(err));
            } else {
              layananQueue.next(item);
            }
          });
        });
    });
    return layananQueue;
  }
  unRegisterPrinter(socketId: string) {
    this.con.subscribe(con => {
      r.db('antrian')
        .table('printer')
        .filter({ socketId })
        .replace((client: any) => client.pluck('id'))
        .run(con)
        .catch(err => {
          throw new ConflictException(err);
        });
    });
  }
  registerPrinter(socketId: string, id: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.con.subscribe(con => {
        r.db('antrian')
          .table('printer')
          .insert(
            {
              id,
              socketId,
            },
            {
              conflict: 'update',
            },
          )
          .run(con)
          .then(resolve)
          .catch(err => {
            reject(new ConflictException(err));
          });
      });
    });
  }
  unRegisterUser(socketId: string) {
    this.con.subscribe(con => {
      r.db('antrian')
        .table('client')
        .filter({ socketId })
        .replace((client: any) => client.pluck('id'))
        .run(con)
        .catch(err => {
          throw new ConflictException(err);
        });
    });
  }
  registerUser(socketId: string, id: number) {
    return new Promise((resolve, reject) => {
      this.con.subscribe(con => {
        r.db('antrian')
          .table('client')
          .insert(
            {
              id,
              socketId,
            },
            {
              conflict: 'update',
            },
          )
          .run(con)
          .then(resolve)
          .catch(err => {
            reject(new ConflictException(err));
          });
      });
    });
  }
  createQueueForDate(datas: Array<{}>) {
    this.con.subscribe(con => {
      r.db('antrian')
        .table('loketAntrian')
        .insert(datas.map(it => ({ ...it, lastUpdate: moment().unix() })), {
          conflict: 'update',
        })
        .run(con)
        .catch(err => {
          throw new ConflictException(err);
        });
    });
  }
  async onApplicationBootstrap(): Promise<void> {
    return new Promise(resolve => {
      this.con.subscribe(con => {
        this.createDatabaseAndTable(con).then(resolve);
      });
    }).then(() => {
      // tslint:disable-next-line:no-unused-expression
      new CronJob(
        '0 0 * * *',
        () => {
          this.clearQueueu();
          this.baseLoketDay().then(datas => {
            this.createQueueForDate(datas);
            this.updateLoketRethinkdb(datas);
          });
        },
        null,
        null,
        'Asia/Makassar',
      );
      this.baseLoketDay().then((datas: any) => {
        this.clearQueueu();
        this.updateLoketRethinkdb(datas);
        this.createQueueForDate(datas);
      });
    });
  }
  clearQueueu() {
    this.antrianRepo.update(
      {
        tanggal: moment()
          .subtract(1, 'days')
          .format('YYYY-MM-DD'),
        terpanggil: false,
      },
      {
        terpanggil: true,
        waktuDipanggil: moment().format('YYYY-MM-DD hh:mm:ss'),
      },
    );
  }
  private updateLoketRethinkdb(datas: any) {
    const lokets = datas.reduce((out, layanan) => {
      const rLokets = layanan.loket.map(l => {
        l.sisaAntrian = layanan.sisaAntrian;
        l.namaLayanan = layanan.namaLayanan;
        return l;
      });

      return out.concat(rLokets);
    }, []);

    this.con.subscribe(con => {
      r.db('antrian')
        .table('lokets')
        .insert(lokets, {
          conflict: 'update',
        })
        .run(con)
        .catch(err => {
          throw new ConflictException(err);
        });
    });
  }

  private createDatabaseAndTable(con: r.Connection) {
    this.logger.log('rethink connected');
    return r
      .dbList()
      .run(con)
      .then(
        dbs => {
          const createTable = () => {
            r.db('antrian')
              .tableList()
              .run(con)
              .then(async tbls => {
                if (!tbls.includes('loketAntrian')) {
                  await r
                    .db('antrian')
                    .tableCreate('loketAntrian')
                    .run(con);
                }
                if (!tbls.includes('client')) {
                  await r
                    .db('antrian')
                    .tableCreate('client')
                    .run(con);
                }
                if (!tbls.includes('printer')) {
                  await r
                    .db('antrian')
                    .tableCreate('printer')
                    .run(con);
                }
                if (!tbls.includes('clientTv')) {
                  await r
                    .db('antrian')
                    .tableCreate('clientTv')
                    .run(con);
                }
                if (!tbls.includes('lokets')) {
                  await r
                    .db('antrian')
                    .tableCreate('lokets')
                    .run(con);
                }
                /**
                 * table ini berisi antrian now dari
                 * setiap user yang akan di update
                 * ketika petugas melakukan pemanggilan
                 */
                if (!tbls.includes('antrianNow')) {
                  await r
                    .db('antrian')
                    .tableCreate('antrianNow')
                    .run(con);
                  await r
                    .db('antrian')
                    .table('antrianNow')
                    .indexCreate('antrianId');
                }
              });
          };
          if (dbs.filter(db => db === 'antrian').length === 0) {
            r.dbCreate('antrian')
              .run(con)
              .then(createTable);
          } else {
            createTable();
          }
        },
        err => {
          throw new ConflictException(err);
        },
      );
  }

  updateInformationService(layananId: number) {
    return this.layananRepo
      .createQueryBuilder('layanan')
      .innerJoinAndSelect('layanan.loket', 'loket', 'loket.aktif = :aktif', {
        aktif: true,
      })
      .getMany()
      .then(
        layanans => {
          this.generateData(layanans).then(datas => {
            this.updateLoketRethinkdb(datas);
            this.createQueueForDate(datas);
          });
        },
        err => {
          throw new ConflictException(err);
        },
      );
  }
  async baseLoketDay(): Promise<Array<{}>> {
    const layanans = await this.layananRepo
      .createQueryBuilder('layanan')
      .leftJoinAndSelect('layanan.loket', 'loket', 'loket.aktif = :aktif', {
        aktif: true,
      })
      .getMany();
    return this.generateData(layanans);
  }

  /**
   * mapping data sesuai kebutuhan
   * @param layanans  id layanan yang akan di proses
   *
   */
  private async generateData(layanans: Layanan[]) {
    if (layanans.length < 1) {
      return [];
    }
    const loketsStatus = await this.getLoketByLayananIds(
      layanans.map(l => l.id),
    );
    return layanans.map(layanan => {
      try {
        const { inQueueByLokets, notCalledByLokets } = loketsStatus.find(
          item => item.id === layanan.id,
        );
        const data = {
          id: todayStringFormated() + '-' + layanan.id,
          sisaAntrian: notCalledByLokets < 0 ? 0 : notCalledByLokets,
          tanggal: todayStringFormated(),
          namaLayanan: layanan.nama,
          loket: layanan.loket.map(loket => {
            const inQQloket = inQueueByLokets.find(
              inqq => inqq.loketId === loket.id,
            );
            let antrianTerakhir = 0;
            if (inQQloket) {
              antrianTerakhir = inQQloket.numberqq;
            }
            return { ...loket, antrianTerakhir };
          }),
        };
        return data;
      } catch (error) {
        throw new ConflictException(error);
      }
    });
  }

  private async getLoketByLayananIds(ids: number[]) {
    interface MaxNumberCalled {
      layananId: number;
      loketId: number;
      terpanggil: number;
      noQueue: number;
    }

    // ngambil data termaximal untuk yang  sudah terpanggil dan belum terpanggil
    const antrian: MaxNumberCalled[] = await this.antrianRepo
      .createQueryBuilder('antrian')
      .select('MAX(antrian.nomor)', 'noQueue')
      .addSelect('antrian.layananId', 'layananId')
      .addSelect('antrian.loketId', 'loketId')
      .addSelect('antrian.terpanggil', 'terpanggil')
      .where('antrian.layananId IN (:ids)')
      .andWhere('DATE(antrian.tanggal) = DATE(:tanggal)')
      .setParameters({ tanggal: todayStringFormated(), ids })
      .groupBy('antrian.layananId')
      .addGroupBy('antrian.loketId')
      .addGroupBy('terpanggil')
      .orderBy('antrian.id', 'DESC')
      .getRawMany();
    // nomor antrian yang sudah terpanggil
    const served = antrian.filter((v: any) => v.terpanggil === 1);
    // nomor antrian yang belum terpanggil
    const notServed = antrian.filter((v: any) => v.terpanggil === 0);
    // mapping antrian yang sedang terpanggil dan belum terpanggil pada setiap layanan
    return ids.map(id => {
      // filter dan mapping data
      const inQueueByLokets = served
        .filter(inQqL => inQqL.layananId === id)
        .map(mapAntrian => ({
          numberqq: mapAntrian.noQueue,
          loketId: mapAntrian.loketId,
        }));
      // cari panggilan paling terakhir
      const maxInQueue = inQueueByLokets.reduce(
        (out, inQueue) => (out > inQueue.numberqq ? out : inQueue.numberqq),
        0,
      );
      // jumlah yang belum terpanggil
      const notCalledByLokets =
        notServed
          .filter(nClldQq => nClldQq.layananId === id)
          .reduce((out, nextNClldQq) => {
            return out > nextNClldQq.noQueue ? out : nextNClldQq.noQueue;
          }, 0) - maxInQueue;
      // return
      return {
        id,
        inQueueByLokets,
        notCalledByLokets,
      };
    });
  }
}
