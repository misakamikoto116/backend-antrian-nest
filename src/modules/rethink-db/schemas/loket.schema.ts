export interface LoketSchema {
  aktif: number;
  antrianTerakhir: number;
  id: number;
  kode: string;
  nama: string;
}
