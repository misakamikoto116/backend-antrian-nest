import { User } from '@entities/user/user.entity';
import { DeepPartial } from 'typeorm';

export interface AntrianNowSchema {
  sisaAntrian: number;
  antrianId: number;
  nomor: number;
  estimasiPemanggilan: number;
  user: DeepPartial<User>;
  id?: number;
  layanan: {
    id: number;
    nama: string;
    keterangan: string;
    instansi: {
      nama: string;
    };
  };
}
