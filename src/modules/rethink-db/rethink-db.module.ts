import { Module, Logger, OnApplicationBootstrap, Inject } from '@nestjs/common';
import { RethinkDbService } from './rethinkdb.provider';
import { LayananModule } from '@entities/layanan/layanan.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Layanan } from '@entities/layanan/layanan.entity';
import { Antrian } from '@entities/antrian/antrian.entity';
import { Loket } from '@entities/loket/loket.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Layanan, Antrian])],
  providers: [RethinkDbService],
  exports: [RethinkDbService],
})
export class RethinkDbModule {}
