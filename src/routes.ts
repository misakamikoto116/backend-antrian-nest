import { AuthModule } from '@modules/auth/auth.module';
import { MobileLayananModule } from '@modules/mobile/mobile-layanan/mobile-layanan.module';
import { MobileAntrianModule } from '@modules/mobile/mobile-antrian/mobile-antrian.module';
import { MobileModule } from '@modules/mobile/mobile.module';
import { MobileUserModule } from '@modules/mobile/mobile-user/mobile-user.module';
import { WebInstansiModule } from '@modules/web/web-instansi/web-instansi.module';
import { WebLayananModule } from '@modules/web/web-layanan/web-layanan.module';
import { WebLayarModule } from '@modules/web/web-layar/web-layar.module';
import { WebLoketModule } from '@modules/web/web-loket/web-loket.module';
import { WebPelayananLoketModule } from '@modules/web/web-pelayanan/loket/web-pelayanan-loket.module';
import { WebPelayananModule } from '@modules/web/web-pelayanan/web-pelayanan.module';
import { WebModule } from '@modules/web/web.module';
import { KabupatenModule as WebKabupatenModule } from '@modules/web/wilayah/kabupaten/kabupaten.module';
import { KecamatanModule as WebKecamatanModule } from '@modules/web/wilayah/kecamatan/kecamatan.module';
import { KelurahanModule as WebKelurahanModule } from '@modules/web/wilayah/kelurahan/kelurahan.module';
import { ProvinsiModule as WebProvinsiModule } from '@modules/web/wilayah/provinsi/provinsi.module';
import { WebWilayahModule } from '@modules/web/wilayah/web.wilayah.module';
import { Routes } from 'nest-router';

import { WebRoleModule } from './modules/web/web-role/web-role.module';
import { WebUserModule } from '@modules/web/web-user/web-user.module';
import { WebAnjunganModule } from '@modules/web/web-anjungan/web-anjungan.module';
import { WebPrinterModule } from '@modules/web/web-printer/web-printer.module';
import { ClientPrinterModule } from '@modules/client/printer/client-printer.module';
import { ClientAnjunganModule } from '@modules/client/anjungan/client-anjungan.module';
import { WebPetugasModule } from '@modules/web/web-petugas/web-petugas.module';
import { WebIklanModule } from '@modules/web/web-iklan/web-iklan.module';
import { ClientLayarTvModule } from '@modules/client/layar-tv/cllient-layar-tv.module';
import { InfoPajakModule } from '@modules/web/info-pajak/info-pajak.module';

export const routes: Routes = [
  {
    path: '/api/v1',
    children: [
      { path: '/auth', module: AuthModule },
      {
        path: '/mobile',
        module: MobileModule,
        children: [
          {
            path: '/user',
            module: MobileUserModule,
          },
          {
            path: '/layanan',
            module: MobileLayananModule,
          },
          {
            path: '/antrian',
            module: MobileAntrianModule,
          },
        ],
      },
      {
        path: '/client',
        children: [
          {
            path: '/printer',
            module: ClientPrinterModule,
          },
          {
            path: '/anjungan',
            module: ClientAnjunganModule,
          },
          {
            path: '/layar',
            module: ClientLayarTvModule,
          },
        ],
      },
      {
        path: '/web',
        module: WebModule,
        children: [
          {
            path: '/layanan',
            module: WebLayananModule,
          },
          {
            path: '/user',
            module: WebUserModule,
          },
          {
            path: '/petugas',
            module: WebPetugasModule,
          },
          {
            path: '/pelayanan',
            module: WebPelayananModule,
            children: [
              {
                path: '/loket',
                module: WebPelayananLoketModule,
              },
            ],
          },
          {
            path: '/layar',
            module: WebLayarModule,
          },
          {
            path: '/loket',
            module: WebLoketModule,
          },
          {
            path: '/instansi',
            module: WebInstansiModule,
          },
          {
            path: '/role',
            module: WebRoleModule,
          },
          {
            path: '/iklan',
            module: WebIklanModule,
          },
          {
            path: '/printer',
            module: WebPrinterModule,
          },
          {
            path: '/anjungan',
            module: WebAnjunganModule,
          },
          {
            path: '/wilayah',
            module: WebWilayahModule,
            children: [
              {
                path: '/provinsi',
                module: WebProvinsiModule,
              },
              {
                path: '/kabupaten',
                module: WebKabupatenModule,
              },
              {
                path: '/kecamatan',
                module: WebKecamatanModule,
              },
              {
                path: '/kelurahan',
                module: WebKelurahanModule,
              },
            ],
          },
          {
            module: InfoPajakModule,
            path: 'info-pajak'
          }
        ],
      },
    ],
  },
];
