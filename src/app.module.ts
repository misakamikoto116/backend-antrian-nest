import { AuthModule } from '@modules/auth/auth.module';
import { InstansiModule } from '@entities/instansi/instansi.module';
import { MobileModule } from '@modules/mobile/mobile.module';
import { Role } from '@entities/role/role.entity';
import { SocketModule } from '@modules/socket/socket.module';
import { User } from '@entities/user/user.entity';
import { UserModule } from '@entities/user/user.module';
import { WilayahModule } from '@entities/wilayah/wilayah.module';
import { Module, OnApplicationBootstrap, Logger } from '@nestjs/common';
import { APP_FILTER, APP_GUARD, APP_INTERCEPTOR } from '@nestjs/core';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { systemConstant } from '@system/constants/system.constant';
import { AppGuard } from '@system/guards/app.guard';

import { HttpExceptionFilter } from '@system/http-exception-filter';

import { config } from 'dotenv';
import { RouterModule } from 'nest-router';
import { getConnection } from 'typeorm';

import { AntrianModule } from '@entities/antrian/antrian.module';
import { LayananModule } from '@entities/layanan/layanan.module';
import { LayarModule } from '@entities/layar/layar.module';
import { LoketModule } from '@entities/loket/loket.module';
import { RoleModule } from '@entities/role/role.module';
import { WebModule } from '@modules/web/web.module';
import { routes } from './routes';
import { RavenModule } from 'nest-raven';
import { AppInterceptor } from '@system/interceptors/app.interceptor';
import { RethinkDbModule } from '@modules/rethink-db/rethink-db.module';
import { AnjunganModule } from '@entities/anjungan/anjungan.module';
import { PrinterModule } from '@entities/printer/printer.module';
import { ClientModule } from '@modules/client/client.module';
import { PetugasModule } from '@entities/petugas/petugas.module';
import { IklanModule } from '@entities/iklan/iklan.module';
import { FirebaseModule } from '@modules/firebase/firebase.module';
import { NestEmitterModule } from 'nest-emitter';
import { EventEmitter } from 'events';
Logger.log(__dirname + '/**/*.entity.js');
config();
@Module({
  imports: [
    RavenModule,
    NestEmitterModule.forRoot(new EventEmitter()),
    TypeOrmModule.forRoot(),
    RouterModule.forRoutes(routes),
    AuthModule,
    UserModule,
    MobileModule,
    LayananModule,
    WebModule,
    LayarModule,
    SocketModule,
    InstansiModule,
    WilayahModule,
    LoketModule,
    AntrianModule,
    AnjunganModule,
    PrinterModule,
    RoleModule,
    JwtModule.register({
      secret: systemConstant.jwtSecreat,
    }),
    RethinkDbModule,
    ClientModule,
    PetugasModule,
    IklanModule,
    FirebaseModule,
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: AppGuard,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: AppInterceptor,
    },
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter,
    },
  ],
})
export class AppModule implements OnApplicationBootstrap {
  onApplicationBootstrap() {
    return new Promise((resolve, reject) => {
      const repo = getConnection().getRepository(Role);
      const roles = [
        {
          title: 'Admin',
        },
        {
          title: 'Petugas',
        },
        {
          title: 'User',
        },
      ];
      const promises = roles.map(role => {
        return repo.findOne({ where: { title: role.title } }).then(roleDb => {
          if (roleDb === undefined) {
            return repo.save(role);
          } else {
            Promise.resolve(roleDb);
          }
        });
      });
      Promise.all(promises).then(resolve, reject);
    });
  }
}
