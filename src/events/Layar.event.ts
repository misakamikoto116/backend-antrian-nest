import { EventEmitter } from 'events';
import { StrictEventEmitter } from 'nest-emitter';
import { Loket } from '@entities/loket/loket.entity';
import { DeepPartial } from 'typeorm';
export interface ILayarEvent {
  updateLayar(
    id: number,
    token: string,
    lokets: Array<DeepPartial<Loket>>,
  ): void;
  deleteLayar(layarId: number);
}
export type LayarEvent = StrictEventEmitter<EventEmitter, ILayarEvent>;
