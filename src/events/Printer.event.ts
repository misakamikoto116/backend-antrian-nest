import { EventEmitter } from 'events';
import { StrictEventEmitter } from 'nest-emitter';
export interface IPrinterEvent {
  printTicket(data: any): void;
}
export type PrinterEvent = StrictEventEmitter<EventEmitter, IPrinterEvent>;
