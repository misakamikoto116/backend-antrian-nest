import { EventEmitter } from 'events';
import { StrictEventEmitter } from 'nest-emitter';
export interface ICallerEvent {
  makeVoice(data: string, loketId:number): void;
}
export type CallerEvent = StrictEventEmitter<EventEmitter, ICallerEvent>;
